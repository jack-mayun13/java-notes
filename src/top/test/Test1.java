package top.test;

import org.junit.Test;

import java.util.Arrays;

public class Test1 {

    /**
     *      8种基本数据类型
     *
     *      byte    short    int    long    char    float   double       boolean
     *       1       2        4      8       2       4       8          true 和 false
     *
     *      3种引用数据类型
     *      接口              数组              类           String属于数组
     *      interface         []            class
     *
     *      关键字
     *      class  interface  enum  byte  short  int  long  float  double  char
     *      boolean  void  if  else  switch  case  default  while  do  for  break
     *      continue  return  private  protected  public  abstract  final  static
     *      synchronized  extends  implements  new  this  super  instanceof
     *      try  catch  finally  throw  throws  package  import   true  false  null
     *      length equals transient
     *
     *      类，接口，枚举，8种基本数据类型，空类型，分支循环，返回，四种权限修饰符，抽象，固定，静态，
     *      同步，继承，实现，构造，本类，父类，实例运算符，异常，包，导入，布尔值，空，长度，相等，不序列化
     *
     *      自动类型提升：当容量小的数据类型的变量与容量大的数据类型的变量做运算时，结果自动提升为容量大的数据类型。
     *                 当byte、char、short三种类型的变量做运算时，结果为int型
     *
     *      强制类型转换：自动类型提升运算的逆运算。
     *                 强制类型转换，可能导致精度损失。
     *
     *      switch结构中的表达式，只能是如下的6种数据类型之一：
     *          byte 、short、char、int、枚举类型、String类型
     *
     *      & 和 &&  的区别：
     *          都是逻辑运算符表示and，即两边同时为true运算结果才为true
     *          && 具有短路功能，前面表达式值为false时不进行后面的运算
     *          &  不管前面的表达式为true还是false都会参与后面的运算
     *             还可以用作位运算符，表示按位操作，二进制对应位两边都为1才取1
     *
     *      有哪些位运算符：与 或 异或 取反 左移 右移 无符号右移
     *
     *      两个二进制 异或 ^ 即a^b = |a-b|
     *
     *      数组工具类：
     *        sort              排序
     *        toString          将数组转换成字符串输出
     *        fill              将数组中的值全部替换成指定值
     *        binarySearch      排序后的数组进行二分查找
     *        equals            判断两个数组是否相等
     *
     *      参数传递时 传值还是传引用：
     *          当传递类型为基本数据类型时，是传值
     *          当传递类型为引用数据类型时，拷贝了引用的值，两者共同指向堆空间中的同一个对象。仍是按值调用
     *
     *      排序算法与其性能：
     *
     *                           平均时间            最坏时间           稳定性
     *      归并排序               nlog₂n            nlog₂n            稳定
     *      冒泡排序               n²                n²                稳定
     *      插入排序               n²                n²                稳定
     *      基数排序
     *
     *      堆排序                nlog₂n              nlog₂n          不稳定
     *      快速排序              nlog₂n              n²              不稳定
     *      希尔排序              n¹˙³                n²              不稳定
     *      选择排序              n²                  n²              不稳定
     *
     */


    //二分查找
    @Test
    public void test1() {
        int[] ss = new int[]{-55,-42,-36,-28,-19,-7,2,8,45,67,87,98,125};
        int yy = 98;                //被查找的数
        int head = 0;
        int end = ss.length-1;
        boolean isFast = true;
        while (head<=end) {
            int midd = (head+end)/2;
            if (yy == ss[midd]) {
                System.out.println("找到了：索引为"+midd);
                isFast = false;
                break;
            }else if (yy < ss[midd]) {
                end = midd-1;
            }else {
                head = midd+1;
            }
        }
        if (isFast) {
            System.out.println("没有找到");
        }
    }


    //冒泡排序
    @Test
    public void test2() {
        int[] arr = new int[]{43,32,76,-98,0,64,33,-21,32,99};
        //冒泡排序
        for(int i = 0;i < arr.length - 1;i++){
            for(int j = 0;j < arr.length - 1 - i;j++){
                if(arr[j] > arr[j + 1]){
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    //快速排序
    @Test
    public void test3(){
        int[] arr = new int[]{43,32,76,-98,0,64,33,-21,32,99,88};
        int left = 0;
        int right = arr.length-1;
        qqq(arr,left,right);
        System.out.println(Arrays.toString(arr));
    }

    private void qqq(int[] arr, int L, int R) {
        if (L >= R) {
            return;
        }
        int left = L;
        int right = R;
        int pro = arr[left];
        while (left < right) {
            while (left < right && pro <= arr[right]) {
                right--;
            }
            if (left < right) {
                arr[left] = arr[right];
            }
            while (left < right && pro >= arr[left]) {
                left++;
            }
            if (left < right) {
                arr[right] = arr[left];
            }
            if (left >= right) {
                arr[left] = pro;
            }
        }
        qqq(arr,L+1,R);
        qqq(arr,L,R-1);
    }

}

