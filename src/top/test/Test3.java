package top.test;

public class Test3 {

    /**
     *      睡眠排序
     *
     */

    public static void main(String[] args) {

        int[] arr = {12,65,42,1,24,46,34,79,85,24,165,885,664};

        Thread[] threads = new Thread[arr.length-1];

        for (int i = 0;i < arr.length-1;i++) {
            int finalI = i;
            threads[i] = new Thread(){
                @Override
                public void run() {
                    try {
                        sleep(arr[finalI]*10);
                        System.out.println(arr[finalI]);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            threads[i].start();
        }

    }







}
