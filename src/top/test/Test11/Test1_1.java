package top.test.Test11;

import org.junit.Test;

import java.util.Arrays;

public class Test1_1 {

    /**
     *
     *      8种基本数据类型
     *      byte short int long char boolean float double
     *
     *      3种引用数据类型
     *      接口 interface    数组[]    类class      其中字符串属于数组
     *
     *      类，接口，枚举，8种基本数据类型，空类型，分支循环，返回，四种权限修饰符，抽象，固定，静态，
     *      同步，继承，实现，构造，本类，父类，实例运算符，异常，包，导入，布尔值，空
     *
     *      class   interface   byte short int long char boolean float double void
     *      if else while switch do for break case 跳出本次循环  return public 受保护的  私有的
     *      抽象的  固定的  静态的  同步的  静态的  实现  new this super 实例运算符  try ca异常 finally
     *      package import true false null
     *
     *      continue private protected abstract final static synchronized extends implements
     *      instanceof catch finally package import length equals
     *
     *      自动类型提升:容量小的数据自动提升为容量大的数据，比如short char byte 自动提升为 int 类型的数据
     *                 int -> float -> double
     *
     *      强制类型转换：自动类型提升的逆过程   可能会导致精度损失
     *
     *      switch结构中的表达式类型
     *          byte char short int String 枚举
     *
     *      & 和 &&  的区别
     *          都是逻辑运算符表示and   表达式两边都为true运算结果才为true
     *          && 有短路  前一个表达式为false 后一个表达式不参与运算
     *          & 不管前一个表达式结果为true还是false 后一个表达式都会参与运算
     *          当式子两边不为boolean类型时，为逻辑与运算符   两边取二进制对应位都为1此位才为1
     *
     *      有哪些位运算符
     *          与 (两个位都为1时，结果才为1)   或  (两个位都为0时，结果才为0)
     *          异或 (两个位相同为0，相异为1)         取反 左移 右移 无符号右移
     *
     *      数组工具类
     *          sort    排序
     *          equals  判断两个字符串的值是否相等
     *          toString   转换为字符串输出
     *          fill        将数组中全部的值替换为指定字符
     *          binarySearch 排序后的数组进行进行二分查找
     *
     *
     *      排序算法与其性能
     *                      最坏              平均              稳定性
     *      冒泡排序        n的平方            n的平方              稳定
     *      快排           n*log以2为底的n方   n的平方             不稳定
     *      堆排序         n*log以2为底的n方   n*log以2为底的n方    不稳定
     *      归并排序    n*log以2为底的n方        n*log以2为底的n方  稳定
     *      选择排序
     *      希尔排序
     *      桶排序
     *      插入排序
     *
     *      归冒插基    堆快希选
     *
     *
     *      代码：
     *          //二分查找
     *          //冒泡排序
     *          //快速排序
     *
     */

    @Test
    public void tet1() {
        //二分查找
        int[] arr = {25,86,46,-75,369,-95,10,0,348,1};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        int yy = 0;
        int head = 0;
        int end = arr.length-1;
        boolean isF = true;
        while (head <= end) {
            int mod = (head + end)/2;
            if (yy == arr[mod]) {
                System.out.println("找到了,索引为：" + mod);
                isF = false;
                break;
            }else if (yy < arr[mod]) {
                end = mod-1;
            }else if (yy > arr[mod]) {
                head = mod + 1;
            }
        }
        if (isF) {
            System.out.println("没有找到");
        }
    }

    @Test
    public void tet2() {
        //冒泡排序
        int[] arr = {25,86,46,-75,369,-95,10,0,348,1};
        for (int i = 0;i < arr.length;i++) {
            for (int j = 0;j < arr.length-1-i;j++) {
                if (arr[j] > arr[j+1]) {
                    int tm = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tm;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    @Test
    public void tet3() {
        //快速排序
        int[] arr = {25,86,46,-75,369,-95,10,0,348,1};
        int left = 0;
        int right = arr.length-1;
        qqq(arr,left,right);
        System.out.println(Arrays.toString(arr));
    }

    private void qqq(int[] arr, int L, int R) {
        if (L > R) {
            return;
        }
        int left = L;
        int right = R;
        int pro = arr[left];
        while (left < right) {
            while (left < right && pro <= arr[right]) {
                right--;
            }
            if (left < right) {
                arr[left] = arr[right];
            }
            while (left < right && pro >= arr[left]) {
                left++;
            }
            if (left < right) {
                arr[right] = arr[left];
            }
            if (left >= right) {
                arr[left] = pro;
            }
        }
        qqq(arr,left + 1,R);
        qqq(arr,L,right - 1);
    }

}
