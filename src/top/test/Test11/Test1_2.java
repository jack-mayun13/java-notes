package top.test.Test11;

import org.junit.Test;

import java.util.Arrays;

public class Test1_2 {

    /**
     *
     *      8种基本数据类型
     *      byte short int long char float double boolean
     *
     *      3种引用数据类型
     *      interface 接口    数组[]    类class
     *
     *
     *      类，接口，枚举，8种基本数据类型，空类型，分支循环，返回，四种权限修饰符，抽象，固定，静态，
     *      同步，继承，实现，构造，本类，父类，实例运算符，异常，包，导入，布尔值，空
     *
     *      class interface byte short int long char boolean float double boolean void
     *      if else while switch case for break 跳出本次循环  do return private public 受保护的
     *      抽象的 final   静态的  sy同步的  ex继承    实现  new this super 实例运算符    try 异常
     *      package import  true false
     *
     *      自动类型提升
     *          容量小的数据类型自动提升为容量大的数据类型
     *
     *      强制类型转换
     *          自动类型提升的逆过程，可能会导致精度损失
     *
     *      switch结构中的表达式
     *          byte short int char String 枚举
     *
     *      & 和 &&  的区别
     *          &&短路  前面不为true不会计算后面    &会
     *                  &两边不为布尔类型时，为逻辑与运算符，二进制同位同为才为
     *
     *      有哪些位运算符
     *          与 或 异或 非 左移 右移 无符号右移
     *
     *      数组工具类
     *          sort toString equals
     *
     *      参数传递时 传值还是传引用
     *          传递为基本数据类型时，是值传递
     *          传递为引用数据类型时，是拷贝指针指向同一个对象，本质上还是值传递
     *
     *      排序算法与其性能
     *
     *
     *      代码：
     *          //二分查找
     *          //冒泡排序
     *          //快速排序
     *
     *
     */
    public static void main(String[] args) {

        //冒泡排序
        int[] arr = {75,25,-95,0,13,463,549,67,88,22,-16};
        for (int i = 0;i < arr.length;i++) {
            for (int j = 0;j < arr.length-1-i;j++) {
                if (arr[j] > arr[j+1]) {
                    int tm = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tm;
                }
            }
        }
        System.out.println(Arrays.toString(arr));

        //二分查找
        int yy = 88;
        int head = 0;
        int end = arr.length-1;
        boolean iF = true;
        while (head <= end) {
            int mod = (head + end) / 2;
            if (yy == arr[mod]) {
                System.out.println("找到了,索引为：" + mod);
                iF = false;
                break;
            }else if (yy < arr[mod]) {
                end = mod - 1;
            }else if (yy > arr[mod]) {
                head = mod + 1;
            }
        }
        if (iF) {
            System.out.println("没有找到");
        }
    }

    @Test
    public void test1() {
        int[] arr = {75,25,-95,0,13,463,549,67,88,22,-16};
        int left = 0;
        int right = arr.length-1;
        qqq(arr,left,right);
        System.out.println(Arrays.toString(arr));
    }

    private void qqq(int[] arr, int L, int R) {
        if (L >= R) {
            return;
        }
        int left = L;
        int right = R;
        int pro = arr[left];
        while (left < right) {
            while (left < right && pro <= arr[right]) {
                right--;
            }
            if (left < right) {
                arr[left] = arr[right];
            }
            while (left < right && pro >= arr[left]) {
                left++;
            }
            if (left < right) {
                arr[right] = arr[left];
            }
            if (left >= right) {
                arr[left] = pro;
            }
        }
        qqq(arr,left+1,R);
        qqq(arr,L,right-1);
    }
}
