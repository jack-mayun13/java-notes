package top.mysql.SQL2;


/**
 *          SQL语句性能分析的工具
 *
 */
public class Indexes {

/**
 *      ----------------------------------索引语法------------------------------------
 *
 *      主键索引 PRIMARY     唯一索引 UNIQUE      全文索引 FULLTEXT     常规索引      联合索引
 *
 *      创建索引
 *      CREATE [ UNIQUE | FULLTEXT ] INDEX index_name ON table_name (index_col_name,... );
 *
 *      查看索引
 *      SHOW INDEX FROM table_name;
 *
 *      删除索引
 *      DROP INDEX index_name ON table_name;
 *
 *
 *      ----------------------------------性能分析-查看执行频次---------------------------
 *
 *      可以查看当前数据库的INSERT、UPDATE、DELETE、SELECT的访问频次：
 *      show [session|global] status like 'Com_______';       (session是当前会话，global是全局的)
 *
 *
 *      ----------------------------------性能分析-慢查询日志---------------------------
 *
 *      慢查询日志记录了所有执行时间超过指定参数（默认10秒）的所有SQL语句的日志。(定位执行效率低的sql语句)
 *
 *      查看慢查询是否开启(OFF是关闭)
 *      SHOW VARIABLES LIKE 'slow_query_log';
 *
 *      如果要开启慢查询日志，需要在MySQL的配置文件（/etc/my.cnf）中配置如下信息：
 *      # 开启MySQL慢日志查询开关
 *      slow_query_log=1
 *      # 设置慢日志的时间为2秒，SQL语句执行时间超过2秒，就会视为慢查询，记录慢查询日志
 *      long_query_time=2
 *
 *      配置完毕之后，通过以下指令重新启动MySQL服务器进行测试
 *      systemctl restart mysqld
 *
 *      查看慢日志文件中记录的信息   在路径     /var/lib/mysql/localhost-slow.log。
 *
 *
 *      ----------------------------------性能分析-profiles---------------------------
 *
 *      show profiles 能够在做SQL优化时帮助我们了解时间都耗费到哪里去了。
 *
 *      查看当前MySQL是否支持profile操作
 *      SELECT @@have_profiling ;
 *
 *      查看profiling是否开启(1为开启，0为关闭)
 *      SELECT @@profiling;
 *
 *      可以通过set语句在session/global级别开启profiling：
 *      SET [session|global] profiling = 1;
 *
 *      -- 查看每一条SQL的耗时基本情况
 *      show profiles;
 *      -- 查看指定query_id的SQL语句各个阶段的耗时情况
 *      show profile for query query_id;
 *      -- 查看指定query_id的SQL语句CPU的使用情况
 *      show profile cpu for query query_id;
 *
 *      ----------------------------------性能分析-explain---------------------------
 *
 *      EXPLAIN 或者 DESC命令获取 MySQL 如何执行 SELECT 语句的信息，包括在 SELECT 语句执行过程中表
 *      如何连接和连接的顺序。
 *
 *      -- 直接在select语句之前加上关键字 explain / desc
 *      EXPLAIN SELECT 字段列表 FROM 表名 WHERE 条件 ;
 *
 *      字段                              含义
 *      id               select查询的序列号，表示查询中执行select子句或者是操作表的顺序
 *                       (id相同，执行顺序从上到下；id不同，值越大，越先执行)。
 *
 *      select_type      表示 SELECT 的类型，常见的取值有 SIMPLE（简单表，即不使用表连接或者子查询）、
 *                       PRIMARY（主查询，即外层的查询）、UNION（UNION 中的第二个或者后面的查询语句）、
 *                       SUBQUERY（SELECT/WHERE之后包含了子查询）等 (意义不大)
 *
 *      type             表示连接类型，性能由好到差的连接类型为NULL、system、const、eq_ref、ref、
 *                       range、 index、all
 *
 *      possible_key     显示可能应用在这张表上的索引，一个或多个。
 *
 *      key              实际使用的索引，如果为NULL，则没有使用索引。
 *
 *      key_len          表示索引中使用的字节数， 该值为索引字段最大可能长度，并非实际使用长度，
 *                       在不损失精确性的前提下， 长度越短越好 。
 *
 *      rows             MySQL认为必须要执行查询的行数，在innodb引擎的表中，是一个估计值，可能并不总是准确的。
 *
 *      filtered         表示返回结果的行数占需读取行数的百分比， filtered 的值越大越好。
 *
 *
 *
 *     id 操作表的顺序     possible_key 可能使用的索引      key 实际用的索引
 *
 *     type 性能差到性能好优化，比较重要的指标
 *     不查询任何表为null    访问系统表为system     主键和唯一索引访问出现const     非唯一索引出现ref
 *     index 表示用了索引但也会对索引进行扫描遍历
 *
 *     key 实际使用的索引     key_len 索引中使用的字节数    filtered 返回结果的行数占需读取行数的百分比
 *
 */

}
