package top.mysql.SQL;

public class Transaction {

/**
 *    事务：
 *    事务 是一组操作的集合，它是一个不可分割的工作单位，事务会把所有的操作作为一个整体一起向系统提交或撤销操作请求，
 *    即这些操作要么同时成功，要么同时失败。
 *
 *
 *    SELECT @@autocommit ;             --查看事务提交方式
 *    SET @@autocommit = 0 ;            --设置事务提交方式
 *
 *    START TRANSACTION 或 BEGIN ;      --开启事务
 *
 *    COMMIT;                           --提交事务
 *    ROLLBACK;                         --回滚事务
 *
 *    注意：
 *    上述的这种方式，我们是修改了事务的自动提交行为, 把默认的自动提交修改为了手动提交, 此时我们执行的DML语句
 *    都不会提交, 需要手动的执行commit进行提交。
 *
 *
 *    事务四大特性(简称ACID)：
 *      原子性（Atomicity）：事务是不可分割的最小操作单元，要么全部成功，要么全部失败。
 *      一致性（Consistency）：事务完成时，必须使所有的数据都保持一致状态。
 *      隔离性（Isolation）：数据库系统提供的隔离机制，保证事务在不受外部并发操作影响的独立环境下运行。
 *      持久性（Durability）：事务一旦提交或回滚，它对数据库中的数据的改变就是永久的。
 *
 *
 *    并发事务问题：
 *       赃读：一个事务读到另外一个事务还没有提交的数据。
 *       不可重复读：一个事务先后读取同一条记录，但两次读取的数据不同，称之为不可重复读。
 *                (同样的sql在一个事务中查询出来的数据不一致)
 *       幻读：一个事务按照条件查询数据时，没有对应的数据行，但是在插入数据时，又发现这行数据已经存在。
 *
 *
 *                            脏读    不可重复读   幻读
 *      Read uncommitted        √       √        √
 *      Read committed          ×       √        √
 *      Repeatable Read(默认)    ×       ×        √
 *      Serializable            ×       ×        ×
 *
 *      查看事务隔离级别
 *      SELECT @@TRANSACTION_ISOLATION;
 *
 *      设置事务隔离级别
 *      SET [ SESSION | GLOBAL ] TRANSACTION ISOLATION LEVEL 隔离级别 ;
 *
 *      注意：事务隔离级别越高，数据越安全，但是性能越低。
 *
 */

}
