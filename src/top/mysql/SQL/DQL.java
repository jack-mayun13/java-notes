package top.mysql.SQL;

public class DQL {

/**
 *      DQL英文全称是Data Query Language(数据查询语言)，数据查询语言，用来查询数据库中表的记录。
 *
 *      查询关键字: SELECT
 *
 *
 *      DQL 查询语句，语法结构如下：
 *
 *      SELECT
 *          字段列表
 *      FROM
 *          表名列表
 *      WHERE
 *          条件列表
 *      GROUP BY
 *          分组字段列表
 *      HAVING
 *          分组后条件列表
 *      ORDER BY
 *          排序字段列表
 *      LIMIT
 *          分页参数
 *
 *      去除重复记录
 *      SELECT DISTINCT 字段列表 FROM 表名;
 *
 *      比较运算符                   功能
 *          >                       大于
 *          >=                      大于等于
 *          <                       小于
 *          <=                      小于等于
 *          =                       等于
 *          <> 或 !=                 不等于
 *          BETWEEN ... AND ...     在某个范围之内(含最小、最大值)
 *          IN(...)                 在in之后的列表中的值，多选一
 *          LIKE                    占位符 模糊匹配(_匹配单个字符, %匹配任意个字符)
 *          IS NULL                 是NULL
 *
 *      条件查询：
 *      SELECT 字段列表 FROM 表名 WHERE 条件列表 ;
 *
 *      逻辑运算符                   功能
 *      AND 或 &&                    并且 (多个条件同时成立)
 *      OR 或 ||                     或者 (多个条件任意一个成立)
 *      NOT 或 !                     非 , 不是
 *
 *      聚合函数                    功能
 *      count                       统计数量
 *      max                         最大值
 *      min                         最小值
 *      avg                         平均值
 *      sum                         求和
 *
 *      SELECT 聚合函数(字段列表) FROM 表名 ;
 *
 *      分组查询：
 *      SELECT 字段列表 FROM 表名 [ WHERE 条件 ] GROUP BY 分组字段名 [ HAVING 分组后过滤条件 ];
 *
 *      where与having区别
 *          执行时机不同：where是分组之前进行过滤，不满足where条件，不参与分组；而having是分组之后对结果进行过滤。
 *          判断条件不同：where不能对聚合函数进行判断，而having可以。
 *
 *      注意事项:
 *          分组之后，查询的字段一般为聚合函数和分组字段，查询其他字段无任何意义
 *          执行顺序: where > 聚合函数 > having
 *          支持多字段分组, 具体语法为 : group by columnA,columnB
 *
 *      排序查询：
 *      SELECT 字段列表 FROM 表名 ORDER BY 字段1 排序方式1 , 字段2 排序方式2 ;
 *
 *          ASC : 升序(默认值)
 *          DESC: 降序
 *
 *      分页查询：
 *      SELECT 字段列表 FROM 表名 LIMIT 起始索引, 查询记录数 ;
 *
 *      注意事项:
 *          起始索引从0开始，起始索引 = （查询页码 - 1）* 每页显示记录数。
 *          如果查询的是第一页数据，起始索引可以省略，直接简写为 limit 10。
 *
 *      DQL执行顺序：
 *          from        表名列表
 *          where       条件列表
 *          group by    分组字段列表
 *          having      分组后条件列表
 *          select      字段列表
 *          order by    排序字段列表
 *          limit        分页参数
 */

}
