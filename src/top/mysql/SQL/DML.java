package top.mysql.SQL;

public class DML {

/**
 *  DML英文全称是Data Manipulation Language(数据操作语言)，用来对数据库中表的数据记录进行增、删、改操作。
 *
 *   添加数据（INSERT）
 *   修改数据（UPDATE）
 *   删除数据（DELETE）
 *
 *   给指定字段添加数据
 *   INSERT INTO 表名(字段名1, 字段名2, ...) VALUES(值1, 值2, ...);
 *
 *   给全部字段添加数据
 *   INSERT INTO 表名 VALUES (值1, 值2, ...);
 *
 *   批量添加数据
 *   INSERT INTO 表名(字段名1, 字段名2, ...) VALUES(值1, 值2, ...),(值1, 值2, ...),(值1, 值2, ...);
 *   INSERT INTO 表名 VALUES (值1, 值2, ...), (值1, 值2, ...), (值1, 值2, ...) ;
 *
 *   注意事项:
 *       插入数据时，指定的字段顺序需要与值的顺序是一一对应的。
 *       字符串和日期型数据应该包含在引号中。
 *       插入的数据大小，应该在字段的规定范围内。
 *
 *   修改数据的具体语法为:
 *      UPDATE 表名 SET 字段名1 = 值1 , 字段名2 = 值2 , .... [ WHERE 条件 ] ;
 *
 *   注意事项:
 *      修改语句的条件可以有，也可以没有，如果没有条件，则会修改整张表的所有数据。
 *
 *   删除数据的具体语法为：
 *      DELETE FROM 表名 [ WHERE 条件 ] ;
 */

}
