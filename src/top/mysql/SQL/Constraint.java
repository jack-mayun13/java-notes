package top.mysql.SQL;

public class Constraint {

/**
 *
 *      概念：约束是作用于表中字段上的规则，用于限制存储在表中的数据。
 *      目的：保证数据库中数据的正确、有效性和完整性。
 *
 *      非空约束 限制该字段的数据不能为null                                NOT NULL
 *      唯一约束 保证该字段的所有数据都是唯一、不重复的                       UNIQUE
 *      主键约束 主键是一行数据的唯一标识，要求非空且唯一                     PRIMARY KEY
 *      默认约束 保存数据时，如果未指定该字段的值，则采用默认值                DEFAULT
 *      检查约束(8.0.16版本之后)    保证字段值满足某一个条件                CHECK(检查条件)
 *      外键约束用来让两张表的数据之间建立连接，保证数据的一致性和完整性         FOREIGN  KEY
 *
 *      有外键的表为子表，外键所关联的主键的表为父表
 *
 *      添加外键
 *      CREATE TABLE 表名(
 *          字段名 数据类型,
 *          ...
 *          [CONSTRAINT] [外键名称] FOREIGN KEY (外键字段名) REFERENCES 主表 (主表列名)
 *      );
 *
 *      ALTER TABLE 表名 ADD CONSTRAINT 外键名称 FOREIGN KEY (外键字段名) REFERENCES 主表 (主表列名) ;
 *
 *      删除外键
 *      ALTER TABLE 表名 DROP FOREIGN KEY 外键名称;
 */
}
