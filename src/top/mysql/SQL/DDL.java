package top.mysql.SQL;


public class DDL {

/**
 *      DDL   Data Definition Language    数据定义语言，用来定义数据库对象(数据库，表，字段)
 *
 *      DDL的数据库操作
 *
 *      查询所有已有数据库
 *      show databases;
 *
 *      查询当前数据库(在那个数据库中)
 *      select database();
 *
 *      创建数据库
 *      create database [ if not exists ] 数据库名 [ default charset 字符集 ] [ collate 排序规则 ];
 *
 *      删除数据库
 *      drop database [ if exists ] 数据库名;
 *
 *      切换数据库
 *      use 数据库名 ;
 *
 *
 *
 *      DDL的表操作
 *
 *      查询当前数据库所有表
 *      show tables;
 *
 *      查看指定表结构
 *      desc 表名 ;
 *
 *      查询指定表的建表语句
 *      show create table 表名;
 *
 *
 *      创建表结构
 *      CREATE TABLE 表名(
 *      字段1 字段1类型 [ COMMENT 字段1注释 ],
 *      字段2 字段2类型 [COMMENT 字段2注释 ],
 *      字段3 字段3类型 [COMMENT 字段3注释 ],
 *      ......
 *      字段n 字段n类型 [COMMENT 字段n注释 ]             //注意最后一个字段没有逗号
 *      ) [ COMMENT 表注释 ] ;
 *
 *
 *      MySQL中的数据类型
 *
 *      MySQL中的数据类型有很多，主要分为三类：数值类型、字符串类型、日期时间类型。
 *
 *      类型              大小              有符合范围           无符号范围
 *
 *      整型：
 *      tinyInt         1byte           (-128，127)          (0，255)
 *      smallInt        2bytes          (-32768，32767)      (0，65535)
 *      mediumInt       3bytes
 *      int/integer     4bytes
 *      bigInt          8bytes
 *
 *      无符号加：unsigned           age tinyInt unsigned
 *
 *      浮点型：
 *      float           4bytes
 *      double          8bytes
 *
 *      任意精度浮点型
 *      decimal           依赖于M(精度)和D(标度)的值
 *
 *      字符串类型：
 *      char            0-255 bytes         定长字符串(需要指定长度)
 *      varChar         0-65535 bytes       变长字符串(需要指定长度)
 *
 *      日期时间类型：
 *      date            3 bytes       YYYY-MM-DD          日期值
 *      time            3 bytes       HH:MM:SS            时间值或持续时间
 *      year            1 bytes       YYYY                年份值
 *      dateTime        8 bytes       YYYY-MM-DDHH:MM:SS  混合日期和时间值
 *      timeStamp       4 bytes       YYYY-MM-DDHH:MM:SS  混合日期和时间值，时间戳(截止到2038-01-19 )
 *
 *
 *      添加字段
 *      ALTER TABLE 表名 ADD 字段名 类型 (长度) [ COMMENT 注释 ] [ 约束 ];
 *
 *      修改数据类型
 *      ALTER TABLE 表名 MODIFY 字段名 新数据类型 (长度);
 *
 *      修改字段名和字段类型
 *      ALTER TABLE 表名 CHANGE 旧字段名 新字段名 类型 (长度) [ COMMENT 注释 ] [ 约束 ];
 *
 *      删除字段
 *      ALTER TABLE 表名 DROP 字段名;
 *
 *      修改表名
 *      ALTER TABLE 表名 RENAME TO 新表名;
 *
 *      删除表
 *      DROP TABLE [ IF EXISTS ] 表名;
 *
 *      删除指定表, 并重新创建表
 *      TRUNCATE TABLE 表名;
 */

/**
 *      总结：
 *
 *      DDl 数据库操作
 *
 *      show databases;                     查询所有已有数据库
 *      create databases 数据库名;            创建数据库
 *      user 数据库名;                        指定数据库
 *      select databases();                 查询当前数据库(在那个数据库中)
 *      drop databases 数据库名;              删除数据库
 *
 *      DDl 表操作
 *
 *      show tables;                                            查询当前数据库所有表
 *      create tables 表名(字段 字段类型，字段 字段类型);              创建表结构
 *      desc 表名;                                               查看指定表结构
 *      show create table 表名;                                  查询指定表的建表语句
 *
 *      alter table 表名 add/modify/change/drop/rename to...;
 *                                            添加字段/修改数据类型/修改字段名和字段类型/删除字段/修改表名
 *
 *      drop table 表名;                                         删除表
 *
 */
}
