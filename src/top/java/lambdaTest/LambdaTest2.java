package top.java.lambdaTest;

import org.junit.Test;

/**
 *    Lambda表达式的使用
 *
 *    举例：   (o1,o2) -> Integer.compare(o1,o2)
 *    格式：   -> ：lambda操作符 或 箭头操作符
 *            -> 左边：lambda形参列表(就是接口中抽象方法的形参列表)
 *            -> 右边：lambda体(就是重写的抽象方法的方法体)
 *
 *     lambda：本质就是作为接口的对象
 *
 *    使用：
 *
 *    总结六种情况：
 *   ->左边：lambda形参列表的参数类型可以省略(类型推断)；如果lambda形参列表只一个参数，其一对()也可以省略
 *   ->右边：lambda体应该使用一对{}包裹；如果lambda体只一条执行语句（可能是return语句，省略这一对{}和return关键字
 */
public class LambdaTest2 {

    /**
     *     语法格式一：无参，无返回值
     *     语法格式二：Lambda 需要一个参数，但是没有返回值。
     *     语法格式三：数据类型可以省略，因为可由编译器推断得出，称为“类型推断”
     *     语法格式四：Lambda 若只需要一个参数时，参数的小括号可以省略
     *     语法格式六：当 Lambda 体只有一条语句时，return 与大括号若有，都可以省略
     *     语法格式五：Lambda 需要两个或以上的参数，多条执行语句，并且可以有返回值
     */
    @Test
    public void test1() {

    }

}
