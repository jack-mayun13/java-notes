package top.java.lambdaTest;

import org.junit.Test;

import java.util.Comparator;

/**
 *          Lambda  表达式
 */
public class LambdaTest1 {

    @Test
    public void test1() {

        /**
         *      匿名写法
         */
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("111111111111111111");
            }
        };
        r1.run();

        System.out.println("*******************");

        /**
         *    Lambda表达式写法
         */
        Runnable r2 = () -> System.out.println("222222222222222222");
        r2.run();
    }

    @Test
    public void test2() {

        Comparator<Integer> c1 = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };
        int compare = c1.compare(42, 58);
        System.out.println(compare);

        System.out.println("*******************");

        //lambda表达式的写法
        Comparator<Integer> c2 = (o1,o2) -> Integer.compare(o1,o2);
        int compare1 = c2.compare(89, 13);
        System.out.println(compare1);

        System.out.println("*******************");

        //方法引用
        Comparator<Integer> c3 = Integer::compare;
        int compare2 = c3.compare(89, 13);
        System.out.println(compare2);
    }

}
