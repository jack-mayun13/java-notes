package top.java.reflectionTest;


import org.junit.Test;

/**
 *          通过反射创建对应的运行时类的对象
 */
public class ReflectionTest3 {

    /**
     *      newInstance()：调用此方法，创建对应的运行时类的对象
     *
     *      只有构造器能造对象，此方法是封装调用了运行时类的空参构造器
     *
     *      要想此方法正常的创建运行时类的对象，要求：
     *          1.运行时类必须提供空参的构造器
     *          2.空参的构造器访问权限得满足，通常为public
     *
     *      在javaBean中要求提供一个public的空参构造器
     *          1.便于通过反射，创建运行时类的对象
     *          2.便于子类继承此运行时类时，默认调用super()时，保证父类有此构造器
     *
     */
    @Test
    public void test1() throws Exception {

        //不带泛型
        Class class1 = Person.class;
        Object o = class1.newInstance();
        Person p1 = (Person) o;
        System.out.println(p1);

        //带泛型
        Class<Person> class2 = Person.class;
        Person p2 = class2.newInstance();
        System.out.println(p2);                 //Person{name='null', age=0}
    }


}
