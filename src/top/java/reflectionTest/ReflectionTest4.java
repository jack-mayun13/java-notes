package top.java.reflectionTest;


import org.junit.Test;

import java.util.Random;

/**
 *          举例体会反射的动态性
 *
 *          动态语言：是一类在运行时可以改变其结构的语言，通俗点说就是在运行时代码可以根据某些条件改变自身结构。
 *
 *          静态语言：与动态语言相对应的，运行时结构不可变的语言就是静态语言。
 */
public class ReflectionTest4 {

    @Test
    public void test1() {
        int i = new Random().nextInt(3);
        String classPath = "";
        switch (i) {
            case 0:
                classPath = "java.util.Date";
                break;
            case 1:
                classPath = "java.sql.Date";//java.sql.Date没有空参构造器，抛InstantiationException
                break;
            case 2:
                classPath = "top.stmo.reflectionTest.Person";
                break;
        }
        Object instance = null;
        try {
            instance = getInstance(classPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(instance);
    }

    /**
     *  创建一个指定类的对象  classPath：指定类的权类名
     */
    public Object getInstance(String classPath) throws Exception {
        Class classPath1 = Class.forName(classPath);
        return classPath1.newInstance();
    }
}
