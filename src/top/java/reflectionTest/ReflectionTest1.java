package top.java.reflectionTest;


import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 *    加载完类之后，在堆内存的方法区中就产生了一个Class类型的对象（一个类只有一个Class对象），
 *    这个对象就包含了完整的类的结构信息。我们可以通过这个对象看到类的结构。
 *
 *    Java反射机制提供的功能：
 *      在运行时判断任意一个对象所属的类
 *      在运行时构造任意一个类的对象
 *      在运行时判断任意一个类所具有的成员变量和方法
 *      在运行时获取泛型信息
 *      在运行时调用任意一个对象的成员变量和方法
 *      在运行时处理注解
 *      生成动态代理
 *
 *      反射相关的主要API：
 *          java.lang.Class:代表一个类
 *          java.lang.reflect.Method:代表类的方法
 *          java.lang.reflect.Field:代表类的成员变量
 *          java.lang.reflect.Constructor:代表类的构造器
 */
public class ReflectionTest1 {

    /**
     *      反射之前，对于Person的操作
     *
     *      在Person类外部，不可以通过Person类的对象调用其内部私有的结构
     */
    @Test
    public void test1() {
        //创建Person对象
        Person p1 = new Person("Tom",12);
        //通过对象，调用其内部的属性，方法
        p1.age = 10;
        System.out.println(p1);     //Person{name='Tom', age=10}
        p1.show();                  //你好~~~~~~~~~~~
    }

    /**
     *      使用反射，对于Person的操作
     */
    @Test
    public void test2() throws Exception {
        //通过反射创建Person类的对象
        Class aClass = Person.class;
        Constructor cons = aClass.getConstructor(String.class,int.class);
        Object tom = cons.newInstance("Tom", 12);
        Person p1 = (Person) tom;
        System.out.println(p1);
        //通过反射，调用对象指定的属性，方法
        Field age = aClass.getDeclaredField("age");
        age.set(p1,10);
        System.out.println(p1);

        //调用方法
        Method show = aClass.getDeclaredMethod("show");
        show.invoke(p1);

        System.out.println("*********************");

        //通过反射，可以调用Person类的私有结构，比如：私有的构造器、方法、属性
        //调用私有的构造器
        Constructor cons1 = aClass.getDeclaredConstructor(String.class);
        cons1.setAccessible(true);
        Object jerry = cons1.newInstance("Jerry");
        Person p2 = (Person) jerry;
        System.out.println(p2);                 //Person{name='Jerry', age=0}
        //调用私有的属性
        Field name = aClass.getDeclaredField("name");
        name.setAccessible(true);
        name.set(p2,"Han");
        System.out.println(p2);

        //调用私有的方法
        Method showNation = aClass.getDeclaredMethod("showNation", String.class);
        showNation.setAccessible(true);
        Object nation = showNation.invoke(p2, "中国");
        System.out.println(((String)nation));

    }
}

class Person {
    private String name;
    public int age;

    public Person() {
    }

    private Person(String name) {
        this.name = name;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void show() {
        System.out.println("你好~~~~~~~~~~~");
    }

    private String showNation(String nation) {
        String str = "国籍是：" + nation;
        System.out.println(str);
        return str;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}