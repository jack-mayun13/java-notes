package top.java.InetAddressTest;


import org.junit.Test;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *   实现TCP的网络编程
 *
 *   例题：客户端发送文件给服务端，服务端将文件保存在本地,并返回发送成功给客户端
 */
public class TCPTest3 {


    //客户端
    @Test
    public void client() {

        FileInputStream fis = null;
        Socket socket = null;
        OutputStream os = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;
        try {
            fis = new FileInputStream(new File("hi.txt"));
            InetAddress ia = InetAddress.getByName("127.0.0.1");
            socket = new Socket(ia,8899);
            os = socket.getOutputStream();
            byte[] bytes = new byte[10];
            int len;
            while ((len = fis.read(bytes)) != -1) {
                os.write(bytes,0,len);
            }

            socket.shutdownOutput();      //关闭数据的输出

            //接收来自服务器端的数据，并显示到控制台
            is = socket.getInputStream();
            baos = new ByteArrayOutputStream();
            while ((len = is.read(bytes)) != -1) {
                baos.write(bytes,0,len);
            }
            System.out.println(baos);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (baos != null) {
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    //服务端
    @Test
    public void server() {

        ServerSocket ss = null;
        Socket socket = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;
        FileWriter fw = null;
        OutputStream os = null;
        try {
            ss = new ServerSocket(8899);
            socket = ss.accept();
            is = socket.getInputStream();
            byte[] bytes = new byte[10];
            int len;
            baos = new ByteArrayOutputStream();
            while ((len = is.read(bytes)) != -1) {
                baos.write(bytes,0,len);
            }
            System.out.println("图片已传输完成");

            fw = new FileWriter(new File("hi.txt2"));
            fw.write(baos.toString());

            //服务器端给于客户端反馈
            os = socket.getOutputStream();
            os.write("服务器端已接收成功".getBytes());


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
           if (baos != null) {
               try {
                   baos.close();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
          if (socket != null) {
              try {
                  socket.close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
         if (ss != null) {
             try {
                 ss.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
