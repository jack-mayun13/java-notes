package top.java.InetAddressTest;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *   实现TCP的网络编程
 *
 *   例题：客户端发送信息给服务端，服务端将数据显示在控制台上
 *
 *      客户端：
 *            1.创建Socket对象，指明服务器端的ip和端口号
 *            2.获取一个输出流，用于输出数据
 *            3.写入数据的操作
 *            4.资源的关闭
 *      服务器端：
 *            1.创建ServerSocket，指明自己的端口号
 *            2.调用accept()  表面接收来自客户端的socket
 *            3.获取一个输入流
 *            4.读取输入流中的数据
 *            5.资源的关闭
 */
public class TCPTest1 {

    //客户端
    @Test
    public void client() {
        Socket socket = null;
        OutputStream os = null;
        try {
            //1.创建Socket对象，指明服务器端的ip和端口号
            InetAddress inet = InetAddress.getByName("127.0.0.1");
            socket = new Socket(inet, 8889);
            //2.获取一个输出流，用于输出数据
            os = socket.getOutputStream();
            //3.写入数据的操作
            os.write("你好，我是客户端".getBytes());
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            //4.资源的关闭
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    //服务端
    @Test
    public void server() {

        ServerSocket serverSocket = null;
        Socket socket = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;
        try {
            //1.创建ServerSocket，指明自己的端口号
            serverSocket = new ServerSocket(8889);
            //2.调用accept()  表明接收来自客户端的socket
            socket = serverSocket.accept();
            //3.获取一个输入流
            is = socket.getInputStream();

        //不建议这样写，可能有乱码  byte数组为5，一个中文字符占3个，将剩下两个不构成中文字符的转换成字符串
//        byte[] bytes = new byte[10];
//        int len;
//        while ((len = is.read(bytes)) != -1) {
//            String s = new String(bytes, 0, len);
//            System.out.print(s);
//        }
            //4.读取输入流中的数据
            baos = new ByteArrayOutputStream();
            byte[] bytes = new byte[5];
            int len;
            while ((len = is.read(bytes)) != -1) {
                baos.write(bytes,0,len);
            }
            System.out.println(baos.toString());

            System.out.println("收到数据来自："+socket.getInetAddress().getHostAddress());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //5.资源的关闭
            try {
                if (baos != null) {
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (serverSocket != null) {
                    serverSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
