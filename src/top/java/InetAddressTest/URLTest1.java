package top.java.InetAddressTest;


import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *      URL：统一资源定位符，它表示 Internet 上某一资源的地址
 *
 *      URL的基本结构由5部分组成：
 *          <传输协议>://<主机名>:<端口号>/<文件名>#片段名?参数列表
 *
 *          片段名：即锚点，例如看小说，直接定位到章节
 *          参数列表格式：参数名=参数值&参数名=参数值....
 */
public class URLTest1 {

    /**
     *      URL类常用方法：
     *
     *      public String getProtocol( ) 获取该URL的协议名
     *      public String getHost( ) 获取该URL的主机名
     *      public String getPort( ) 获取该URL的端口号
     *      public String getPath( ) 获取该URL的文件路径
     *      public String getFile( ) 获取该URL的文件名
     *      public String getQuery( ) 获取该URL的查询名
     */
    @Test
    public void test1() throws MalformedURLException {
        URL url = new URL("http://localhost:8080/examples/ttt.png");
        System.out.println(url.getProtocol());
        System.out.println(url.getHost());
        System.out.println(url.getPort());
        System.out.println(url.getPath());
        System.out.println(url.getFile());
        System.out.println(url.getQuery());
    }

    @Test
    public void test2() throws IOException {

        URL url = new URL("http://localhost:8080/examples/ttt.png");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.connect();
        InputStream inputStream = urlConnection.getInputStream();
        //获取到流然后操作...
        inputStream.close();
    }

}
