package top.java.InetAddressTest;


import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *      网络编程的目的：
 *          直接或间接地通过网络协议与其它计算机实现数据交换，进行通讯。
 *
 *      网络编程中有两个主要的问题：
 *          问题一：如何准确地定位网络上一台或多台主机；定位主机上的特定的应用
 *          问题一：找到主机后如何可靠高效地进行数据传输
 *
 *      网络编程中的两个要素：
 *          IP和端口号      (对应问题一)
 *          网络通信协议：应用层、传输层、网络层、物理+数据链路层    (对应问题二)
 *
 *      通信要素1：IP 和 端口号
 *          唯一的标识 Internet 上的计算机（通信实体）
 *
 *      端口号：正在计算机上运行的进程
 *      要求：不同的进程有不同的端口号
 *      范围：被规定为一个 16 位的整数 0~65535
 *
 *      端口号与IP地址的组合得出一个网络套接字：Socket。
 *
 *      TCP/IP 以其两个主要协议：传输控制协议(TCP)和网络互联协议(IP)
 *
 *      TCP协议：
 *          使用TCP协议前，须先建立TCP连接，形成传输数据通道
 *          传输前，采用“三次握手”方式，点对点通信，是可靠的
 *          TCP协议进行通信的两个应用进程：客户端、服务端。
 *          在连接中可进行大数据量的传输
 *          传输完毕，需释放已建立的连接，效率低
 *      UDP协议：
 *          将数据、源、目的封装成数据包，不需要建立连接
 *          每个数据报的大小限制在64K内
 *          发送不管对方是否准备好，接收方收到也不确认，故是不可靠的
 *          可以广播发送
 *          发送数据结束时无需释放资源，开销小，速度快
 *
 *       TCP 生活案例：打电话          UDP 生活案例：发短信
 */
public class InetAddressTest1 {

        /**
         *      如何实例化InetAddress：
         *              getByName()
         *              getLocalHost()          获取本地的
         *
         *      两个常用方法：
         *              getHostName()
         *              getHostAddress()
         */
        public static void main(String[] args) {
                try {
                        //通过ip地址实例化Ip地址
                        InetAddress inet1 = InetAddress.getByName("192.168.10.255");
                        System.out.println(inet1);

                        //通过域名实例化Ip地址
                        InetAddress inet2 = InetAddress.getByName("www.baidu.com");
                        System.out.println(inet2);

                        InetAddress inet3 = InetAddress.getByName("127.0.0.1");
                        System.out.println(inet3);

                        //获取本地的Ip地址
                        InetAddress inet4 = InetAddress.getLocalHost();
                        System.out.println(inet4);

                        System.out.println("*******************");

                        //通过Ip地址实例化对象获取域名
                        String hostName = inet2.getHostName();
                        System.out.println(hostName);

                        //通过实例化对象获取Ip地址
                        String hostAddress = inet2.getHostAddress();
                        System.out.println(hostAddress);

                } catch (UnknownHostException e) {
                        e.printStackTrace();
                }
        }

}
