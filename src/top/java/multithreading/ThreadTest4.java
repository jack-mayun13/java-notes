package top.java.multithreading;


import org.junit.Test;

/**
 *       线程安全问题
 *
 *       原因：
 *          当某个线程操作共享数据的过程中，尚未完成操作其他线程参与进来
 *       解决思路：
 *          当一个线程操作共享数据时，其他线程不能参与，直到此线程操作共享数据完成
 *          即使该线程出现阻塞也不能改变
 *       解决方法：
 *
 *          方式一：    同步代码块
 *          synchronized(同步监视器) {
 *              //需要被同步的代码
 *          }
 *
 *          被同步的代码：操作共享数据的代码，即为需要被同步的代码
 *          共享数据：多个线程共同操作的变量
 *          同步监视器：俗称，锁。     要求：任何一个类的对象都可以充当锁，多个线程必须要共用同一把锁
 *
 *          实现Runnable接口的方式可以直接用this当作同步监视器
 *          继承的方式Thread不可以直接用this当作同步监视器，可以用当前  类.class 当作同步监视器(Class class = Window5.class)
 */


public class ThreadTest4 {

    public static void main(String[] args) {

        Window4 window4 = new Window4();
        Thread thread = new Thread(window4);
        Thread thread2 = new Thread(window4);
        Thread thread3 = new Thread(window4);

        thread.setName("窗口1");
        thread2.setName("窗口2");
        thread3.setName("窗口3");

        thread.start();
        thread2.start();
        thread3.start();
        while (true) {
            if (!(thread.isAlive() && thread2.isAlive() && thread3.isAlive())) {
                break;
            }
        }
    }

    @Test
    public void test5() {
        Window5 window5 = new Window5();
        Window5 window6 = new Window5();
        Window5 window7 = new Window5();

        window5.setName("窗口一");
        window6.setName("窗口二");
        window7.setName("窗口三");

        window5.start();
        window6.start();
        window7.start();
        while (true) {
            if (!(window5.isAlive() && window6.isAlive() && window7.isAlive())) {
                break;
            }
        }
    }
}

class Window4 implements Runnable {

    private int ticket = 100;

    @Override
    public void run() {
       while (true) {
            synchronized (this) {
                if (ticket > 0) {
//                    try {
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    System.out.println(Thread.currentThread().getName() + "票号为：" + ticket);
                    ticket--;
                } else {
                    System.out.println("没票了");
                    break;
                }
            }
       }
    }
}


/**
 *              继承方式
 */

class Window5 extends Thread{

    private static int title = 100;
//    private static Object obj = new Object();

    @Override
    public void run() {
       while (true) {
            //        synchronized (obj) {
            synchronized (Window5.class) {
                if (title > 0) {
                    System.out.println(Thread.currentThread().getName() + "票号为：" + title);
                    title--;
                } else {
                    System.out.println("没票了");
                    break;
                }
            }
       }
    }
}
