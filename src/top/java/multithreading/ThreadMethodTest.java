package top.java.multithreading;

/**
 *          测试Thread类中的常用方法
 */


public class ThreadMethodTest {

    public static void main(String[] args) {

        Test2 t = new Test2();
        //启动当前线程，调用当前线程的run()方法
        t.start();
        //通常需要重写Thread中的此方法，创建线程要执行的操作声明在此方法中
        t.run();
        //静态方法，返回执行当前代码的线程
        Thread.currentThread();
        //获取当前线程名称
        t.getName();
        //设置当前线程名称
        t.setName("qqq");
        //释放当前cpu的执行权
        t.yield();
        //在线程a中调用线程b的join()，此时线程a就进入阻塞状态，直到线程b完全执行完以后，线程a才结束阻塞状态
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //当前线程睡眠指定毫秒数
        try {
            t.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //判断当前线程是否还存活
        t.isAlive();
    }
}

class Test2 extends Thread {

}
