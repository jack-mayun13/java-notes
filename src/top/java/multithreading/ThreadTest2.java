package top.java.multithreading;


/**
 *          线程的优先级
 *
 *          getPriority();
 *          setPriority();
 *
 *          Thread中的优先级常量
 *
 *          MIN_PRIORITY = 1;
 *
 *          NORM_PRIORITY=5;            默认值
 *
 *          MAX_PRIORITY=10;
 */

public class ThreadTest2 {

    public static void main(String[] args) {

        Test3 test3 = new Test3();
        int i = test3.getPriority();
        System.out.println(i);
        test3.setPriority(Thread.MAX_PRIORITY);
        int priority = test3.getPriority();
        System.out.println(priority);
    }

}

class Test3 extends Thread {
    @Override
    public void run() {
        super.run();
    }
}
