package top.java.multithreading;

/**
 *          线程的通信       例题：两个线程打印1-100，交替打印
 *
 *          wait()          一旦执行此方法，当前线程就会进入阻塞状态，并释放同步监视器
 *          notify()        一旦执行此方法，就会唤醒被wait的一个线程，如果有多个线程被wait，就唤醒优先级高的那个
 *          notifyAll()     一旦执行此方法，就会唤醒所有被wait的线程
 *
 *          注意：
 *              1.这三个方法必须使用在同步代码块或同步方法中
 *              2.这三个方法的调用者必须是同步代码块或者同步方法中的同步监视器(不写默认为this)
 *                否则会出现 IllegalMonitorStateException 异常
 *              3.这三个方法是定义在Object类中
 *
 *
 *        sleep()  和    wait()  的异同
 *        相同点：一旦执行，都可以使当前线程进入阻塞状态
 *              都需要处理异常
 *        不同点：sleep()声明在Thread中，wait()声明在Object中
 *              sleep()可以在任何需要的场景下调用，wait()只能在同步代码块或者同步方法中使用
 *              如果两个方法都使用在同步代码块或者同步方法中，sleep()不会释放同步监视器，wait()释放同步监视器
 *
 */

public class ThreadTest9 {
    public static void main(String[] args) {
        Number number = new Number();
        Thread t1 = new Thread(number);
        Thread t2 = new Thread(number);
        t1.setName("线程1");
        t2.setName("线程2");
        t1.start();
        t2.start();
    }
}

class Number implements Runnable {

    private int number = 1;

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                notify();
//                notifyAll();
                if (number <= 100) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "：" + number);
                    number++;
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    break;
                }
            }
        }
    }
}


