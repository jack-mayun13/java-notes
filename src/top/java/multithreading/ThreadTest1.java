package top.java.multithreading;

public class ThreadTest1 {

    /**
     *          Thread的匿名子类
     */

    public static void main(String[] args) {

        new Thread(){
            @Override
            public void run() {
                while (true) {
                    System.out.println("11111111" + Thread.currentThread().getName());
                }
            }
        }.start();

        new Thread(){
            @Override
            public void run() {
                while (true) {
                    System.out.println("22222222" + Thread.currentThread().getName());
                }
            }
        }.start();

    }

}



