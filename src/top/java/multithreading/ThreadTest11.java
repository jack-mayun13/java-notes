package top.java.multithreading;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 *        多线程的创建      方式三：Callable接口       --JDK 5.0新增
 *
 *        1.创建一个实现了Callable接口的实现类
 *        2.实现类去实现Callable中的抽象方法  call()    将此线程需要执行的操作声明在call()中
 *        3.创建Callable实现类的对象，将此对象作为参数传递到FutureTask类的构造器中
 *        4.创建FutureTask类的对象，将此对象作为参数传递到Thread类的构造器中，创建Thread类的对象
 *        5.通过Thread类的对象调用start()
 *        6.如果需要返回值：通过FutureTask类的对象调用get()方法获取call()方法的返回值
 *
 *        如何理解 实现Callable接口创建多线程的方式 比 实现Callable接口创建多线程的方式 强大
 *        1.call()方法可以有返回值(借助FutureTask类)
 *        2.call()可以抛出异常，被外面的操作捕获，获取异常信息
 *        3.Callable是支持泛型的
 */

public class ThreadTest11 {
    public static void main(String[] args) {
        NumThread numThread = new NumThread();
        FutureTask futureTask = new FutureTask(numThread);
        Thread t1 = new Thread(futureTask);
        t1.start();

        try {
            Object num = futureTask.get();   //get()返回值即为FutureTask构造器参数实现类重写call()的返回值
            System.out.println("总和为：" + num);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}

class NumThread implements Callable {

    @Override
    public Object call() throws Exception {
        int num = 0;
        for (int i = 0;i <= 100;i++) {
            if (i % 2 == 0) {
                System.out.println(i);
                num += i;
            }
        }
        return num;
    }
}
