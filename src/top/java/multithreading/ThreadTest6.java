package top.java.multithreading;

/**
 *          演示线程的死锁问题
 *
 *          不同线程分别占用对方需要的同步资源不放弃
 *          都在等待对方放弃自己需要的同步资源，就形成线程的死锁
 *
 *          在嵌套同步中，用两个不同类的对象作为同步监视器，一个线程先调a后调b，一个线程先调b后调a，出现死锁问题
 *
 *          如何避免死锁：
 *              专门的算法(避开一个先调a后调b，一个先调b后调a)
 *              尽量减少同步资源的定义
 *              尽量避免嵌套同步
 */

public class ThreadTest6 {

    public static void main(String[] args) {

        StringBuffer s1 = new StringBuffer();
        StringBuffer s2 = new StringBuffer();

        new Thread(){
            @Override
            public void run() {
                synchronized (s1) {
                    s1.append("a");
                    s2.append("1");

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    synchronized (s2) {
                        s1.append("b");
                        s2.append("2");
                        System.out.println(s1);
                        System.out.println(s2);
                    }
                }
            }
        }.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (s2) {
                    s1.append("c");
                    s2.append("3");

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    synchronized (s1) {
                        s1.append("d");
                        s2.append("4");
                        System.out.println(s1);
                        System.out.println(s2);
                    }
                }
            }
        }).start();

    }

}
