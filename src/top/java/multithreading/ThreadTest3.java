package top.java.multithreading;

import org.junit.Test;

/**
 *          卖票问题
 */


public class ThreadTest3 {

    public static void main(String[] args) {

        Window w1 = new Window();
        Window w2 = new Window();
        Window w3 = new Window();

        w1.setName("窗口1");
        w2.setName("窗口2");
        w3.setName("窗口3");

        w1.start();
        w2.start();
        w3.start();
    }

    @Test
    public void test2() {
        Window2 window2 = new Window2();
        Thread thread = new Thread(window2);
        Thread thread2 = new Thread(window2);
        Thread thread3 = new Thread(window2);
        thread.setName("窗口1");
        thread2.setName("窗口2");
        thread3.setName("窗口3");
        thread.start();
        thread2.start();
        thread3.start();
        while (true) {

        }
    }

}

class Window extends Thread {

    private static int total = 100;

    @Override
    public void run() {
        while (true) {
            if (total > 0) {
                System.out.println(getName() + "票号为：" + total);
                total--;
            }else {
                System.out.println("没票了");
                break;
            }
        }
    }
}


class Window2 implements Runnable {

    private int ticket = 100;

    @Override
    public void run() {
        while (true) {
            if (ticket > 0) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "票号为：" + ticket);
                ticket--;
            }else {
                System.out.println("没票了");
                break;
            }
        }
    }
}
