package top.java.multithreading;

import java.util.concurrent.locks.ReentrantLock;

/**
 *       线程安全问题
 *
 *          方式三：    Lock锁方式
 *
 *          实例化ReentrantLock对象，调用该对象lock()方法开启同步，调用该对象unlock()方法关闭同步
 *
 *          synchronized 与 Lock的异同
 *          相同：二者都可以解决线程安全问题
 *          不同：synchronized是隐式锁，机制在执行完相应代码后，自动释放同步监视器
 *               Lock是显示锁，需要手动的开启同步(lock())和手动的关闭同步(unlock())
 *
 *               Lock只有代码块锁，synchronized有代码块锁和方法锁
 *
 *               使用Lock锁，JVM将花费较少的时间来调度线程，性能更好，并具有更好的扩展性(提供更多的子类)
 *
 *          优先使用顺序：
 *              Lock ->  同步代码块  ->  同步方法
 *
 */

public class ThreadTest7 {

    public static void main(String[] args) {

        Window8 w1 = new Window8();
        Window8 w2 = new Window8();
        Window8 w3 = new Window8();
        w1.setName("窗口一");
        w2.setName("窗口二");
        w3.setName("窗口三");
        w1.start();
        w2.start();
        w3.start();
        while (true) {
            if (!(w1.isAlive() && w2.isAlive() && w3.isAlive())) {
                break;
            }
        }
    }

}


class Window8 extends Thread {

    private static int title = 100;

    private static ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        while (true) {
            try {

                lock.lock();         //调用锁定方法lock()

                if (title > 0) {
                    try {
                        sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "票号为：" + title);
                    title--;
                }else {
                    System.out.println("没票了");
                    break;
                }

            }finally {
                lock.unlock();      //调用解锁方法unlock()
            }

        }
    }

}