package top.java.循环;


public class WhileTest {
    public static void main(String[] args) {

     /*
            循环节四要素: 初始化条件  循环条件(是boolean类型)   循环体   迭代条件

            执行顺序:   1 2 3 4 2 3 4 2 3 4 ...
            for(1;2;4) {
                3
            }

            执行顺序:   1 2 3 4 2 3 4 2 3 4 ...
            1
            while(2) {
                3
                4
            }

            执行顺序:   1 3 4 2 3 4 2 3 4 ...
            1
            do{
                3
                4
            }while(2)
      */

        //两个数最大公约数，最小公倍数问题
/*
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入一个正整数：");
        int m = scanner.nextInt();
        System.out.println("输入另一个正整数：");
        int n = scanner.nextInt();
        for (int i = (m<n?m:n);i >= 1;i--) {
            if (m%i == 0 && n%i == 0) {
                System.out.println("最大公约数为：" + i);
                break;
            }
        }
        for (int i = (m>n?m:n);i <= m*n;i++) {
            if (i%m == 0 && i%n == 0) {
                System.out.println("最小公倍数为："+ i);
                break;
            }
        }

        //水仙花
        for (int i = 100;i <= 999;i++) {
            int i1 = i / 100;
//            System.out.print(i1 + " ");
            int i2 = i / 10 % 10;
//            System.out.print(i2 + " ");
            int i3 = i % 10;
//            System.out.print(i3 + " ");
            if (i1*i1*i1+i2*i2*i2+i3*i3*i3 == i) {
                System.out.println(i);
            }
        }
*/
        //100以内质数
        for (int i = 2;i < 100000;i++) {
            boolean isFalse = true;
            for (int j = 2;j <= Math.sqrt(i);j++) {  //优化二  i 改为 Math.sqrt(i)  即根号i
                if (i % j == 0) {
                    isFalse = false;
                    break;              //优化一
                }
            }
            if (isFalse) {
                System.out.println(i);
            }
        }

        //break+标签  结束指定标识的循环结构
        //continue+标签   结束指定标识的当次循环

        label:for (;;) {
            for (;;) {
                if (true) {
                    break label;
                }
            }
        }
    }
}
