package top.java.数组;

public class ArrayTest3 {
    public static void main(String[] args) {

        /*
            杨辉三角
        */
        int[][] a = new int[10][];
        for (int i =0;i < a.length;i++) {
            a[i] = new int[i+1];
            //赋值
            for (int j = 0;j < a[i].length;j++) {
                if (j == 0 || i == j) {
                    a[i][j] = 1;
                }else {
                    a[i][j] = a[i-1][j-1] + a[i-1][j];
                }
            }
        }
//        //赋值
//        for (int i = 0;i < a.length;i++) {
//            for (int j = 0;j < a[i].length;j++) {
//                if (j == 0 || i == j) {
//                    a[i][j] = 1;
//                }else {
//                    a[i][j] = a[i-1][j-1] + a[i-1][j];
//                }
//            }
//        }
        //遍历
        for (int i = 0;i<a.length;i++) {
            for (int j = 0;j < a[i].length;j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("----------------------");

        //二分查找法
        int[] ss = new int[]{-55,-42,-36,-28,-19,-7,2,8,45,67,87,98,125};
        int yy = 98;                //被查找的数
        int head = 0;
        int end = ss.length-1;
        boolean isFast = true;
        while (head<=end) {
            int midd = (head+end)/2;
            if (yy == ss[midd]) {
                System.out.println("找到了：索引为"+midd);
                isFast = false;
                break;
            }else if (yy < ss[midd]) {
                end = midd-1;
            }else {
                head = midd+1;
            }
        }
        if (isFast) {
            System.out.println("没有找到");
        }
    }
}
