package top.java.数组;

public class ArrayTest2 {
    public static void main(String[] args) {

        /*
                二维数组
         */

        int[] arr = new int[]{1,2,3};//一维数组

        //静态初始化
        int[][] arr1 = new int[][]{{1,2,3},{4,5},{6,7,8}};

        //动态初始化1
        String[][] arr2 = new String[3][2];     //行，列
        //动态初始化2
        String[][] arr3 = new String[3][];

        //也是正确的写法：
        int[] arr4[] = new int[][]{{1,2,3},{4,5,9,10},{6,7,8}};
        int[] arr5[] = {{1,2,3},{4,5},{6,7,8}};//类型推断

        //遍历
        System.out.println(arr2);           //输出为地址
        System.out.println(arr2[0]);        //输出为地址

        /*
                初始化默认值：
                    外出元素：地址值
                    内层元素：与一维数组初始化情况相同
         */

        for (int i = 0;i < arr2.length;i++) {
            for (int j = 0;j < arr2[i].length;j++) {
                System.out.print(arr2[i][j]);
            }
            System.out.println();
        }

    }
}
