package top.java.数组;

public class ArrayTest {
    public static void main(String[] args) {
     /*
        多个同类型按一定顺序排列的集合

        数组本身是引用数据类型，而数组中的元素既可以是引用数据类型，也可以是基本数据类型
      */

        //声明并初始化

        //静态初始化：数组的初始化和数组元素的赋值操作同时进行
        int[] a = new int[]{1,2,3};

        //也是正确的写法：
        int[] c = {1,2,3};//类型推断

        //动态初始化：数组的初始化和数组元素的赋值操作分开进行
        int[] b = new int[10];

        System.out.println(a);           //输出为地址

        /*
            数组元素的默认初始化值

            基本数据类型：
            整型       byte,short,int,long    默认值0
            浮点型     float,double           默认值0.0
            字符型     char                   默认值空,即Ascll码中的'0'
            布尔型     boolean                默认值false

            引用数据类型：
            字符串     String                 默认null

         */

        for (int i = 0;i < b.length;i++) {
            System.out.println(b[i]);
        }
    }
}
