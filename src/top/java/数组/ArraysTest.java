package top.java.数组;

/*
    Arrays工具类
 */

import java.util.Arrays;

public class ArraysTest {
    public static void main(String[] args) {

        /*
            sort            排序
            toString        将数组转换成字符串输出
            fill            将数组中的值全部替换成指定值
            binarySearch    排序后的数组进行二分查找
            equals          判断两个数组是否相等
         */

        int[] aa = new int[]{18,58,-45,17,64,35,78,94,-4,47,-88,-56};
//        int[] bb = new int[]{18,58,-45,17,64,35,78,94,-4,47,-88,-56};
//        boolean equals = Arrays.equals(aa, bb);
//        System.out.println(equals);
        Arrays.sort(aa);
        System.out.println(Arrays.toString(aa));
//        Arrays.fill(aa,2);
        int i = Arrays.binarySearch(aa, 64);
        System.out.println(i);
//        for (int i=0;i<aa.length;i++) {
//            System.out.println(aa[i]);
//        }

    }
}
