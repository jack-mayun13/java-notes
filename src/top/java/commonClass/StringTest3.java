package top.java.commonClass;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;


public class StringTest3 {

    /**
     *      基本数据类型,包装类  与    String  之间的转换
     *
     *      基本数据类型,包装类   -->    String类型      1. XXX + "" 进行连接运算  2.String中的valueOf方法
     *
     *       String类型     -->     基本数据类型,包装类    调用包装类的    parserXxx()
     */
    @Test
    public void test1() {
                    //基本数据类型,包装类   -->    String类型

        //  方式1：连接运算
        int a1 = 1552;
        String s1 = a1 + "";            //返回值在堆中    (有变量参与，结果在堆中)


        //  方式二：String中的valueOf方法
        Integer a2 = new Integer(147);
        String s2 = String.valueOf(a1);
        String s3 = String.valueOf(a2);


                    //String类型     -->     基本数据类型,包装类
        String ss1 = "1230";

        //方式一：调用包装类的    parserXxx()
        int i = Integer.parseInt(ss1);

        int i2 = new Integer("85255");
        System.out.println(i2);

    }


    /**
     *      String  与   char数组  的转换
     *
     *      String -->  char[]          调用String的toCharArray()
     *
     *      char[] -->  String          调用String的构造器
     */
    @Test
    public void test2() {

        String str = "abc123";
        char[] chars = str.toCharArray();
        for (int i = 0;i < chars.length;i++) {
            System.out.println(chars[i]);
        }

        char[] arr = new char[]{'h','e','l','l','o'};
        String s = new String(arr);
        System.out.println(s);
    }

    /**
     *          String  与   byte[]  之间的转换
     *
     *          String   -->     byte[]          调用String的getByte()
     *
     *          byte[]   -->     String          调用String的构造器
     */
    @Test
    public void test3() throws UnsupportedEncodingException {
        String s1 = "abc123";
        byte[] bytes = s1.getBytes();                //使用默认字符集进行编码
        System.out.println(Arrays.toString(bytes));  //[97, 98, 99, 49, 50, 51]

        String s2 = "abc123终归";
        byte[] gbks = s2.getBytes("gbk");   //使用指定字符集进行编码
        System.out.println(Arrays.toString(gbks));//[97, 98, 99, 49, 50, 51, -42, -43, -71, -23]
        //编码：字符串转换为字节(即转换为二进制数据)
        //解码：字节转换为字符串
        //解码时使用的字符集必须与编码时使用的字符集一致，否则会出现乱码

        System.out.println("*****************");
        s2 = "abc123张家界";
        byte[] bytes1 = s2.getBytes();
        System.out.println(new String(bytes1));     //abc123张家界
        String s3 = new String(gbks);
        System.out.println(s3);                     //abc123�չ�  编码用的gbk，解码用的默认字符集
        String gbk = new String(gbks, "gbk");       //使用指定字符集解码
        System.out.println(gbk);
    }
}
