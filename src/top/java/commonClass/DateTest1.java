package top.java.commonClass;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *          jdk 8   之前的日期时间的api测试
 *          1.System中的currentTimeMillis()
 *          2.java.util.Date  和其子类  java.sql.Date
 *          3.java.text.SimpleDateFormat
 *          4.java.util.Calendar
 */

public class DateTest1 {


    /**
     *      System类提供的public static long currentTimeMillis()用来返回当前时
     *      间与1970年1月1日0时0分0秒之间以毫秒为单位的时间差。
     *      此方法适于计算时间差。
     */
    @Test
    public void test1() {
        long start = 0l;
        long end = 0l;
        start = System.currentTimeMillis();
        for (int i = 0;i < 100000;i++) {
            for (int j = 0;j < 100000;j++) {
                for (int q = 0;q < 100000;q++) {

                }
            }
        }
        end = System.currentTimeMillis();
        System.out.println("时间为：" + (end - start));
    }

    /**
     *      java.util.Date类
     *          --java.sql.Date类
     *
     *       它们两个为子父类关系，util包下的为父类，sql包下的为子类
     *
     *      1.两个构造器的使用
     *      //构造器1：Date()                      创建一个对应当前时间的Date对象
     *      //构造器1：Date(1657740740946l)        创建一个指定时间戳的Date对象
     *
     *      2.两个方法的使用
     *          toString()      显示年月日时分秒
     *          getTime()       获取当前对象对应的时间戳
     *
     *      3.java.sql.Date  对应数据库中的日期类型的变量
     *          1.如何实例化
     *          2.sql.Date    -->     util.Date     如何转换？   直接赋值，多态
     *          2.util.Date   -->     sql.Date      如何转换？   利用util.Date对象getTime()转换为时间戳
     *                                                         作为参数放入到sql.Date()构造器中
     */
    @Test
    public void test2() {
        //构造器1：Date()        创建一个对应当前时间的Date对象
        Date date1 = new Date();                    //java.util.Date包
        System.out.println(date1.toString());       //Thu Jul 14 03:29:53 CST 2022
        System.out.println(date1.getTime());        //1657740740946     获取当前对象对应的时间戳

        //构造器1：Date(1657740740946l)        创建一个指定时间戳的Date对象
        Date date2 = new Date(1657740740946l);
        System.out.println(date2);                  //Thu Jul 14 03:29:53 CST 2022

        //创建 java.sql.Date 对象
        java.sql.Date date3 = new java.sql.Date(1657740740946l);
        System.out.println(date3.toString());       //2022-07-14

        //sql.Date    -->     util.Date     如何转换？   直接赋值，多态
        Date date4 = new java.sql.Date(1657740740946l);

        System.out.println("********************************");
        //util.Date   -->     sql.Date
        //情况一:
        java.sql.Date date5 = (java.sql.Date) date4;
        //情况二：
        Date date6 = new Date();
 //       java.sql.Date date7 = (java.sql.Date) date6;        //ClassCastException
        java.sql.Date date8 = new java.sql.Date(date6.getTime());
        System.out.println(date8);
    }

    /**
     *      SimpleDateFormat 的使用：对日期Date类的格式化和解析
     *
     *      两个操作：
     *          格式化：        日期      -->    字符串          format()
     *          解析：         字符串     -->     日期          parse()
     *
     *      SimpleDateFormat的实例化
     *          - new SimpleDateFormat()
     *          - new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
     */

    @Test
    public void test3() throws ParseException {

        //  格式化：        日期      -->    字符串

        //实例化，使用默认构造器
        SimpleDateFormat sim = new SimpleDateFormat();

        Date date = new Date();
        System.out.println(date);               //Thu Jul 14 20:06:42 CST 2022

        String format = sim.format(date);
        System.out.println(format);             //22-7-14 下午8:06

        //解析：         字符串     -->     日期
//        String s1 = "2022-7-14";                //异常     ParseException
        String s1 = "22-7-14 下午8:11";
        Date date1 = null;
        try {
            date1 = sim.parse(s1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date1);              //Thu Jul 14 20:11:00 CST 2022

        System.out.println("************指定的方式进行格式化：调用带参数的构造器*************");

        SimpleDateFormat sim2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format1 = sim2.format(date);
        System.out.println(format1);                    //2022-07-14 20:19:12

        //必须符合构造器对应的格式，否则抛异常(空参则是默认格式)
        Date parse = sim2.parse("2024-07-14 20:19:12");
        System.out.println(parse);                  //Sun Jul 14 20:19:12 CST 2024
    }

    /**
     *         Calendar(日历类)的使用
     *
     *         获取月份时：1月是0，12月是11         获取星期时：周日是1 周六是7
     *
     *         Calendar是一个抽象类，主要用于完成日期字段之间相互操作的功能
     *
     *         实例化：
     *              -   使用Calendar.getInstance()方法
     *              -   调用它的子类GregorianCalendar的构造器
     *
     *         常用方法
     *              get()
     *              set()
     *              add()
     *              getTime()
     *              setTime()
     */
    @Test
    public void test4() {
        //实例化
        Calendar instance = Calendar.getInstance();
        System.out.println(instance.getClass());     //class java.util.GregorianCalendar

        //常用方法
        int i = instance.get(Calendar.DAY_OF_MONTH);                //本月第多少天
        System.out.println(i);                          //15
        int i1 = instance.get(Calendar.DAY_OF_YEAR);                //本年第多少天
        System.out.println(i1);                         //196
        instance.set(Calendar.DAY_OF_MONTH,22);                 //set 22
        int i2 = instance.get(Calendar.DAY_OF_MONTH);
        System.out.println(i2);                         //22

        System.out.println("***********************");

        instance.add(Calendar.DAY_OF_MONTH,3);          //add 3
        int i3 = instance.get(Calendar.DAY_OF_MONTH);
        System.out.println(i3);                         //25

        //  日历类     -->     Date
        Date time = instance.getTime();
        System.out.println(time);                       //Mon Jul 25 01:21:49 CST 2022

        //  Date     -->     日历类
        Date date = new Date();
        instance.setTime(date);
        int i4 = instance.get(Calendar.DAY_OF_MONTH);
        System.out.println(i4);                         //15
    }

}
