package top.java.commonClass;

import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

/**
 *          jdk 8   之后的日期时间的api测试
 *
 *          1.LocalDate代表IOS格式（yyyy-MM-dd）的日期,可以存储 生日、纪念日等日期。
 *          2.LocalTime表示一个时间，而不是日期。
 *          2.LocalDateTime是用来表示日期和时间的，这是一个最常用的类之一。
 *
 *          说明：LocalDateTime比LocalDate，LocalTime使用频率更高
 *
 *          now() / * now(ZoneId zone) 静态方法，根据当前时间创建对象/指定时区的对象
 *          of() 静态方法，根据指定日期/时间创建对象
 *          getDayOfMonth()/getDayOfYear() 获得月份天数(1-31) /获得年份天数(1-366)
 *          getDayOfWeek() 获得星期几(返回一个 DayOfWeek 枚举值)
 *          getMonth() 获得月份, 返回一个 Month 枚举值
 *          getMonthValue() / getYear() 获得月份(1-12) /获得年份
 *          getHour()/getMinute()/getSecond() 获得当前对象对应的小时、分钟、秒
 *          withDayOfMonth()/withDayOfYear()/
 *          withMonth()/withYear() 将月份天数、年份天数、月份、年份修改为指定的值并返回新的对象
 *          plusDays(), plusWeeks(),
 *          plusMonths(), plusYears(),plusHours() 向当前对象添加几天、几周、几个月、几年、几小时
 *          minusMonths() / minusWeeks()/
 *          minusDays()/minusYears()/minusHours() 从当前对象减去几月、几周、几天、几年、几小时
 */

public class DateTest3 {

    @Test
    public void test1() {
        //now() 获取当前的日期，时间
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println(localDate);              //2022-07-15
        System.out.println(localTime);              //02:59:04.189
        System.out.println(localDateTime);          //2022-07-15T02:59:04.189

        //of()           设置指定时间，无偏移量
        LocalDateTime of = LocalDateTime.of(2022, 7,
                15, 3, 2, 45);
        System.out.println(of);                             //2022-07-15T03:02:45

        //getXXX()
        System.out.println(localDateTime.getDayOfMonth());             //15
        System.out.println(localDateTime.getDayOfWeek());              //FRIDAY
        System.out.println(localDateTime.getDayOfYear());              //196

        //体现了不可变性
        LocalDateTime localDateTime1 = localDateTime.withDayOfMonth(22);
        System.out.println(localDateTime);                 //2022-07-15T03:11:49.441
        System.out.println(localDateTime1);                //2022-07-22T03:11:49.441
    }


    /**
     *  Instant类的使用             类似与java.util.Date类
     */
    @Test
    public void test2() {
        //对应本初子午线对应的标准时间
        Instant instant = Instant.now();
        System.out.println(instant);                //2022-07-14T19:20:39.129Z       (时区原因)

        //添加时间的偏移量
        OffsetDateTime offsetDateTime = instant.atOffset(ZoneOffset.ofHours(8));
        System.out.println(offsetDateTime);         //2022-07-15T03:23:17.178+08:00

        //获取时间戳
        long l = instant.toEpochMilli();
        System.out.println(l);                      //1657826822763

        //根据给定的时间戳获取Instant对象
        Instant instant1 = Instant.ofEpochMilli(l);
        System.out.println(instant1);               //2022-07-14T19:27:02.763Z
    }

    /**
     *      DateTimeFormatter: 格式化或解析日期，时间      类似于SimpleDateFormat
     *
     *          实例化：自定义的格式。如：ofPattern(“yyyy-MM-dd hh:mm:ss”)
     *
     *          //格式化         日期      -->    字符串          format()
     *          //解析：         字符串     -->     日期          parse()
     */
    @Test
    public void test3() {
        //格式化         日期      -->    字符串          format()
        //格式化之前
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);                        //2022-07-15T03:36:04.217
        //格式化之后
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
        String format = dateTimeFormatter.format(now);
        System.out.println(format);                     //2022-07-15 03:36:04

        //解析：         字符串     -->     日期          parse()
        TemporalAccessor parse = dateTimeFormatter.parse("2022-07-15 03:36:04");
        System.out.println(parse);
    }
}
