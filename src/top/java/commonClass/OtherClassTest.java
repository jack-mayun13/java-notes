package top.java.commonClass;

import org.junit.Test;


/**
 *      其它常用类的使用
 *
 *      1.System
 *      2.Math
 *      3.BigInteger    和    BigDecimal
 */

public class OtherClassTest {

    /**
     *      System类
     *
     *          native long currentTimeMillis()：
     *      该方法的作用是返回当前的计算机时间，时间的表达格式为当前计算机时
     *      间和GMT时间(格林威治时间)1970年1月1号0时0分0秒所差的毫秒数。
     *
     *          void exit(int status)：
     *      该方法的作用是退出程序。其中status的值为0代表正常退出，非零代表
     *      异常退出。使用该方法可以在图形界面编程中实现程序的退出功能等。
     *
     *          void gc()：
     *      该方法的作用是请求系统进行垃圾回收。至于系统是否立刻回收，则
     *      取决于系统中垃圾回收算法的实现以及系统执行时的情况。
     *
     *          String getProperty(String key)：
     *      该方法的作用是获得系统中属性名为key的属性对应的值。
     */
    @Test
    public void test1() {
        String javaVersion = System.getProperty("java.version");
        System.out.println("java的version:" + javaVersion);
        String javaHome = System.getProperty("java.home");
        System.out.println("java的home:" + javaHome);
        String osName = System.getProperty("os.name");
        System.out.println("os的name:" + osName);
        String osVersion = System.getProperty("os.version");
        System.out.println("os的version:" + osVersion);
        String userName = System.getProperty("user.name");
        System.out.println("user的name:" + userName);
        String userHome = System.getProperty("user.home");
        System.out.println("user的home:" + userHome);
        String userDir = System.getProperty("user.dir");
        System.out.println("user的dir:" + userDir);
    }

    /**
     *      Math类
     *
     *      java.lang.Math提供了一系列静态方法用于科学计算。其方法的参数和返回值类型一般为double型。
     *
     *      abs 绝对值
     *      acos,asin,atan,cos,sin,tan 三角函数
     *      sqrt 平方根
     *      pow(double a,doble b) a的b次幂
     *      log 自然对数
     *      exp e为底指数
     *      max(double a,double b)
     *      min(double a,double b)
     *      random() 返回0.0到1.0的随机数
     *      long round(double a) double型数据a转换为long型（四舍五入）
     *      toDegrees(double angrad) 弧度—>角度
     *      toRadians(double angdeg) 角度—>弧度
     */


    /**
     *          BigInteger可以表示不可变的任意精度的整数
     *
     *          商业计算中，要求数字精度比较高，故用到java.math.BigDecimal类。
     *          BigDecimal类支持不可变的、任意精度的有符号十进制定点数。
     */


}
