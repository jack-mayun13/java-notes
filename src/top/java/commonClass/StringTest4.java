package top.java.commonClass;

import org.junit.Test;


public class StringTest4 {

    /**
     *      1. 模拟一个trim方法，去除字符串两端的空格。
     *      2. 将一个字符串进行反转。将字符串中指定部分进行反转。比如“abcdefg”反转为”abfedcg”
     *      3. 获取一个字符串在另一个字符串中出现的次数。
     *          比如：获取“ ab”在 “abkkcadkabkebfkabkskab” 中出现的次数
     *      4.获取两个字符串中最大相同子串。比如：str1 = "abcwerthelloyuiodef“;str2 = "cvhellobnm"
     *          提示：将短的那个串进行长度依次递减的子串与较长的串比较。
     *      5.对字符串中字符进行自然顺序排序。
     *          提示：
     *              1）字符串变成字符数组。
     *              2）对数组排序，选择，冒泡，Arrays.sort();
     *              3）将排序后的数组变成字符串。
     */

    @Test
    public void test1() {
        //1. 模拟一个trim方法，去除字符串两端的空格。

        String s1 = "d   he  ll o  ";
        int i = 1;
        int j;
        if (s1.length() != 0) {
            while (s1.startsWith(" ")) {
                s1 = s1.substring(i);
            }
        }
        if (s1.length() != 0) {
            while (s1.endsWith(" ")) {
                j = s1.length() - 1;
                s1 = s1.substring(0, j);
            }
        }
        System.out.println("--" + s1 + "--");
    }

    @Test
    public void test2() {
        //2.将一个字符串进行反转。将字符串中指定部分进行反转。比如“abcdefg”反转为”abfedcg”
        int a = 2;
        int b = 6;
        String s1 = "abcdefg";
        if (b < s1.length()) {
            char[] chars = s1.toCharArray();
            for (int i = a,j = b;;i++,j--) {
                if (i <= j) {
                    char temp = chars[i];
                    chars[i] = chars[j];
                    chars[j] = temp;
                }else break;
            }
            String s = new String(chars);
            System.out.println(s);
        }

    }

    @Test
    public void test3() {
        //3. 获取一个字符串在另一个字符串中出现的次数。
        //     比如：获取“ ab”在 “abkkcadkabkebfkabkskab” 中出现的次数
        String s1 = "abkkcabbabdkabkebfkabkskab";
        String s2 = "ab";
        int length = s2.length();
        int a = 0;
        int num = 0;
        while (length <= s1.length()) {
            String substring = s1.substring(a, length);
            if (substring.equals(s2)) {
                num++;
            }
            a++;
            length++;
        }
        System.out.println(num);
    }

    @Test
    public void test4() {
        //4.获取两个字符串中最大相同子串。比如：str1 = "abcwerthelloyuiodef“;str2 = "cvhellobnm"
        //       提示：将短的那个串进行长度依次递减的子串与较长的串比较。
            String s1 = "abcwerthelloyuiodef";
            String s2 = "cvhellobnm";
        String maxSameString = getMaxSameString(s1, s2);
        System.out.println(maxSameString);
    }

    public String getMaxSameString(String str1,String shr2) {
        if (str1 != null && shr2 != null) {
            String maxStr = (str1.length() >= shr2.length()) ? str1 : shr2;
            String minStr = (str1.length() < shr2.length()) ? str1 : shr2;
            int length = minStr.length();
            for (int i = 0;i < length;i++) {
                for (int x = 0,y = length-i;y <= length;x++,y++) {
                    String subStr = minStr.substring(x,y);
                    if (maxStr.contains(subStr)) {
                        return subStr;
                    }
                }
            }
        }
        return null;
    }

    @Test
    public void test5() {
//        5.对字符串中字符进行自然顺序排序。
//             提示：
//                1）字符串变成字符数组。
//                2）对数组排序，选择，冒泡，Arrays.sort();
//                3）将排序后的数组变成字符串。
    }

    @Test
    public void test6() {
        String str = null;
        String str2 = str + 'd';
        System.out.println(str2);                   //nulld
        StringBuffer sb = new StringBuffer();
        sb.append(str);
        System.out.println(sb.length());            //4
        System.out.println(sb);                     //"null"
        StringBuffer sb1 = new StringBuffer(str);   //异常    NullPointerException
        System.out.println(sb1);
    }

}
