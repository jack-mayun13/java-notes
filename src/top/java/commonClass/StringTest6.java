package top.java.commonClass;

import org.junit.Test;

public class StringTest6 {
    /**
     *      StringBuffer    和   StringBuilder   的常用方法(两者相同，只是StringBuffer为同步方法)
     *
     *      StringBuffer append(xxx)：提供了很多的append()方法，用于进行字符串拼接
     *      StringBuffer delete(int start,int end)：删除指定位置的内容
     *      StringBuffer replace(int start, int end, String str)：把[start,end)位置替换为str
     *      StringBuffer insert(int offset, xxx)：在指定位置插入xxx
     *      StringBuffer reverse() ：把当前字符序列逆转
     *
     *      public int indexOf(String str)
     *      public String substring(int start,int end)  //返回一个从start开始end结束，左闭右开的子字符串
     *      public int length()
     *      public char charAt(int n )                  //返回某索引处的字符return value[index]
     *      public void setCharAt(int n ,char ch)       //替换指定位置的字符
     *
     *      总结：
     *      增：  append(xxx)：提供了很多的append()方法，用于进行字符串拼接
     *      删：  delete(int start,int end)：删除指定位置的内容
     *      改：  replace(int start, int end, String str)：把[start,end)位置替换为str
     *           setCharAt(int n ,char ch)       //替换指定位置的字符
     *      查：  charAt(int n )                  //返回某索引处的字符return value[index]
     *      插：  insert(int offset, xxx)：在指定位置插入xxx
     *      长度：length()
     *      遍历：直接打印(toString) / 自己写 for()+charAt()
     */

    @Test
    public void test1() {

        StringBuffer sbf1 = new StringBuffer();
        System.out.println(sbf1.append(1));                 //1
        StringBuffer append = sbf1.append("saddsadsaasd");
        System.out.println(sbf1 == append);                 //true
        System.out.println(sbf1);                           //1saddsadsaasd
        sbf1.delete(0,1);
        System.out.println(sbf1);                           //saddsadsaasd
        sbf1.replace(0,8,"hello");
        System.out.println(sbf1);                           //helloaasd
        sbf1.insert(5,"world");
        System.out.println(sbf1);                           //helloworldaasd
        System.out.println("*****************************");
        sbf1.reverse();
        System.out.println(sbf1);                           //dsaadlrowolleh
        String s1 = sbf1.substring(0, 5);
        System.out.println(s1);                             //dsaad
    }

}
