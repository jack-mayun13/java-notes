package top.java.commonClass;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *          SimpleDateFormat 的练习
 */

public class DateTest2 {

    //字符串"2020-09-08"转换为java.sql.Date
    @Test
    public void test1() throws ParseException {
        String s1 = "2020-09-08";
        SimpleDateFormat sim1 = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = sim1.parse(s1);
        long time = parse.getTime();
        java.sql.Date date = new java.sql.Date(time);
        System.out.println(date);
    }


    @Test
    public void test2() throws ParseException {
        String s1 = "2020-09-08";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = dateFormat.parse(s1);
        Date date2 = new Date();
        long l1 = date2.getTime() - date1.getTime();
        long l = l1 / (24 * 60 * 60 * 1000) + 1;
        long l2 = l % 5;
        System.out.println(l2);
    }

}
