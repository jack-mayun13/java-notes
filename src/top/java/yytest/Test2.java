package top.java.yytest;

import org.junit.Test;

import java.util.Date;
import java.util.Scanner;

public class Test2 {

    @Test
    public void test() {
        //数组下标越界异常
        int num[] = new int[]{1,2,3};
        System.out.println(num[3]);
    }

    @Test
    public void test1() {
        //空指针异常
        int num[] = new int[]{1,2,3};
        num = null;
        int length = num.length;
    }

    @Test
    public void test2() {
        //算术异常
        int n = 11;
        int num = n/0;
    }

    @Test
    public void test3() {
        //数字格式化异常
        String a = "qqq";
        int i = Integer.parseInt(a);
    }

    @Test
    public void test4() {
        //输入不匹配异常
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        //输入字母导致
    }

    @Test
    public void test5() {
        //类型转换异常
        Object o = new String();
        Date date = (Date) o;
    }

}
