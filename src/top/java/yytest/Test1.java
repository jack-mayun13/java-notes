package top.java.yytest;


public class Test1 {
    public static void main(String[] args) {
        /**
         *      byte short int long char float double boolean(false true)
         *
         *
         */
        PPP p = new PPP(11);

    }
}

//饿汉式单例模式
class Person {

    private static Person person = new Person();

    private Person() {

    }

    public static Person getPerson() {
        return person;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null && obj.getClass() != this.getClass()) {
            return false;
        }
        if (obj instanceof Person) {
            Person p = (Person) obj;
//            if (p.~~ == this.~~ && p.~~.equals(this.~~)) {
//                return true;
//            }
        }
        return false;
    }
}

//懒汉式单例模式（线程不安全）
class Son {

    private static Son son = null;

    private Son() {

    }

    public static Son getSon() {
        if (son == null) {
            son = new Son();
        }
        return son;
    }

}

class BBB {
    public BBB() {
        System.out.println("2222222222");
    }
}


class PPP extends BBB{

    int a;

    public PPP() {
        System.out.println("111111111");
    }

    public PPP(int a) {
        this.a = a;
        System.out.println("3333333333");
    }

}

class CCC {

    private static CCC ccc = null;

    private CCC() {

    }

    public static CCC getCcc() {
        if (ccc == null) {
            synchronized (CCC.class) {
                if (ccc == null) {
                    ccc = new CCC();
                }
            }
        }
        return ccc;
    }
}

