package top.java.exceptionTest;

public class Test3 {

    public static void main(String[] args) {
        int method = 0;
        try {                                                   //处理异常
            Person person = new Person();
            method = person.method();           //1111111
        }catch (NullPointerException e) {       //444444
            e.getMessage();                     //555555
        }
        System.out.println("结束main方法    "+method);  //666666
    }

}

class Person {

    public int method() throws NullPointerException{            //抛出异常
        Son son = new Son();
        int eat = son.eat();                       //2222222
        System.out.println("结束method方法"+eat);
        return eat;
    }

}

class Son {

    public int eat() throws NullPointerException{               //抛出异常
        //  NullPointerException			空指针异常
        int num[] = null;
        System.out.println(num[5]);             //3333333
        System.out.println("结束eat方法");
        return 1;
    }

}