package top.java.exceptionTest;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *          try-catch-finally   处理异常
 */


public class Test2 {

    @Test
    public void test1() {

        try {
//            int num[] = null;
//            System.out.println(num[5]);                           //空指针异常

            String s = "abc";
            Integer i = Integer.valueOf(s);                         //数字格式化异常

            System.out.println("111111111111111111");               //try中异常下面的代码不会被执行
        }catch (NullPointerException e) {
            System.out.println("处理空指针异常~~~~~~");
            System.out.println(e.getMessage());                     //打印异常信息

            e.printStackTrace();                                    //打印堆栈信息
        }catch (NumberFormatException e) {
            System.out.println("数字格式化异常");
            System.out.println(e.getMessage());
        }
        System.out.println("22222222222222222222");
    }


    @Test
    public void test2() {
        FileInputStream fis = null;
        try {
            File file = new File("hello.txt");
            fis = new FileInputStream(file);
            int data = fis.read();
            while (data != -1) {
                System.out.print((char)data);
                data = fis.read();
            }
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (fis != null)
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
