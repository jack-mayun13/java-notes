package top.java.exceptionTest;

public class Test4 {

    /**
     *      抛出自定义异常
     */

    public static void main(String[] args) {

        Num num = new Num();
        try {
            num.method(152);
        } catch (MyException e) {
            System.out.println(e.getMessage());
        }finally {
            System.out.println(num.nnn);
        }
    }

}

class Num {

    int nnn;

    public void method(int a) throws MyException{
        if (a > 0) {
            this.nnn = a;
        }else {
            throw new MyException("不能输入负数");
        }
    }

}

