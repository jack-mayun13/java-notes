package top.java.exceptionTest;

import org.junit.Test;

import java.util.Date;
import java.util.Scanner;

public class Test1 {


    @Test
    public void test1() {

        //  NullPointerException			空指针异常

        int num[] = null;
        System.out.println(num[5]);
    }

    @Test
    public void test2() {

        //  ArrayIndexOutOfBoundsException	数组下标越界

        int num[] = {1,2,5,4};
        System.out.println(num[4]);
    }

    @Test
    public void test3() {

        //  ClassCastException				类型转换异常

        Object obj = new Date();
        String s = (String) obj;
    }

    @Test
    public void test4() {

        //  NumberFormatException			数字格式化异常

        String s = "abc";
        Integer i = Integer.valueOf(s);
    }

    @Test
    public void test5() {

        //  InputMismatchException			输入不匹配异常

        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();      //输入字母时
    }

    @Test
    public void test6() {

        //  ArithmeticException				算术异常

        int a = 10;
        int i = a / 0;
    }
}
