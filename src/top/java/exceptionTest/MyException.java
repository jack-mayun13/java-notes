package top.java.exceptionTest;

/**
 *       自定义异常类
 */

public class MyException extends Exception{

    final static long serialVersionUID = -9489884232266L;

    public MyException() {

    }

    public MyException(String msg) {
        super(msg);
    }

}
