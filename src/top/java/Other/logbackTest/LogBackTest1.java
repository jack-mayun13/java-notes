package top.java.Other.logbackTest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  Logback日志框架分为以下模块：
 *      logback-core： 该模块为其他两个模块提供基础代码。 （必须有）
 *      logback-classic：完整实现了slf4j API的模块。（必须有）
 *      logback-access 模块与 Tomcat 和 Jetty 等 Servlet 容器集成，以提供 HTTP 访问日志功能
 *
 *      想使用Logback日志框架，至少需要在项目中整合如下三个模块
 *          slf4j-api：日志接口
 *          logback-core：基础模块
 *          logback-classic：功能模块，它完整实现了slf4j API
 *
 *
 *    设置日志输出级别的作用是什么？
 *      用于控制系统中哪些日志级别是可以输出的。
 *
 *    Logback的日志级别是什么样的？
 *      ALL  和 OFF分别是打开全部日志和关闭全部日志
 *      级别程度依次是：TRACE< DEBUG< INFO<WARN<ERROR
 *      默认级别是debug（忽略大小写），只输出当前级别及高于该级别的日志
 */
public class LogBackTest1 {

    /**
     *      快速搭建LogBack日志框架 记录程序的执行情况到控制台
     *
     *      1.导入Logback框架到项目中去。在项目下新建文件夹lib，导入Logback的jar包到该文件夹下
     *
     *      2.将存放jar文件的lib文件夹添加到项目依赖库中去
     *
     *      3.将Logback的核心配置文件logback.xml直接拷贝到src目录下（必须是src下）
     *
     *      4.创建Logback框架提供的Logger日志对象，后续使用其方法记录系统的日志信息
     */

    //创建Logback框架提供的Logger日志对象
    public static final Logger LOGGER = LoggerFactory.getLogger("LogBackTest1.class");


    public static void main(String[] args) {

        try {
            LOGGER.debug("main方法开始");
            LOGGER.info("第二行");
            int a = 10;
            int b = 0;
            LOGGER.trace("a= " + a);
            LOGGER.trace("b= " + b);
            System.out.println(a / b);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("出现异常" + e);
        }

    }
}
