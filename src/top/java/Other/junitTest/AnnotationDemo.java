package top.java.Other.junitTest;

import java.lang.reflect.Method;

/**
 *                  模拟一个简单的 Junit 框架
 */

public class AnnotationDemo {

    @MyJunit
    public void test1() {
        System.out.println("-----test1-----");
    }

    public void test2() {
        System.out.println("-----test2-----");
    }

    @MyJunit
    public void test3() {
        System.out.println("-----test3-----");
    }

    /**
     *      启动菜单    有注解才被调用
     */
    public static void main(String[] args) throws Exception {

        AnnotationDemo demo = new AnnotationDemo();

        //获取类对象
        Class c = AnnotationDemo.class;

        //提取全部方法
        Method[] methods = c.getDeclaredMethods();

        //遍历方法，看是否有MyJunit注解
        for (Method method : methods) {
            if (method.isAnnotationPresent(MyJunit.class)) {
                    method.invoke(demo);
            }
        }
    }

}
