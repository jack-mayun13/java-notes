package top.java.Other.timerTest;

import org.junit.Test;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *      定时器是一种控制任务延时调用，或者周期调用的技术。
 *      作用：闹钟、定时邮件发送。
 *
 *      定时器的实现方式
 *          方式一：Timer
 *          方式二： ScheduledExecutorService
 *
 *      --------------------Timer方式---------------------
 *
 *     public Timer()      创建Timer定时器对象
 *     schedule(TimerTask task,long delay,long period)开启一个定时器，按照计划处理TimerTask任务
 *
 *     Timer定时器的特点和存在的问题
 *      1、Timer是单线程，处理多个任务按照顺序执行，存在延时与设置定时器的时间有出入
 *      2、可能因为其中的某个任务的异常使Timer线程死掉，从而影响后续任务执行
 *
 *      --------------------ScheduledExecutorService定时器---------------------
 *
 *      newScheduledThreadPool(int corePoolSize)        得到线程池对象
 *
 *      public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay,
 *      long period,TimeUnit unit)                      周期调度方法
 *
 *      ScheduledExecutorService的优点
 *      1、基于线程池，某个任务的执行情况不会影响其他定时任务的执行。
 *
 */
public class TimeTest1 {

    /**
     *  --------------------Timer方式---------------------
     */
    @Test
    public void test1() throws InterruptedException {

        Timer timer = new Timer();

        //TimerTask task, long delay, long period   delay为初始化延迟时间，period为周期执行时间
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "执行一次");
            }
        },0,2000);
        while (true){}
    }

    @Test
    public void test2() {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(3);
        pool.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "执行一次");
            }
        },0,2, TimeUnit.SECONDS);
        while (true){}
    }

}
