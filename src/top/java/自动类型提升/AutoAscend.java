package top.java.自动类型提升;

public class AutoAscend {
    public static void main(String[] args) {

        /*
             自动类型提升 <--> 强制类型转换    只讨论7种基本类型，不包括boolean类型

                整型常量默认为int类型    浮点型常量默认为double类型

             自动类型提升：
             byte,char,short -> int -> long -> float -> double
             当容量小的数据类型变量与容量大的数据类型变量做运算时，结果自动提升为容量大的数据类型
             byte,char,short发生自动类型提升时都转换为int类型

             强制类型转换：
                自动类型提升的逆运算，强制将容量大的数据类型变量转换为容量小的数据类型变量
                可能会出现精度损失
         */

        //b为double类型数据，1为int类型数据，相加结果自动类型提升为double类型数据
        //不能赋值给int类型，可以强转但会精度损失
        double b = 3.1415926;
        int c = (int) b + 1;

        System.out.println(c);

        /*
            字母通过字符集(Ascll码)转换为数字再进行运算
            汉字也是通过字符集转换为数字再进行运算
         */

        char f = 'a';
        int h = 10;
        int j = f+h;
        System.out.println(j);      //输出为107
        char y = '就';
        int p = y+h;
        System.out.println(p);      //输出为23611
        int l = y+f;
        System.out.println(l);      //输出为23698

//        Long a = 56666;           //Long是类
        long a = 56666;             //56666是int类型，赋值给long自动类型提升
    }
}
