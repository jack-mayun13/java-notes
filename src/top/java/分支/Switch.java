package top.java.分支;

public class Switch {

    public static void main(String[] args) {

        /**
         *      switch语句的执行顺序不是表达式里面的值与挨个case匹配，与case里面的值匹配成功后，
         *          才会执行case里面的语句。
         *
         *      用反编译工具，去看看源码的执行过程。
         *      第一种情况，如果case里面的值都是比较接近的。编译器会把case值装到一个类似数组的容器里，
         *          然后通过索引直接找到具体的值。数组遍历是不用挨个遍历的，通过索引可以锁定具体的值。
         *      第二种情况，case里面的值间距比较大，比如第一个case里面的值是1，第二个case里面的值是2，
         *          第三个case里面的值是1000，这会儿编译器的算法是通过二分查找法，来找到具体case值。
         */

        /*
              switch(表达式){
                case 常量1:
                     语句1;
                     break;
                case 常量2:
                     语句2;
                     break;
                     .....
                case 常量N:
                     语句N;
                     break;
                default:
                     语句:
                     break;
                }

                jdk7以后，switch中的表达式可以是String类型
                case之后只能声明常量,不能声明范围

                switch执行效率比if-else高
         */
        /*
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();

        switch (s) {
            case "+":
                System.out.println("+");
                break;
            case "-":
                System.out.println("-");
                break;
            case "*":
                System.out.println("*");
                break;
            case "/":
                System.out.println("/");
                break;
            default:
                System.out.println("无");
                break;
        }
*/
        int i = 4;
        //没有匹配的则跳出switch
        switch (i) {
            case 1:
                System.out.println(1);
                break;
            case 2:
                System.out.println(2);
                break;
            case 3:
                System.out.println(3);
                break;
        }

        System.out.println(5);
    }
}
