package top.java.genericTest;


import org.junit.Test;

import java.util.ArrayList;

/**
 *      泛型不同的引用不能相互赋值
 *
 *      经验：泛型要使用一路都用。要不用，一路都不要用。
 *
 *      在静态方法中不能使用类的泛型 (静态结构类加载的时间创建，早于对象的创建)     异常类不能是泛型的
 *
 */
public class GenericTest4 {

    @Test
    public void test1() {
        ArrayList<Integer> integers = new ArrayList<>();
        ArrayList<String> strings = new ArrayList<>();
        // integers = strings;                          //泛型不同的引用不能相互赋值
    }

}



class Order1<T> {

    T item;

    public Order1() {
        //T[] arr = new T[3];     //编译不通过       类型参数 'T' 不能直接实例化

        T[] arr = (T[]) new Object[10];
    }
}

class Father<T1, T2> {

}

// 子类不保留父类的泛型
// 1)没有类型 擦除
class Son1 extends Father {// 等价于class Son extends Father<Object,Object>{

}

// 2)具体类型
class Son2 extends Father<Integer, String> {

}

// 子类保留父类的泛型
// 1)全部保留
class Son3<T1, T2> extends Father<T1, T2> {

}

// 2)部分保留
class Son4<T2> extends Father<Integer, T2> {

}

// 子类不保留父类的泛型
// 1)没有类型 擦除
class Son5<A, B> extends Father{//等价于class Son extends Father<Object,Object>{

}

// 2)具体类型
class Son6<A, B> extends Father<Integer, String> {

}

// 子类保留父类的泛型
// 1)全部保留
class Son7<T1, T2, A, B> extends Father<T1, T2> {

}

// 2)部分保留
class Son8<T2, A, B> extends Father<Integer, T2> {

}

