package top.java.genericTest;


import org.junit.Test;

import java.util.*;

/**
 *        泛型的练习     /       在集合中使用泛型
 */

public class GenericTest2 {


    //按名称从小到大排序
    @Test
    public void test1() {
        TreeSet<Employee> treeSet = new TreeSet<>();
        Employee e1 = new Employee("AAA", 18, new MyDate(2009, 5, 4));
        Employee e2 = new Employee("BBB", 18, new MyDate(1999, 7, 14));
        Employee e3 = new Employee("CCC", 19, new MyDate(2009, 1, 2));
        Employee e4 = new Employee("DDD", 20, new MyDate(1989, 8, 4));
        Employee e5 = new Employee("EEE", 21, new MyDate(1989, 6, 3));
        treeSet.add(e1);
        treeSet.add(e2);
        treeSet.add(e3);
        treeSet.add(e4);
        treeSet.add(e5);
        for (Employee employee : treeSet) {
            System.out.println(employee);
        }
    }

    //按生日从小到大排序
    @Test
    public void test2() {
        TreeSet<Employee> treeSet = new TreeSet<>(new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                if (o1.getBirthday().getYear() == o2.getBirthday().getYear()) {
                    if (o1.getBirthday().getMonth() == o2.getBirthday().getMonth()) {
                        return o1.getBirthday().getDay() - o2.getBirthday().getDay();
                    }else {
                        return o1.getBirthday().getMonth() - o2.getBirthday().getMonth();
                    }
                }else {
                    return o1.getBirthday().getYear() - o2.getBirthday().getYear();
                }
            }
        });

        Employee e1 = new Employee("AAA", 18, new MyDate(2009, 1, 4));
        Employee e2 = new Employee("BBB", 18, new MyDate(1999, 7, 14));
        Employee e3 = new Employee("CCC", 19, new MyDate(2009, 1, 2));
        Employee e4 = new Employee("DDD", 20, new MyDate(1989, 8, 4));
        Employee e5 = new Employee("EEE", 21, new MyDate(1989, 6, 3));
        treeSet.add(e1);
        treeSet.add(e2);
        treeSet.add(e3);
        treeSet.add(e4);
        treeSet.add(e5);
        Iterator<Employee> iterator = treeSet.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    @Test
    public void test3( ){
        HashSet<Person3> set = new HashSet<>();
        Person3 p1 = new Person3(1001,"AA");
        Person3 p2 = new Person3(1002,"BB");
        set.add(p1);
        set.add(p2);
        p1.name = "CC";
        set.remove(p1);
        //[Person3{id=1002, name='BB'}, Person3{id=1001, name='CC'}]
        // 哈希值改变，所以删除不成功
        System.out.println(set);
        set.add(new Person3(1001,"CC"));
        //[Person3{id=1002, name='BB'}, Person3{id=1001, name='CC'},
        // Person3{id=1001, name='CC'}]
        // 第一次存入的时候，是按照AA的哈希值存入的，第二次是按照CC哈希值存入，所以直接添加成功
        System.out.println(set);
        set.add(new Person3(1001,"AA"));
        //[Person3{id=1002, name='BB'}, Person3{id=1001, name='CC'},
        // Person3{id=1001, name='CC'}, Person3{id=1001, name='AA'}]
        //存入的数据与初始数据AA的哈希值相同，但是equals()不同，添加成功
        System.out.println(set);
    }
}

class Employee implements Comparable<Employee> {
    private String name;
    private int age;
    private MyDate birthday;

    public Employee() {
    }

    public Employee(String name, int age,MyDate birthday) {
        this.name = name;
        this.age = age;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public MyDate getBirthday() {
        return birthday;
    }

    public void setBirthday(MyDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                '}';
    }

    //按名字从小到大排序
    @Override
    public int compareTo(Employee o) {
      return this.name.compareTo(o.name);
    }
}

class MyDate {
    private int year;
    private int month;
    private int day;

    public MyDate() {
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "MyDate{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }
}

class Person3 {
    int id;
    String name;

    public Person3() {
    }

    public Person3(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person3{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person3 person3 = (Person3) o;
        return id == person3.id && Objects.equals(name, person3.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
