package top.java.genericTest;


import org.junit.Test;

import java.util.*;

/**
 *      在JDK1.5之前只能把元素类型设计为Object，JDK1.5之后使用泛型来解决。
 *
 *      所谓泛型，就是允许在定义类、接口时通过一个标识表示类中某个属性的类型或者是某个方法的返回值及参数类型。
 *      这个类型参数将在使用时（例如，继承或实现这个接口，用这个类型声明变量、创建对象时）确定（即传入实
 *      际的类型参数，也称为类型实参）。
 *
 *      总结：
 *          集合接口或集合类在jdk5.0时都修改为带泛型的结构
 *          在实例化集合类时，可以指明具体的泛型类型
 *          指明完以后，在集合类或接口中凡是定义类或接口时，内部结构使用到类的泛型的位置，都指定为实例化的泛型类型
 *          比如：add(E e) -->  实例化以后，add(Integer e)
 *          泛型的类型必须是类，不能是基本数据类型，需要用到基本数据类型的位置，用包装类替换
 *          如果实例化时，没有指明泛型的类型，默认为 Object 类型
 *
 */
public class GenericTest1 {

    //未使用泛型的情况
    @Test
    public void test1() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(75);
        arrayList.add(68);
        arrayList.add(88);
        arrayList.add(60);

        //问题一：类型不安全
//        arrayList.add("Tom");

        for (Object obj : arrayList) {
            //问题二：强转时可能出现强转转换异常
            //java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer
            int score = (Integer) obj;
            System.out.println(score);
        }
    }

    //使用泛型的情况
    @Test
    public void test2() {
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(75);
        integers.add(68);
        integers.add(88);
        integers.add(60);

        //编译时，就会进行类型检查，保证数据的安全
//        integers.add("Tom");


        for (Integer integer : integers) {
            //避免了强转类型转换
            int score = integer;
            System.out.println(score);
        }

        System.out.println("*********************");

        Iterator<Integer> iterator = integers.iterator();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            System.out.println(next);
        }
    }

    @Test
    public void test3() {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Tom",111);
        hashMap.put("Jon",222);
        hashMap.put("Sun",333);
        hashMap.put("Tre",444);

        Set<Map.Entry<String,Integer>> entrySets = hashMap.entrySet();
        for (Map.Entry<String,Integer> entry : entrySets) {
            String name = entry.getKey();
            Integer score = entry.getValue();
            System.out.println("Key：" + name + " \t " + "Value：" + score);
        }
    }
}
