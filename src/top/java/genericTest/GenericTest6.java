package top.java.genericTest;


import org.junit.Test;

import java.util.List;

/**
 *          泛型类和泛型方法使用场景
 */
public class GenericTest6 {

    @Test
    public void test1() {
        CustomerDao customerDao = new CustomerDao();
        customerDao.add(new Customer());
        List<Customer> forList = customerDao.getForList(10);

        StudentDao studentDao = new StudentDao();
        studentDao.add(new Student());
        List<Student> forList1 = studentDao.getForList(20);
    }

}

class Customer {

}

class Dao<E> {                  //表的共性操作的Dao
    //添加一条记录
    public void add(E e) {

    }
    //删除一条记录
    public boolean remove(int index) {
        return false;
    }
    //修改一条记录
    public void update(int index,E e) {

    }
    //查询一条记录
    public E getIndex(int index) {
        return null;
    }
    //查询多条记录
    public List<E> getForList(int index) {
        return null;
    }

    //泛型方法的使用场景      举例：  返回的类型不确定   获取表中一共有多少条数据? 获取最大员工入职时间?
    public <T> T getValue(){
        return null;
    }
}

class CustomerDao extends Dao<Customer> {       //只能操作一个表的Dao

}

class Student {

}

class StudentDao extends Dao<Student> {       //只能操作一个表的Dao

}
