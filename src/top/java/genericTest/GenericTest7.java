package top.java.genericTest;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *      泛型在继承方面的体现
 *
 *      通配符的使用
 */
public class GenericTest7 {

    /**
     *    泛型在继承方面的体现：
     *
     *    虽然类A是类B的父类，但是List<A> 和List<B>二者不具备子父类关系，二者是并列关系。
     *
     *    补充：类A是类B的父类，List<G> 是 ArrayList<G> 的父类
     */
    @Test
    public void test1( ){
        Object obj = null;
        String str = null;
        obj = str;

        Object[] objArr = null;
        String[] strArr = null;
        objArr = strArr;

        List<Order> list1 = null;
        List<String> list2 = null;
        //list1 = list2;                  //此时list1 和 list2 不具有子父类关系

        List<String> list3 = null;
        ArrayList<String> list4 = null;
        List<String> list5 = null;
        list3 = list4;
        list3 = list5;                    //类A是类B的父类，A<G> 是 B<G> 的父类
    }

    /**
     *      通配符的使用
     *
     *      类A是类B的父类， List<A> 和 List<B>是没有关系的，二者共同的父类是：List<?>
     */
    @Test
    public void test2() {
        List<Object> list1 = null;
        List<String> list2 = null;

        List<?> list = null;

        list = list1;
        list = list2;

        ArrayList<String> list3 = new ArrayList<>();
        list3.add("AAA");
        list3.add("BBB");

        ArrayList<String> list4 = new ArrayList<>();
        list4.add("CCC");
        list4.add("DDD");

        list = list3;
        list = list4;

        //list.add("CC");                 // 对于List<?> 就不能向其内部添加数据,除了null

        Iterator<?> iterator = list.iterator();
        while (iterator.hasNext()) {
            Object next = iterator.next();
            System.out.println(next);
        }
    }

    @Test
    public void test3() {

        List<? extends Date> list1 = null;
        List<? extends Object> list2 = null;

        List<Date> list3 = null;
        List<Object> list4 = null;

        list1 = list3;
        //list1 = list4;            //编译不通过
        list2 = list3;
        list2 = list4;
    }
}
