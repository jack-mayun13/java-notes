package top.java.genericTest;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 *          泛型方法
 *
 *   泛型方法：在方法中出现了泛型的结构，泛型参数与类的泛型参数没有任何关系
 *          即   泛型方法所属的类是不是泛型类都没有关系
 *
 *          泛型方法在调用时，指明泛型参数的类型
 *
 *          泛型方法可以声明为静态的(static)    泛型参数类型是在调用方法时确定，不是在实例化时确定
 */
public class GenericTest5 {

    /**
     *      自定义泛型方法
     */
    @Test
    public void test1() {
        String[] arr = {"Tom","Jon","Que"};
        //泛型方法在调用时，指明泛型参数的类型
        List<String> strings = copyFromArrayToList(arr);
        System.out.println(strings);                //[Tom, Jon, Que]

        Integer[] integers = new Integer[]{111,222,333,444};
        List<Integer> integers1 = copyFromArrayToList(integers);
        System.out.println(integers1);              //[111, 222, 333, 444]
    }

    public <T> List<T> copyFromArrayToList(T[] t) {
        ArrayList<T> ts = new ArrayList<>();
        for (T arr : t) {
            ts.add(arr);
        }
        return ts;
    }

}
