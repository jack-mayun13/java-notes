package top.java.genericTest;

import org.junit.Test;

/**
 *      自定义泛型结构：泛型类、泛型接口、泛型方法
 */

public class GenericTest3 {

    /**
     *      自定义泛型类
     *
     *      如果定义了泛型类，实例化没有指明泛型，则认为此泛型类型为Object类型
     *      要求：如果定义了类是带泛型的，建议在实例化时指明类的泛型
     */
    @Test
    public void test1(){
        //如果定义了泛型类，实例化没有指明泛型，则认为此泛型类型为Object类型
        Order order1 = new Order();
        order1.setOrderT(123);
        order1.setOrderT("Tom");
        order1.setOrderT(new Order());

        //如果定义了类是带泛型的，建议在实例化时指明类的泛型
        Order<Integer> order2 = new Order<>();
        order2.setOrderT(111);
        order2.setOrderT(222);
//        order2.setOrderT("Tom");      //编译不通过
    }

    /**
     *      子类在继承带泛型的父类时，指明了泛型的类型，则实例化子类对象时，不再需要指明泛型
     */
    @Test
    public void test2( ){

        SubOrder subOrder = new SubOrder();
        subOrder.setOrderT(123);
//        subOrder.setOrderT("Tom");    //编译不通过

        BunOrder bunOrder = new BunOrder();
        bunOrder.setOrderT("Tom");
//        bunOrder.setOrderT(123);      //编译不通过，因为继承时指明的是String类型

        TomOrder<String> tomOrder = new TomOrder<>();
        tomOrder.setOrderT("Tom");
//        tomOrder.setOrderT(123);      //编译不通过
    }
}

class Order<T> {
    String orderName;
    int orderId;
    T orderT;

    public Order() {

    }

    public Order(String orderName,int orderId,T orderT) {
        this.orderName = orderName;
        this.orderId = orderId;
        this.orderT = orderT;
    }

    public void setOrderT(T orderT) {
        this.orderT = orderT;
    }

    public T getOrderT() {
        return orderT;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderName='" + orderName + '\'' +
                ", orderId=" + orderId +
                ", orderT=" + orderT +
                '}';
    }
}

//继承时指明泛型类型为Integer类型
class SubOrder extends Order<Integer> {

}

////继承时指明泛型类型为String类型
class BunOrder extends Order<String> {

}

//继承时未指明泛型类型
class TomOrder<E> extends Order<E> {

}