package top.java.enumClass;


import org.junit.Test;

/**
 *      Enum类中的常用方法
 *
 *      1. values()方法：返回枚举类型的对象数组。该方法可以很方便地遍历所有的枚举值。
 *      2. valueOf(String str)：可以把一个字符串转为对应的枚举类对象。要求字符
 *        串必须是枚举类对象的“名字”。如不是，会有运行时异常：IllegalArgumentException。
 *      3. toString()：返回当前枚举类对象常量的名称
 */
public class enumTest2 {

    @Test
    public void test1() {
        Season1[] values = Season1.values();
        for (int i = 0;i < values.length;i++) {                 //SPRING,SUMMER,AUTUMN,WINTER
            System.out.println(values[i]);
        }

        //valueOf(String str)           根据参数名返回枚举类中同名对象
        Season1 spring = Season1.valueOf("SPRING");
        System.out.println(spring);
    }

}
