package top.java.enumClass;


import org.junit.Test;

/**
 *          使用enum关键字定义的枚举实现接口(即每个枚举都有单独的抽象方法)
 *
 *          实现：
 *              为每个属性重写单独的抽象方法
 */
public class enumTest3 {

    @Test
    public void test1() {

        Season2.SPRING.show();

        Season2.SUMMER.show();

        Season2.AUTUMN.show();

        Season2.WINTER.show();
    }

}

interface Info {
    void show();
}

// 使用enum关键字定义枚举类
enum Season2 implements Info{
    //提供当前枚举类的对象，多个对象之间用 ',' 隔开，末尾用 ';' 结束
    SPRING("春天","春暖花开"){
        @Override
        public void show() {
            System.out.println("111111111111111111111");
        }
    },
    SUMMER("夏天","夏日炎炎"){
        @Override
        public void show() {
            System.out.println("222222222222222222222");
        }
    },
    AUTUMN("秋天","秋高气爽"){
        @Override
        public void show() {
            System.out.println("33333333333333333");
        }
    },
    WINTER("冬天","冰天雪地"){
        @Override
        public void show() {
            System.out.println("44444444444444444444");
        }
    };

    //1.声明Season对象的属性:   private final修饰
    private final String seasonName;
    private final String seasonDesc;

    //2.私有化构造器,并给对象属性初始化
    private Season2(String seasonName,String seasonDesc) {
        this.seasonDesc = seasonDesc;
        this.seasonName = seasonName;
    }

    //其它述求：获取枚举类对象的属性

    public String getSeasonName() {
        return seasonName;
    }

    public String getSeasonDesc() {
        return seasonDesc;
    }

    //默认打印变量名,有需求再重写toString()
}
