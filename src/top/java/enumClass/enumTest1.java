package top.java.enumClass;

import org.junit.Test;

/**
 *     枚举类的使用：
 *         1.枚举类的理解： 类的对象只有有限个，确定的，我们称此类为枚举类
 *         2.当需要定义一组常量时，强烈建议使用枚举类
 *         3.如果枚举类中只有一个对象，则可以作为单例模式的实现方式
 *
 *
 *     如何定义枚举类：
 *          jdk5.0  之前          自定义枚举类
 *          jdk5.0  之后          可以使用enum关键字定义枚举类
 *
 */
public class enumTest1 {

    /**
     *      自定义枚举类
     */
    @Test
    public void test1() {
        Season spring = Season.SPRING;
        System.out.println(spring);

    }

    /**
     *      使用enum关键字定义枚举类
     *
     *      定义的枚举类默认继承于 class java.lang.Enum
     */
    @Test
    public void test2() {
        Season1 spring = Season1.SPRING;
        System.out.println(spring);

        System.out.println(spring.getClass().getSuperclass());
    }
}

// 使用enum关键字定义枚举类
enum Season1 {
    //提供当前枚举类的对象，多个对象之间用 ',' 隔开，末尾用 ';' 结束
    SPRING("春天","春暖花开"),
    SUMMER("夏天","夏日炎炎"),
    AUTUMN("秋天","秋高气爽"),
    WINTER("冬天","冰天雪地");

    //1.声明Season对象的属性:   private final修饰
    private final String seasonName;
    private final String seasonDesc;

    //2.私有化构造器,并给对象属性初始化
    private Season1(String seasonName,String seasonDesc) {
        this.seasonDesc = seasonDesc;
        this.seasonName = seasonName;
    }

    //其它述求：获取枚举类对象的属性

    public String getSeasonName() {
        return seasonName;
    }

    public String getSeasonDesc() {
        return seasonDesc;
    }

    //默认打印变量名,有需求再重写toString()
}

//自定义枚举类
class Season {

    //1.声明Season对象的属性:   private final修饰
    private final String seasonName;
    private final String seasonDesc;

    //2.私有化构造器,并给对象属性初始化
    private Season(String seasonName,String seasonDesc) {
        this.seasonDesc = seasonDesc;
        this.seasonName = seasonName;
    }

    //提供当前枚举类的多个对象：public static final修饰
    public static final Season SPRING = new Season("春天","春暖花开");
    public static final Season SUMMER = new Season("夏天","夏日炎炎");
    public static final Season AUTUMN = new Season("秋天","秋高气爽");
    public static final Season WINTER = new Season("冬天","冰天雪地");

    //其它述求：获取枚举类对象的属性

    public String getSeasonName() {
        return seasonName;
    }

    public String getSeasonDesc() {
        return seasonDesc;
    }

    //提供toString()方法

    @Override
    public String toString() {
        return "Season{" +
                "seasonName='" + seasonName + '\'' +
                ", seasonDesc='" + seasonDesc + '\'' +
                '}';
    }
}
