package top.java.运算符;

public class Operator {
    public static void main(String[] args) {

        /*
            优先级：

             只有单目运算符，三元运算符，赋值运算符 是从右向左运算的

             三元运算符统一类型然后赋值，要求类型相同
         */

        /*
            比较运算符   instanceof  检查是否是类的对象   返回值为Boolean类型
         */

        if (null instanceof String) {       //null 是否为  String的对象
            System.out.println("是");
        }else {
            System.out.println("不是");
        }

        /*
               << 左移   >> 右移   >>> 无符号右移    & 与运算   | 或运算   ^ 异或运算   ~ 取反运算

               >> 右移    二进制向右移，最高位是1用1补，最高位是0用0补
               >>> 无符号右移    无论最高位是1还是0，都用0补

               5 & 12       5 | 12      5 ^ 12      !5

        二进制双方都为1才得1      二进制只要一方为1就得1     二进制双方相同为0，不同为1    各二进制全部取反
         */



        /*
            逻辑运算符   只能用于boolean类型运算

            & 逻辑与   && 短路与  | 逻辑或   || 短路与  ! 逻辑非   ^ 逻辑异或
         */

//        ^运算    相同为false    不同为true
//        true ^ true     为false
//        false ^ false   为false
//        true ^ false    为true
//        false ^ true    为true

        /*
            +=,-=,*=,/=,%= 不会改变变量本身的数据类型
         */

        short s1 = 12;
//        s1 = s1 + 1;      //编译失败
        s1 += 2;

        int s2 = 1;
        s2 *= 0.1;      //不会改变变量本身的数据类型 int类型与double类型运算结果还为int类型
        System.out.println(s2);
        s2++;
        System.out.println(s2);

        /*
            比较运算符的结果都是boolean类型
         */

        /**
         *      -12 % 5 = 2    12 % -5 = -2      %取余运算，结果的符号与被模数(前一个)的符号相同
         */

    }
}
