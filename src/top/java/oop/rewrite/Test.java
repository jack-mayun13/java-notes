package top.java.oop.rewrite;

public class Test {
    public static void main(String[] args) {

        Student student = new Student();
        student.sleep();

        Person person = new Person();
        person.sleep();

        System.out.println("***************");
        student.eat();

    }
}
