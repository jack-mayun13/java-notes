package top.java.oop.rewrite;

public class Student extends Person{

    private String major;

    public Student() {
        super();
    }

    public Student(String major) {
        this.major = major;
    }

    public Student(String major,String name,int age) {
        super(name,age);
        this.major = major;
    }

    public void study() {
        System.out.println("学习");
    }

    public void sleep() {
        System.out.println("多多多睡睡睡");
    }

}
