package top.java.oop;

/**
        面向对象
 */


public class PropertyTest {
    public static void main(String[] args) {

        

        /*
            类为class设计的概念
            对象为具体new出来的

            类和对象的使用
            1.创建类，设计类的成员
            2.创建类的对象
            3.通过"对象.属性"或"对象.方法"调用对象的结构
         */

        /*
            对象的创建和使用内存解析：
                虚拟机栈    用于存储局部变量等
                堆         用于存放对象实例
                方法区      用于存储已被虚拟机加载的类信息，常量，静态变量，即时编译器编译后的代码等数据
         */
    }
}

        /*
                属性(成员变量)    和   局部变量

                相同：
                    1.送一变量的格式：
                        数据类型 变量名 = 变量值
                    2.先声明，后使用
                    3.变量都有其对应的作用域
                不同：
                    1.声明位置不同
                        属性：直接声明在类的一对{}内
                        局部变量：声明在方法内，方法形参，代码块内，构造器形参，构造器内部的变量
                    2.权限修饰符不同
                        属性：声明属性时，可使用权限修饰符
                        局部变量：不可以使用权限修饰符
                    3.默认初始化值不同
                        属性的默认初始化值：
                            基本数据类型：
                            整型       byte,short,int,long    默认值0
                            浮点型     float,double           默认值0.0
                            字符型     char                   默认值空,即Ascll码中的'0'
                            布尔型     boolean                默认值false

                            引用数据类型(类，数组，接口)          默认null

                        局部变量：没有初始化值，在调用之前一定要赋值
                    4.在内存中加载的位置不同
                        属性：加载到堆空间中(非static)
                        局部变量：加载到栈中
         */


class Person{

    String name;
    int age = 1;
    boolean isMale;

    public void eat() {
        System.out.println("人可以吃");
    }

    public void sleep() {
        System.out.println("人可以睡觉");
    }

    public void talk(String language) {
        System.out.println("人可以说" + language);
    }
}
