package top.java.oop.polymorphism;

public class Man extends Person{

    boolean isSmoking;

    String earnMoney;

    public Man() {

    }

    public void eat() {
        System.out.println("男人，，，吃");
    }

    public void walk() {
        System.out.println("男人，，，走路");
    }

    public void run() {
        System.out.println("男人，，，跑步");
    }

}
