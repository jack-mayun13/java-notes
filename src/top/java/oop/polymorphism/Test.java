package top.java.oop.polymorphism;

public class Test {

    public static void main(String[] args) {

        /**
         *      子类的对象赋给父类的引用
         *
         *      编译看左边，运行看右边
         */
        Person p = new Man();
        p.walk();

        Person p1 = new Woman();
        p1.walk();

        /**
         *         向下转型测试       多态为向上转型
         */
        System.out.println("******************");

        Person p2 = new Man();
        Man man = (Man) p2;
        man.run();

        System.out.println("********Woman*********");
//        Woman woman = (Woman) p2;   // p2为Man类型，强转为Woman类型则报强制类型转换异常
//        woman.shop();

        //为了避免向下转型时出现强制类型转换异常，通常使用instanceof进行判断

        System.out.println("***********************");
        if (p2 instanceof Woman) {     //判断p2是否为Woman类型
            System.out.println("isWoman~~~~~~~~");
            Woman woman1 = (Woman) p2;
            woman1.shop();
        }
        if (p2 instanceof Man) {       //判断p2是否为Man类型
            System.out.println("isMan~~~~~~~~~");
            Man man1 = (Man) p2;
            man1.run();
        }
        if (p2 instanceof Person) {
            System.out.println("isPerson~~~~~~");
        }


    }
}
