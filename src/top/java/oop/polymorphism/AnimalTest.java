package top.java.oop.polymorphism;

public class AnimalTest {

    public static void main(String[] args) {

        AnimalTest test = new AnimalTest();

        test.func(new Dog());

        Animal animal = new Cat();

        test.func(animal);
    }

    public void func(Animal animal) {
        animal.eat();
        animal.shout();
    }
}

class Animal {

    public void eat() {
        System.out.println("动物，吃");
    }

    public void shout() {
        System.out.println("动物，叫");
    }

}

class Dog extends Animal {

    public void eat() {
        System.out.println("狗，吃");
    }

    public void shout() {
        System.out.println("汪!汪!汪！");
    }
}

class Cat extends Animal {

    public void eat() {
        System.out.println("猫，吃");
    }

    public void shout() {
        System.out.println("喵！喵！喵！");
    }
}

