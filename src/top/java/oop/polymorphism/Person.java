package top.java.oop.polymorphism;


public class Person {

    private String name;

    private int age;

    public Person() {

    }

    public Person(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public void eat() {
        System.out.println("吃");
    }

    public void sleep() {
        System.out.println("睡");
    }

    public void walk() {
        System.out.println("走路");
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null && this.getClass() != obj.getClass()) {
            return false;
        }
        if (obj instanceof Person) {
            Person o = (Person) obj;
            if (o.age == this.age && o.name.equals(this.name)) {
                return true;
            }
        }
        return false;
    }

}
