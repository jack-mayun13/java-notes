package top.java.oop.object;


public class Person {

    private String name;

    private int age;

    public Person() {

    }

    public Person(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public void eat() {
        System.out.println("吃");
    }

    public void sleep() {
        System.out.println("睡");
    }

    public void walk() {
        System.out.println("走路");
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null && obj.getClass() != this.getClass()) {
            return false;
        }
        if (obj instanceof Person) {
            Person o = (Person) obj;
            if (o.age == this.age && o.name.equals(this.name)) {
                return true;
            }
        }
        return false;
    }

//    @Override
//    public String toString() {
//        return "Person[ " + this.name + ", " + this.age + " ]";
//    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
