package top.java.oop.object;


public class EqualsTest {

    public static void main(String[] args) {

        /**
         *      == 和 equals 的区别
         *
         *      ==：是运算符
         *      1.可以使用在基本数据类型变量和引用数据变量中
         *      2.比较基本数据类型，则比较两个变量保存的数据是否相同（不一定要类型相同）
         *      3.比较引用数据类型，则比较两个对象地址值是否相同
         *
         *      equals：是方法
         *      1.是一个方法而非运算符
         *      2.只能适用于引用数据类型
         *      3.String,Date,File,包装类等都重写了equals方法
         *      4.重写以后比较的是两个对象的实体内容是否相同
         */

        Person t1 = new Person("12344",25);
        Person t2 = new Person("12344",25);
        System.out.println(t1.equals(t2));

        System.out.println("*************************");
        String s1 = "12345";
        String s2 = "12345";

        System.out.println(s1.equals(s2));

        /**
         *          toString()方法
         *
         *   像String,Date,File,包装类等都重写了Object中的toString方法
         *   自定义类需重写toString方法,返回对象的 "实体内容"
         */

        System.out.println("*************************");

        System.out.println(t1.getClass());
        System.out.println(t1);

//        Date date = new Date();
//        System.out.println(date.toString());

    }

}
