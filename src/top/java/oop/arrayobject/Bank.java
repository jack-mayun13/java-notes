package top.java.oop.arrayobject;

public class Bank {

    private Customer[] customers;

    private int numberOfCustomer;

    public Bank() {
        customers = new Customer[10];
    }

    public void addCustomer(String f,String l) {
        Customer customer = new Customer(l, f);
        customers[numberOfCustomer++] = customer;
    }

    public int getNumberOfCustomer() {
        return numberOfCustomer;
    }

    public Customer getCustomer(int index) {
        if (index <= numberOfCustomer && numberOfCustomer >= 0 && index >= 0) {
            return customers[index];
        }
        return null;
    }

}
