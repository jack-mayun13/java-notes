package top.java.oop.arrayobject;

public class Account {

    private double  balance;

    public Account(double balance) {
        this.balance = balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    //存钱
    public void deposit(double amt) {
        if (amt > 0) {
            balance += amt;
            System.out.println("存钱成功");
        }
    }

    public void withdraw(double amt) {
        if (amt > 0 && balance >= amt) {
            balance -= amt;
            System.out.println("取钱成功，余额为" + balance);
            return;
        }
        System.out.println("余额不足");
    }
}
