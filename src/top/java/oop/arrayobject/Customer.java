package top.java.oop.arrayobject;

public class Customer {

    private String lastName;
    private String firstName;
    private Account account;

    public Customer(String lastName,String firstName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    public String getLastName() {
        return getLastName();
    }

    public String getFirstName() {
        return firstName;
    }
}
