package top.java.oop.abstractTest;

public class AbstractTest {

    public static void main(String[] args) {
        /**
         *      修饰类为抽象类，抽象类无法实例化(即无法造对象)
         *
         *      修饰方法为抽象方法，抽象方法不能有实体
         *                      有抽象方法的类一定是抽象类，抽象类不一定有抽象方法
         *                      子类继承抽象类后必须重写父类及其间接父类的所有抽象方法(除非自身也是抽象类)
         *
         *      注意点：  abstract 不能修饰属性，构造器等
         *
         *              abstract 不能修饰私有方法,静态方法,final的方法(不能被重写),final的类(不能被继承)
         */

        //创建抽象类的匿名子类对象，创建的是Person的匿名子类对象
        Person p = new Person() {

            @Override
            public void eat() {
                System.out.println("吃吃吃");
            }

            @Override
            public void walk() {
                System.out.println("走走走");
            }
        };

        AbstractTest a1 = new AbstractTest();
        //多态，子类对象传给父类引用
        a1.method(p);

        System.out.println("******************");

        //抽象类的匿名子类的匿名对象
        a1.method(new Person() {
            @Override
            public void eat() {
                System.out.println("再次吃");
            }

            @Override
            public void walk() {
                System.out.println("再次走路");
            }
        });
    }

    public void method(Person person) {
        person.eat();
        person.walk();
    }
}

abstract class Person {

    String name;
    int age;

    public Person() {

    }

    public Person(String name,int age) {
        this.age = age;
        this.name = name;
    }

    public abstract void eat();

    public abstract void walk();
}

class Son extends Person{

    @Override
    public void eat() {

    }

    @Override
    public void walk() {

    }
}
