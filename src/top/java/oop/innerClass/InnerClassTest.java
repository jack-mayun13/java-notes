package top.java.oop.innerClass;

public class InnerClassTest {

    /**
     *    为成员内部类(静态、非静态) 和 局部内部类(方法内、代码块内、构造器内)
     */

    public static void main(String[] args) {
        //静态成员内部类实例化
        Person.Son son = new Person.Son();
        son.walk();
        //非静态成员内部类实例化
        Person p = new Person();
        Person.Man man = p.new Man();
        man.eat();
    }

}

class Person {

    String name;
    int age;

    //静态成员内部类
    public static class Son {
        String name;
        int age;

        public void walk() {
            System.out.println("Son走");
        }
    }

    //非静态成员内部类
    public class Man {
        String name;
        int age;

        public void eat() {
            System.out.println("Man吃");
        }
    }

    {
        class Dog {
            String name;
            int age;

            public void look() {
                System.out.println("Dog看");
            }
        }
    }

    public Person() {
        class Bird {
            String name;
            int age;

            public void show() {
                System.out.println("Bird鸟");
            }
        }
    }

    public void walk() {
        System.out.println("Person");
        class Woman {
            String name;
            int age;

            public void sing() {
                System.out.println("Woman唱歌");
            }
        }
    }

    //局部内部类的使用
    public Comparable getComparable() {
        //返回一个实现了Comparable接口的类的对象

        //创建一个实现了Comparable接口的类
        class MyComparable implements Comparable{
            @Override
            public int compareTo(Object o) {
                return 0;
            }
        }
//        return new MyComparable();

        return new Comparable() {
            @Override
            public int compareTo(Object o) {
                return 0;
            }
        };
    }

}
