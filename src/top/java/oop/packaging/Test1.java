package top.java.oop.packaging;


import org.junit.Test;

/**
 *          单元测试方法
 *
 */

public class Test1 {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Test
    public void Test1() {
        Test1 test1 = new Test1();
        System.out.println(test1.getName().equals("455"));
    }

    /**
     *          boolean byte short int long float double char   false   true
     *          Boolean Byte Short Integer Long Float Double Character
     */

}
