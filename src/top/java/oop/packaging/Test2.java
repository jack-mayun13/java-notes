package top.java.oop.packaging;

import org.junit.Test;

/**
 *      包装类与基本数据类型的转换       自动装箱,自动拆箱
 */

public class Test2 {

    /**
     *      基本数据类型转换为包装类      -->     调用包装类的构造器
     */
    @Test
    public void Test1() {
        int a = 11;
        Integer i1 = new Integer(a);
        System.out.println(i1.toString());

        Float aFloat = new Float(5585F);
        System.out.println(aFloat);

        Integer i2 = new Integer("1254");
        System.out.println(i2);

        int num = 101;
        method(num);                //自动装箱

        Integer i3 = 5522;
    }

    public void method(Object obj) {
        System.out.println(obj);
    }


    /**
     *      包装类转换为基本数据类型      -->     调用包装类的 xxxValue 方法
     */
    @Test
    public void Test2() {

        Integer i1 = new Integer(12358);
        int i = i1.intValue();
        System.out.println(i + 111);        //转换以后可以进行运算

        int i2 = i1 + 101;
        System.out.println(i2);             //自动拆箱
        int i3 = i1;
    }

}
