package top.java.oop.packaging;

import org.junit.Test;

import java.util.Scanner;
import java.util.Vector;

/**
 *      包装类和基本数据类型  与   String类型的转换
 */


public class Test3 {

    /**
     *      基本数据类型,包装类   -->    String类型
     */
    @Test
    public void test1() {
        //  方式1：连接运算
        int a1 = 1552;
        String s1 = a1 + "";            //返回值在堆中    (有变量参与，结果在堆中)


        //  方式二：String中的valueOf方法
        Integer a2 = new Integer(147);
        String s2 = String.valueOf(a1);
        String s3 = String.valueOf(a2);
    }


    /**
     *      String类型     -->     基本数据类型,包装类
     */
    @Test
    public void test2() {

        String s1 = "1230";

        //方式一：调用包装类的    parserXxx()
        int i = Integer.parseInt(s1);

        int i2 = new Integer("85255");
        System.out.println(i2);

    }



    //  练习题

    public static void main(String[] args) {
        Vector v = new Vector();
        Scanner scanner = new Scanner(System.in);
        int max = 0;
        char a;
        while (true) {
            System.out.println("请输入成绩：");
            int i = scanner.nextInt();
            if (i < 0) {
                break;
            }
            if (i > 100) {
                System.out.println("非法，请重新输入：");
                continue;
            }
            v.addElement(i);
            if (max < i) {
                max = i;
            }
        }
        for (int i = 0;i < v.size();i++) {
            Object obj = v.elementAt(i);
            Integer ob = (Integer) obj;
            if (max-ob <= 10){
                a = 'A';
            }else if (max-ob <= 20){
                a = 'B';
            }else if (max-ob <= 30){
                a = 'C';
            }else {
                a = 'D';
            }
            System.out.println("成绩为：" + ob + " " + a);
        }
    }
}
