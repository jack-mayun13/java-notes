package top.java.oop;

public class Constructor {

    public static void main(String[] args) {

        Persons persons = new Persons();

    }
}


class Persons {

    int age;

    String name;

    public Persons() {
        eat();
    }

    public Persons(int age,String name) {
        this();
        this.age = age;
        this.name = name;
    }

    public void eat() {
        System.out.println("吃饭");
    }
}
