package top.java.oop.statictest;

public class StaticTest1 {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle();
        System.out.println(c1.getId()+" "+Circle.getTotal());
        System.out.println(c2.getId()+" "+Circle.getTotal());
        Circle c3 = new Circle(3.5);
        System.out.println(c3.getId()+" "+Circle.getTotal());
    }
}

class Circle {
    private double radius;
    private int id;
    private static int init=1001;
    private static int total;

    public Circle() {
        this.id = init++;
        total++;
    }

    public Circle(double radius) {
        this();
        this.radius = radius;
    }

    public int getId() {
        return id;
    }

    public static int getTotal() {
        return total;
    }

    public double findArea() {
        return Math.PI * radius * radius;
    }


}
