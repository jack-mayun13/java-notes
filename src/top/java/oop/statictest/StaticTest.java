package top.java.oop.statictest;

public class StaticTest {
    public static void main(String[] args) {

        Chinese c1 = new Chinese();
        c1.name = "111";
        c1.age = 29;
        Chinese c2 = new Chinese();
        c2.name = "222";
        c2.age = 40;

        c1.nation = "CN";
        System.out.println(c2.nation);
        Chinese.eat();

////        对象调用
//        String nation = c2.nation;
//        String name = c2.name;
////        类调用
//        String nation1 = Chinese.nation;
//        int age = Chinese.age;


    }
}

class Chinese {
    String name;
    int age;
    static String nation;

    //静态方法不能调用非静态方法
    public static void eat() {
        System.out.println("吃");
//        walk();
        String nation = Chinese.nation;
        eat();
    }

    //非静态方法可以调用静态方法
    public void walk() {
        System.out.println("走路");
        eat();
        String nation = Chinese.nation;
    }
}