package top.java.oop.designMode.proxy;


/**
 *      静态代理模式
 */
public class StaticProxy {
    public static void main(String[] args) {
        ProxyService proxyService = new ProxyService(new Service());
        proxyService.browse();
    }
}

interface NetWork {
    void browse();
}

//被代理类
class Service implements NetWork {
    @Override
    public void browse() {
        System.out.println("真实的服务器访问网络");
    }
}

//代理类
class ProxyService implements NetWork {

    private NetWork netWork;

    public ProxyService(NetWork netWork) {
        this.netWork = netWork;
    }

    public void check() {
        System.out.println("访问前检查网络");
    }

    @Override
    public void browse() {
        check();                 //对原有的方法进行增强
        netWork.browse();
    }
}