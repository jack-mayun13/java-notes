package top.java.oop.designMode.singleton.test1;

public class SingletonTest2 {
    public static void main(String[] args) {
        Order o1 = Order.getOrder();
        Order o2 = Order.getOrder();
        System.out.println(o1 == o2);
        System.out.println("*****************");
        Person p1 = Person.getPerson();
        Person p2 = Person.getPerson();
        System.out.println(p1 == p2);
    }
}

class Order {
    private static Order order = new Order();
    private Order() {

    }
    public static Order getOrder() {
        return order;
    }
}

class Person {
    private static Person person = null;
    private Person() {

    }
    public static Person getPerson() {
        if (person == null) {
            person = new Person();
        }
        return person;
    }
}