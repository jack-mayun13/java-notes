package top.java.oop.designMode.factory;

public class FactoryTest1 {

    public static void main(String[] args) {

        /**
         *  无工厂模式
         */
        System.out.println("*******无工厂模式********");
        Audi audi = new Audi();
        audi.run();
        BYD byd = new BYD();
        byd.run();

        /**
         *      简单工厂模式
         */
        System.out.println("*******简单工厂模式********");
        Car audi1 = CarFactory.getCar("Audi");
        audi1.run();
        Car byd1 = CarFactory.getCar("BYD");
        byd1.run();
        System.out.println("*******简单工厂模式2********");
        CarFactory.getAudi().run();
        CarFactory.getBYD().run();

        /**
         *      工厂方法模式
         */
        System.out.println("********工厂方法模式********");
        Car byd2 = new BYDFactory().getCar();
        byd2.run();
        Car audi2 = new AudiFactory().getCar();
        audi2.run();
    }
}


/**
 *      简单工厂
 */
class CarFactory {

    public static Car getCar(String car) {
        if (car.equals("Audi")) {
            return new Audi();
        }else if (car.equals("BYD")) {
            return new BYD();
        }else {
            return null;
        }
    }

    public static Car getAudi() {
        return new Audi();
    }

    public static Car getBYD() {
        return new BYD();
    }
}

/**
 *      工厂方法
 */
interface Factory {
    Car getCar();
}

class AudiFactory implements Factory {

    @Override
    public Car getCar() {
        return new Audi();
    }
}

class BYDFactory implements Factory {

    @Override
    public Car getCar() {
        return new BYD();
    }
}



interface Car {
    void run();
}

class Audi implements Car {

    @Override
    public void run() {
        System.out.println("奥迪在跑");
    }
}

class BYD implements Car {

    @Override
    public void run() {
        System.out.println("比亚迪在跑");
    }
}
