package top.java.oop.interfaceTest;

public class Test2 {

    public static void main(String[] args) {

    }

}

interface AAA {
    void play();
}

interface BBB {
    void play();
}

class DDD {
    public void play() {
        System.out.println("DDD");
    }
}

/**
 *      类 "CCC" 必须声明为抽象，或为实现 "AAA" 中的抽象方法 "play()"
 */
//class CCC implements AAA,BBB {
//
//}

/**
 *       优先继承中的
 */
class CCC extends DDD implements AAA {

}

/**
 *       优先继承中的
 */
//class CCC extends DDD implements AAA,BBB {
//
//}