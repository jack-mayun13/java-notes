package top.java.oop.interfaceTest;

public class Test1 {

}

interface A {
    int x = 0;
}

class B {
    int x = 1;
}

/**
 *      C继承了A和B中的x
 *      在C中调用A中的x用A.x(x是全局常量)
 *      在C中调用B中的x用suoer.x
 *      如B还有父类，在C中调用B父类的x，需在B中写方法返回super.x，在c中调用此方法
 */
class C extends B implements A{
    public void pX() {
//        System.out.println(x);        //x不明确
    }
}
