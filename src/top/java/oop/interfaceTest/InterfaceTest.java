package top.java.oop.interfaceTest;

public class InterfaceTest {

    public static void main(String[] args) {
        InterfaceTest i1 = new InterfaceTest();
        Flash flash = new Flash();
        i1.Computer(flash);

        System.out.println("**************");

        Printer printer = new Printer();
        i1.Computer(printer);

        i1.Computer(new USB() {
            @Override
            public void start() {

            }

            @Override
            public void end() {

            }
        });
    }

    public void Computer(USB usb) {
        usb.start();
        System.out.println("传输细节");
        usb.end();
    }
}

interface USB {
    void start();
    void end();
}

class Flash implements USB {

    @Override
    public void start() {
        System.out.println("USB接口开始传输");
    }

    @Override
    public void end() {
        System.out.println("USB接口结束传输");
    }
}

class Printer implements USB {

    @Override
    public void start() {
        System.out.println("打印机开始传输");
    }

    @Override
    public void end() {
        System.out.println("打印机结束传输");
    }
}

