package top.java.oop.test.多态测试;

public class Test {
    public static void main(String[] args) {
        Test test = new Test();

        int i = (int) (Math.random() * 2 + 1);
        Anomal anomal = roAnomal(i);
        anomall(anomal);
    }

    public static void anomall(Anomal anomal) {
        anomal.eat();
        anomal.walk();
    }

    public static Anomal roAnomal(int i) {
        switch (i) {
            case 1:
                return new Dog();
            case 2:
                return new Cat();
        }
        return null;
    }
}

class Anomal{
    public void eat() {
        System.out.println("动物，吃");
    }

    public void walk() {
        System.out.println("动物，跑");
    }
}

class Cat extends Anomal {
    public void eat() {
        System.out.println("猫，，，吃");
    }
    public void walk() {
        System.out.println("猫，，，跑");
    }
}

class Dog extends Anomal {
    public void eat() {
        System.out.println("狗，，，吃");
    }
    public void walk() {
        System.out.println("狗，，，跑");
    }
}
