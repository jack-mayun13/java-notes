package top.java.oop.test.继承测试;

public class Test {
    public static void main(String[] args) {
        Student student = new Student();
        student.sleep();
    }
}

class Person {
    String name;
    int age;

    public Person() {

    }

    public Person(String name,int age) {
        this.age = age;
        this.name = name;
    }

    public void eat() {
        System.out.println("人，，吃饭");
    }

    public void sleep() {
        System.out.println("人，，睡觉");
    }
}

class Student extends Person {

    int id;

    public Student() {
        this(515151);
    }

    //不管调用那个构造器。都会直接间接的调用父类的空参构造器，子类从而获取父类所有的属性和方法

    public Student(int id) {
        this.id = id;
    }
}
