package top.java.oop.codeBlock;

public class Test2 {
    public static void main(String[] args) {
        System.out.println("1111111111111111");
        new Son();
    }
}


//        1111111111111111
//        222222222222222
//        55555555555555555
//        3333333333333333
//        444444444444444
//        666666666666666666
//        777777777777777777


class Son extends Person1{
    static {
        System.out.println("55555555555555555");
    }
    {
        System.out.println("666666666666666666");
    }
    public Son() {
        System.out.println("777777777777777777");
    }
}

class Person1 {
    static {
        System.out.println("222222222222222");
    }
    {
        System.out.println("3333333333333333");
    }
    public Person1() {
        System.out.println("444444444444444");
    }
}

