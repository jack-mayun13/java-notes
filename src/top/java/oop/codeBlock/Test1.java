package top.java.oop.codeBlock;

public class Test1 {

    public static void main(String[] args) {
        System.out.println(Person.desc);
        Person p1 = new Person();
        Person p2 = new Person();
        System.out.println(Person.desc);
    }

}

class Person {

    /**
     *      静态代码块        1.随着类的加载而加载，且只加载一次
     *                      2.只能调用静态属性和方法，不能调用非静态
     *                      3.初始化类的信息，即给静态属性初始化
     *                      4.可以有多个，从上到下依次执行
     *
     *      非静态代码块      1.随着对象的加载而加载，可加载多次
     *                      2.可以调用静态和非静态的属性和方法
     *                      3.可以在创建对象时对属性进行初始化
     *                      4.可以有多个，从上到下依次执行
     *
     */

    String name;
    int age;
    static String desc = "我是一个人";

    public Person() {

    }

    public Person(String name,int age) {
        this.age = age;
        this.name = name;
    }

    {
        System.out.println("非静态代码块");
    }

    static {
        System.out.println("静态代码块");
    }

    public void eat() {
        System.out.println("吃");
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
