package top.java.oop.codeBlock;

public class Son11 extends Father{
    static {
        System.out.println("1111111111");
    }
    {
        System.out.println("22222222222");
    }
    public Son11() {
        System.out.println("33333333333");
    }

    public static void main(String[] args) {
        System.out.println("777777777777");
        new Son11();
    }
}

class Father {
    static {
        System.out.println("444444444444");
    }
    {
        System.out.println("555555555555");
    }
    public Father() {
        System.out.println("666666666666");
    }
}
