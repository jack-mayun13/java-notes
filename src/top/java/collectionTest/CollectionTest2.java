package top.java.collectionTest;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 *   Collection常用方法：
 *
 *         提供一种方法访问一个容器(container)对象中各个元
 *      素，而又不需暴露该对象的内部细节。迭代器模式，就是为容器而生。
 *
 *       遍历
 *      iterator()：返回迭代器对象，用于集合遍历
 *
 *      搭配的方法：
 *          hasNext()
 *          next()
 *
 *
 *      Iterator接口remove()方法         内部定义了remove()方法，可以在遍历的时候，删除集合中的元素，
 *                                     此方法不同于集合直接调用remove()方法
 *
 *      说明：如果还未调用next()或在上一次调用 next 方法之后已经调用了 remove 方法，再调用remove
 *      都会报IllegalStateException。
 *
 *      增强foreach循环
 *
 *      Java 5.0 提供了 foreach 循环迭代访问 Collection和数组。
 *
 *      for(要遍历的元素类型  遍历后自定义元素名称 : 要遍历的结构名称) {
 *
 *      }
 */

public class CollectionTest2 {

    @Test
    public void test1() {
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(234);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Object());

        Iterator iterator = coll.iterator();
        for (int i = 0;i < 5;i++) {
            System.out.println(iterator.next());
        }
      //  System.out.println(iterator.next());            //NoSuchElementException
        System.out.println("*******************");
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        //前面已经遍历完，指针已经指向最后一个元素，所以不遍历了

        Iterator iterator1 = coll.iterator();
        while (iterator1.hasNext()) {
            System.out.println(iterator1.next());
        }
    }

    @Test
    public void test2() {
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(234);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Object());

        System.out.println(coll);               //[123, 234, Tom, false, java.lang.Object@4e515669]
        Object obj = "Tom";

        Iterator iterator = coll.iterator();
        while (iterator.hasNext()) {
            if (obj.equals(iterator.next())) {
                iterator.remove();              //[123, 234, false, java.lang.Object@4e515669]
            }
        }
        System.out.println(coll);
    }

    @Test
    public void test3() {
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(234);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Object());

        for (Object obj : coll) {
            System.out.println(obj);
        }
    }

}
