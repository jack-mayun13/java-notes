package top.java.collectionTest;

import org.junit.Test;

/**
 * Map结构的理解
 *  Key：    无序的，不可重复的，使用Set存储所有的Key -> Key所在的类要重写equals()和HashCode()方法(HashMap为例)
 *  Value：  无序的，可重复的，使用Collection存储所有的Value
 *
 *  一个键值对：key-value构成了一个Entry对象，Entry对象也是无序的，不可重复的，使用Set存储所有的entry
 */
public class MapTest2 {

    @Test
    public void test1() {

    }
}
