package top.java.collectionTest;

import org.junit.Test;

import java.util.ArrayList;

/**
 *      鉴于Java中数组用来存储数据的局限性，我们通常使用List替代数组
 *      List集合类中元素有序、且可重复，集合中的每个元素都有其对应的顺序索引。
 *      List容器中的元素都对应一个整数型的序号记载其在容器中的位置，可以根据序号存取容器中的元素。
 *      JDK API中List接口的实现类常用的有：ArrayList、LinkedList和Vector。
 *
 *      Collection接口 ：单列数据      用来存储一个一个的对象
 *         -List接口：存储有序的，可重复的数据            --> "动态数组"
 *           -ArrayList
 *           -LinkedList
 *           -Vector
 *
 *     ArrayList  LinkedList  Vector  的异同
 *
 *        相同点：   1.三个类都实现了List接口，存储有序的，可重复的数据
 *
 *        不同点：   1.ArrayList 作为List接口的主要实现类，线程不安全，效率高，底层使用Object[]存储
 *                 2.LinkedList 底层使用双向链表存储 对于频繁插入删除效率比 ArrayList 高
 *                 3.Vector 作为一个古老的实现类，线程安全，效率低，底层使用Object[]存储
 *
 *
 *  ArrayList源码分析：
 *
 *      jdk 7情况下
 *          ArrayList list = new ArrayList();//底层创建了长度是10的Object[]数组elementData
 *          list.add(123);//elementData[0] = new Integer(123);
 *          ...
 *          list.add(11);//如果此次的添加导致底层elementData数组容量不够，则扩容。
 *          默认情况下，扩容为原来的容量的1.5倍，同时需要将原有数组中的数据复制到新的数组中。
 *
 *          结论：建议开发中使用带参的构造器：ArrayList list = new ArrayList(int capacity)
 *
 *      jdk 8中ArrayList的变化：
 *          ArrayList list = new ArrayList();//底层Object[] elementData初始化为{}.并没创建长度为10的数组
 *
 *          list.add(123);//第一次调用add()时，底层才创建了长度10的数组，并将数据123添加到elementData[0]
 *          ...
 *          后续的添加和扩容操作与jdk 7 无异。
 *      小结：jdk7中的ArrayList的对象的创建类似于单例的饿汉式，而jdk8中的ArrayList的对象
 *             的创建类似于单例的懒汉式，延迟了数组的创建，节省内存。
 *
 *
 *      LinkedList的源码分析：
 *          LinkedList list = new LinkedList(); 内部声明了Node类型的first和last属性，默认值为null
 *          list.add(123);//将123封装到Node中，创建了Node对象。
 *
 *       其中，Node定义为：体现了LinkedList的双向链表的说法
 *
 *
 *
 *      Vector的源码分析：
 *          jdk7和jdk8中通过Vector()构造器创建对象时，底层都创建了长度为10的数组。
 *          在扩容方面，默认扩容为原来的数组长度的2倍。
 *
 */
public class ListTest1 {

    @Test
    public void test1() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(123);
    }

}
