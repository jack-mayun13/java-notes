package top.java.collectionTest;

import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;


public class SetTest2 {

    /**
     *      LinkedHashSet() 的使用
     *
     *      作为hashSet的子类，在添加数据的同时还存放了两个指针，记录前一个数据和后一个数据
     *
     *      对于频繁的遍历操作，LinkedHashSet()效率高于HashSet()
     */
    @Test
    public void test1() {
        Set set = new LinkedHashSet();
        set.add(123);
        set.add(456);
        set.add("AAA");
        System.out.println(set);            //[123, 456, AAA]
        set.add(123);
        System.out.println(set);            //[123, 456, AAA]
        set.add(new Person1("Tom",20));
        set.add(new Person1("Tom",20));

        System.out.println();
    }


    /**
     *      TreeSet() 的使用
     *
     *      TreeSet：可以按照添加对象的指定属性，进行排序
     *
     *      向TreeSet中添加的数据，要求是相同类的对象，不然会报ClassCastException
     *
     *      TreeSet 两种排序方法：自然排序和定制排序。默认情况下，TreeSet 采用自然排序。
     *      向TreeSet中添加自定义类型数据，要求重写 排序方式
     */
    @Test
    public void test2() {
        Set set = new TreeSet();
        set.add(123);
//        set.add("AA");        //ClassCastException
        set.add(456);
        set.add(123);
        set.add(856);
        set.add(648);

        System.out.println(set);        //[123, 456, 648, 856]
        System.out.println("******************");

        TreeSet treeSet = new TreeSet();
        treeSet.add(new Person2("AAA",23));
        treeSet.add(new Person2("AAA",78));
        treeSet.add(new Person2("AAA",46));
        treeSet.add(new Person2("DDD",15));
        treeSet.add(new Person2("EEE",15));
        treeSet.add(new Person2("FFF",15));
        System.out.println(treeSet);
    }


}

class Person2 implements Comparable{
    private String name;
    private int age;

    public Person2() {
    }

    public Person2(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person1{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

//    //按照年龄从小到大排序,再按名字从小到大
//    @Override
//    public int compareTo(Object o) {
//        if (o instanceof Person2) {
//            Person2 p2 = (Person2) o;
//            if (p2.age > this.age) {
//                return -1;
//            }else if (p2.age < this.age){
//                return 1;
//            }else {
//                return this.name.compareTo(p2.name);
//            }
//        }
//       throw new RuntimeException("传入的数据不匹配");
//    }

    //按照名字从大到小，再按年龄从小到大
    @Override
    public int compareTo(Object o) {
        if (o instanceof Person2) {
            Person2 p2 = (Person2) o;
            if (p2.name.equals(this.name)) {
                return this.age - p2.age;
            }else {
                return -this.name.compareTo(p2.name);
            }
        }
        throw new RuntimeException("传入的数据不匹配");
    }
}
