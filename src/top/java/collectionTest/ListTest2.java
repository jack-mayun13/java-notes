package top.java.collectionTest;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 *      List接口常用方法
 *
 *      void add(int index, Object ele):在index位置插入ele元素
 *      boolean addAll(int index, Collection eles):从index位置开始将eles中的所有元素添加进来
 *      Object get(int index):获取指定index位置的元素
 *      int indexOf(Object obj):返回obj在集合中首次出现的位置
 *      int lastIndexOf(Object obj):返回obj在当前集合中末次出现的位置
 *      Object remove(int index):移除指定index位置的元素，并返回此元素
 *      Object set(int index, Object ele):设置指定index位置的元素为ele
 *      List subList(int fromIndex, int toIndex):返回从fromIndex到toIndex位置的子集合
 *
 *      List是Collection的子接口，Collection中的方法都可以使用，
 *      而且List是有序可重复的，不同于Set的无序不可重复。List有一些特有的方法
 *
 *      总结常用方法：
 *          增   add(Object obj)
 *          删   remove(int index) / remove(Object obj)
 *          改   set(int index, Object ele)
 *          查   get(int index)
 *          插   add(int index, Object ele)
 *          长度  size()
 *          遍历  1.Iterator迭代器方式     2.增强for循环       3.普通循环
 */
public class ListTest2 {

    @Test
    public void test1() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(123);
        arrayList.add(123);
        arrayList.add("细算");
        arrayList.add(new Person("Tom",22));
        System.out.println(arrayList);      //[123, 123, 细算, Person{name='Tom', age=22}]

        arrayList.add(1,"BB");
        System.out.println(arrayList);      //[123, BB, 123, 细算, Person{name='Tom', age=22}]

        List list1 = Arrays.asList("Jon", 258, "lll");
        arrayList.addAll(list1);
        //[123, BB, 123, 细算, Person{name='Tom', age=22}, Jon, 258, lll]
        System.out.println(arrayList);

        Object o = arrayList.get(1);
        System.out.println(o);          //BB

        System.out.println("**********************");

        int index = arrayList.indexOf("细算");
        System.out.println(index);          //3

        int jon = arrayList.lastIndexOf("Jon");
        System.out.println(jon);                    //5
        Object remove = arrayList.remove(1);
        System.out.println(remove);                 //BB
        System.out.println(arrayList);//[123, 123, 细算, Person{name='Tom', age=22}, Jon, 258, lll]

        arrayList.set(1,"CC");
        System.out.println(arrayList);//[123, CC, 细算, Person{name='Tom', age=22}, Jon, 258, lll]

        List list = arrayList.subList(4, 7);
        System.out.println(list);               //[Jon, 258, lll]       左闭右开
    }

    @Test
    public void test2() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(123);
        arrayList.add(123);
        arrayList.add("细算");
        arrayList.add(new Person("Tom",22));

        Iterator iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("**********************");

        for (Object obj : arrayList) {
            System.out.println(obj);
        }

        System.out.println("**********************");

        for (int i = 0;i < arrayList.size();i++) {
            System.out.println(arrayList.get(i));
        }
    }

    /**
     *  练习题
     */
    @Test
    public void test3() {
        List list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        updateList(list);
        System.out.println(list);//
    }
    private static void updateList(List list) {
        list.remove(2); //考察删除的是索引还是内容，即2是基本数据类型还是自动装箱包装类   删索引[1, 2]
       // list.remove(new Integer(2));        //删对象   [1, 3]
    }

}
