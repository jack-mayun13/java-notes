package top.java.collectionTest;


import org.junit.Test;

import java.util.*;

/**
 *          集合、数组都是对多个数据进行存储操作的结构，简称Java容器。
 *
 *          Collection接口 ：单列数据      用来存储一个一个的对象
 *
 *              -List接口：存储有序的，可重复的数据            --> "动态数组"
 *                  -ArrayList     -LinkedList       -Vector
 *
 *              -Set接口：存储无序的，不可重复的数据           -->  高中讲的 "集合"
 *                  -HashSet       -LinkedHashSet    -TreeSet
 *
 *          Map接口 ：双列数据       用来存储一对(Key - Value)一对的对象
 *                  -HashMap       -LinkedHashMap    -TreeMap       -HashTable      -Properties
 *
 */
public class CollectionTest1 {

    /**
     *      Collection常用方法：
     *
     *      添加
     *         add(Object obj)
     *         addAll(Collection coll)         讲另一个Collection的元素全部添加进来
     *      获取有效元素的个数
     *         int size()
     *      清空集合
     *         void clear()
     *      是否是空集合
     *         boolean isEmpty()
     */
    @Test
    public void test1() {
        Collection coll1 = new ArrayList();

        coll1.add("啊打发方式");
        coll1.add(115115);           //自动装箱
        coll1.add(new Date());

        System.out.println(coll1.size());            // 3

        Collection coll2 = new ArrayList();
        coll2.add(1554);
        coll2.add("sdadsa");
        boolean b1 = coll2.addAll(coll1);           // true         是否添加成功
        System.out.println(b1);
        System.out.println(coll2.size());           // 6

        //[1554, sdadsa, 啊打发方式, 115115, Sat Jul 16 20:12:07 CST 2022]
        System.out.println(coll2);

        boolean empty = coll1.isEmpty();
        System.out.println(empty);                  //false
        coll1.clear();
        System.out.println(coll1.isEmpty());        //true
    }

    /**
     *    Collection常用方法：
     *
     *   是否包含某个元素
     *      boolean contains(Object obj)：是通过元素的equals方法来判断是否是同一个对象
     *      boolean containsAll(Collection c)：也是调用元素的equals方法来比较的。拿两个集合的元素挨个比较。
     *
     *      说明： 向Collection接口实现类的对象中添加自定义类对象时，要重写   equals()方法
     *            没有重写equals方法，则调用Object中equals方法，用的 == 比较地址
     */
    @Test
    public void test2() {
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(234);
        coll.add(new String("Tom"));
        coll.add(false);
        coll.add(new Object());

        boolean contains = coll.contains(123);
        System.out.println(contains);               //true
        System.out.println(coll.contains(new String("Tom")));    //true
        coll.add(new Person("Jon",20));
        boolean jon = coll.contains(new Person("Jon", 20));
        System.out.println(jon);  //false     没有重写equals方法，则调用Object中equals方法，用的==比较地址

        System.out.println("*************************");

        Collection coll2 = Arrays.asList(123,234);  //asList()返回一个受指定数组支持的固定大小的列表。
        System.out.println(coll.containsAll(coll2));      // true
    }

    /**
     *   Collection常用方法：
     *
     *   删除,判断形参中所有元素是否都存在于当前集合中
     *     boolean remove(Object obj) ：通过元素的equals方法判断是否是要删除的那个元素。只会删除找到的第一个元素
     *     boolean removeAll(Collection coll)：取当前集合的差集，即移除两个集合共有的部分
     *
     *    说明： 向Collection接口实现类的对象中添加自定义类对象时，要重写   equals()方法
     *          没有重写equals方法，则调用Object中equals方法，用的 == 比较地址
     *
     *   取两个集合的交集
     *      boolean retainAll(Collection c)：把交集的结果存在当前集合中，不影响c,即修改当前集合为两个集合的交集
     */
    @Test
    public void test3() {
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(234);
        coll.add(new String("Tom"));
        coll.add(new Person("Jon",20));
        coll.add(false);
        System.out.println(coll);   //[123, 234, Tom, Person{name='Jon', age=20}, false]

        boolean remove = coll.remove(123);
        System.out.println(remove);                                  // true
        boolean jon = coll.remove(new Person("Jon", 20));
        System.out.println(jon);                                     // true
        System.out.println(coll);                                    // [234, Tom, false]
        Collection coll1 = Arrays.asList(123,234,false);

        System.out.println("***********************");

        boolean b = coll.removeAll(coll1);
        System.out.println(b);                                      // true
        System.out.println(coll);                                   // [Tom]
        coll.add(123);
        coll.add(234);
        System.out.println("**************************");
        System.out.println(coll);               //[Tom, 123, 234]
        System.out.println(coll1);              //[123, 234, false]
        coll.retainAll(coll1);
        System.out.println(coll);               //[123, 234]
        System.out.println(coll1);              //[123, 234, false]

    }

    /**
     *   Collection常用方法：
     *
     *   集合是否相等
     *      boolean equals(Object obj)
     *   转成对象数组
     *      Object[] toArray()
     *   获取集合对象的哈希值
     *      hashCode()
     */
    @Test
    public void test4() {
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(234);
        coll.add(new String("Tom"));
        coll.add(new Person("Jon",20));
        coll.add(false);
        System.out.println(coll);   //[123, 234, Tom, Person{name='Jon', age=20}, false]

        Collection coll1 = Arrays.asList(123,234);
        boolean equals = coll.equals(coll1);
        System.out.println(equals);       //false
        coll.retainAll(coll1);
        System.out.println(coll);                       //[123, 234]
        System.out.println(coll.equals(coll1));         //true
        Collection coll2 = Arrays.asList(234,123);
        System.out.println(coll.equals(coll2));     //false     因为是有序的

        System.out.println("*******************");
        coll.add(new Person("Jon",20));
        System.out.println(coll.hashCode());        //1314108633

        //集合    -->      数组
        Object[] objects = coll.toArray();
        System.out.println(Arrays.toString(objects)); //[123, 234, Person{name='Jon', age=20}]

        //数组    -->      集合
        List<String> strings = Arrays.asList(new String[]{"AA", "BB", "CC"});
        System.out.println(strings);

        System.out.println("******************");

        List<int[]> ints = Arrays.asList(new int[]{123, 254, 456}); //[[I@17d10166]
        System.out.println(ints);
        System.out.println(ints.size());                            //1
        List<Integer> integers = Arrays.asList(123, 254, 456);
        System.out.println(integers);                              //[123, 254, 456]
        System.out.println(integers.size());                        //3
        System.out.println(Arrays.asList(new Integer[]{123,254,456}));//[123, 254, 456]
    }
}

class Person {
    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null && o.getClass() != toString().getClass()) {
            return false;
        }
        if (o instanceof Person) {
            Person o1 = (Person) o;
            if (o1.getAge() == this.getAge() && o1.getName().equals(this.getName())) {
                return true;
            }
        }
        return false;
    }

}