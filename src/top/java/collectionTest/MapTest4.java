package top.java.collectionTest;


import org.junit.Test;

import java.util.Comparator;
import java.util.Objects;
import java.util.TreeMap;

public class MapTest4 {

    /**
     *      按照key进行排序： 自然排序  定制排序
     *
     *      向TreeMap中添加key-value，要求key必须是由同一个类创建的对象     不然报 ClassCastException
     *
     *      没有重写则：ClassCastException       Person cannot be cast to java.lang.Comparable
     *
     */
    @Test
    public void test1() {

        //  自然排序

        TreeMap map = new TreeMap();
//        map.put("AAA",123);
//        map.put("BBB",234);
//        map.put("CCC",234);
//        map.put("DDD",234);
//        System.out.println(map);

        map.put(new Person4("EEE",22),58);
        map.put(new Person4("FFF",34),76);
        map.put(new Person4("PPP",75),34);

        //{Person4{name='EEE', age=22}=58, Person4{name='FFF', age=34}=76,
        // Person4{name='PPP', age=75}=34}
        System.out.println(map);

    }

    @Test
    public void test2( ){

        TreeMap map = new TreeMap(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if (o1 instanceof Person4 && o2 instanceof Person4) {
                    Person4 p1 = (Person4) o1;
                    Person4 p2 = (Person4) o2;
                    if (p1.getName().equals(p2.getName())) {
                        return p1.getName().compareTo(p2.getName());
                    }else {
                        return p1.getAge() - p2.getAge();
                    }
                }
                throw new RuntimeException("传入数据类型不一致");
            }
        });
        map.put(new Person4("EEE",22),58);
        map.put(new Person4("FFF",34),76);
        map.put(new Person4("PPP",75),34);

        //{Person4{name='EEE', age=22}=58, Person4{name='FFF',
        // age=34}=76, Person4{name='PPP', age=75}=34}
        System.out.println(map);
    }


}

class Person4 implements Comparable{

    private String name;
    private int age;

    public Person4() {
    }

    public Person4(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person4 person4 = (Person4) o;
        return age == person4.age && Objects.equals(name, person4.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "Person4{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Person4) {
            Person4 p4 = (Person4) o;
            if (p4.name.equals(this.name)) {
                return this.age - p4.age;
            }else {
                return this.name.compareTo(p4.name);
            }
        }
        throw new RuntimeException("传入数据类型不一致");
    }
}
