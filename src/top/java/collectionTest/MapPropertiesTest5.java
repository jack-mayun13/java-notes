package top.java.collectionTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class MapPropertiesTest5 {

    public static void main(String[] args) throws IOException {

        FileInputStream file = null;
        try {
            Properties properties = new Properties();
            file = new FileInputStream("jdbc.properties");
            properties.load(file);          //加载流对应的文件
            String username = properties.getProperty("username");
            String password = properties.getProperty("password");
            System.out.println("username:" + username + '\n' + "password:" + password);
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            file.close();
        }
    }
}
