package top.java.collectionTest;


import org.junit.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapTest3 {

/**
 *   添加、删除、修改操作：
 *      Object put(Object key,Object value)：将指定key-value添加到(或修改)当前map对象中
 *      void putAll(Map m):将m中的所有key-value对存放到当前map中
 *      Object remove(Object key)：移除指定key的key-value对，并返回value
 *      void clear()：清空当前map中的所有数据
 */
    @Test
    public void test1() {
        HashMap hashMap = new HashMap();
        hashMap.put("AA",123);
        hashMap.put(455,234);
        hashMap.put("AA",852);          //修改
        hashMap.put(796,123);
        hashMap.put("BB",159);
        hashMap.put("CC",863);
        System.out.println(hashMap);            //{AA=852, BB=159, CC=863, 455=234, 796=123}

        HashMap hashMap1 = new HashMap();
        hashMap1.put("YYY","OOOO");
        hashMap1.put("Tom","Tom");
        System.out.println(hashMap1);           //{Tom=Tom, YYY=OOOO}

        hashMap.putAll(hashMap1);
        //{AA=852, BB=159, CC=863, Tom=Tom, 455=234, YYY=OOOO, 796=123}
        System.out.println(hashMap);

        System.out.println("**********************");

        Object remove = hashMap.remove(455);
        System.out.println(remove);              //234
        Object cc = hashMap.remove("CC");
        System.out.println(cc);                 //863
        System.out.println(hashMap);            //{AA=852, BB=159, Tom=Tom, YYY=OOOO, 796=123}

        hashMap.clear();
        System.out.println(hashMap);            //{}
        System.out.println(hashMap.size());     //0
    }

    /**
     *   元素查询的操作：
     *      Object get(Object key)：获取指定key对应的value
     *      boolean containsKey(Object key)：是否包含指定的key
     *      boolean containsValue(Object value)：是否包含指定的value
     *      int size()：返回map中key-value对的个数
     *      boolean isEmpty()：判断当前map是否为空
     *      boolean equals(Object obj)：判断当前map和参数对象obj是否相等
     */
    @Test
    public void test2() {
        HashMap hashMap = new HashMap();
        hashMap.put("AA",123);
        hashMap.put(796,123);
        hashMap.put("BB",159);
        hashMap.put("CC",863);

        Object aa = hashMap.get("AA");
        System.out.println(aa);             //123
        Object ff = hashMap.get("FF");
        System.out.println(ff);             //null

        System.out.println(hashMap.containsKey("AA"));          //true
        System.out.println(hashMap.containsValue(123));         //true

        System.out.println("**************");

        System.out.println(hashMap.size());                     //4

        System.out.println(hashMap.isEmpty());                  //false

    }

    /**
     *   元视图操作的方法：              遍历
     *      Set keySet()：返回所有key构成的Set集合
     *      Collection values()：返回所有value构成的Collection集合
     *      Set entrySet()：返回所有key-value对构成的Set集合
     */
    @Test
    public void test3() {
        HashMap hashMap = new HashMap();
        hashMap.put("AA",123);
        hashMap.put(796,123);
        hashMap.put("BB",159);
        hashMap.put("CC",863);

        //遍历所有的Key
        Set set = hashMap.keySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("****************");
        //遍历所有的Value
        Iterator iterator1 = hashMap.values().iterator();
        while (iterator1.hasNext()) {
            System.out.println(iterator1.next());
        }

        System.out.println("*****************");
        //遍历所有的key-value对
        Set set1 = hashMap.entrySet();
        Iterator iterator2 = set1.iterator();
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }
        System.out.println("*********取单个***********");
        Iterator iterator3 = set1.iterator();
        while (iterator3.hasNext()) {
            Object next = iterator3.next();
            Map.Entry entry = (Map.Entry) next;
            System.out.println(entry.getKey() + "----" + entry.getValue());
        }

        System.out.println("***********另一种遍历方式************");
        Set set2 = hashMap.keySet();
        for (Object obj : set2) {
            System.out.print(obj + "======");
            Object o = hashMap.get(obj);
            System.out.println(o);
        }
    }

    /**
     *      常用方法：
     *          添加： put(Object key,Object value)
     *          删除： remove(Object key)
     *          修改： put(Object key,Object value)
     *          查询： get(Object key)
     *          长度： size()
     *          遍历： Set keySet() / Collection values() / Set entrySet()
     */

}
