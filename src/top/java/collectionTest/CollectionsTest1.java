package top.java.collectionTest;


import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 *    Collections 是一个操作 Set、List 和 Map 等集合的工具类
 *
 *
 *
 */
public class CollectionsTest1 {

    /**
     *   reverse(List)：反转 List 中元素的顺序
     *   shuffle(List)：对 List 集合元素进行随机排序
     *   sort(List)：根据元素的自然顺序对指定 List 集合元素按升序排序
     *   sort(List，Comparator)：根据指定的 Comparator 产生的顺序对 List 集合元素进行排序
     *   swap(List，int， int)：将指定 list 集合中的 i 处元素和 j 处元素进行交换
     */
    @Test
    public void test1() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(123);
        arrayList.add(852);
        arrayList.add(-85);
        arrayList.add(-65);
        System.out.println(arrayList);      //[123, 852, -85, -65]

//        Collections.reverse(arrayList);       //[-65, -85, 852, 123]    反转
//        Collections.shuffle(arrayList);       //[852, -65, 123, -85]    随机
//        Collections.sort(arrayList);          //[-85, -65, 123, 852]    排序

          //定制排序
//        Collections.sort(arrayList, new Comparator<Object>() {
//            @Override
//            public int compare(Object o1, Object o2) {
//                return 0;
//            }
//        });

//        Collections.swap(arrayList,0,1);   //[852, 123, -85, -65]  交换


        System.out.println(arrayList);
    }

    /**
     *   查找、替换
     *      Object max(Collection)：根据元素的自然顺序，返回给定集合中的最大元素
     *      Object max(Collection，Comparator)：根据 Comparator 指定的顺序，返回给定集合中的最大元素
     *      Object min(Collection)
     *      Object min(Collection，Comparator)
     *      int frequency(Collection，Object)：返回指定集合中指定元素的出现次数
     *      boolean replaceAll(List list，Object oldVal，Object newVal)：使用新值替换List 对象的所有旧值
     */
    @Test
    public void test2() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(123);
        arrayList.add(852);
        arrayList.add(123);
        arrayList.add(123);
        arrayList.add(-85);
        arrayList.add(-65);
        System.out.println(arrayList);      //[123, 852, -85, -65]

//        Comparable max = Collections.max(arrayList);
//        System.out.println(max);                        //852       最大值

          //定制排序求最大值
//        Collections.max(arrayList, new Comparator<Object>() {
//            @Override
//            public int compare(Object o1, Object o2) {
//                return 0;
//            }
//        });

//        int frequency = Collections.frequency(arrayList, 123);
//        System.out.println(frequency);                              //1     求次数

        Collections.replaceAll(arrayList,123,888);

        System.out.println(arrayList);
    }


    /**
     *      void copy(List dest,List src)：将src中的内容复制到dest中
     */
    @Test
    public void test3() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(123);
        arrayList.add(852);
        arrayList.add(-85);
        arrayList.add(-65);
        System.out.println(arrayList);      //[123, 852, -85, -65]

        //IndexOutOfBoundsException("Source does not fit in dest")
//        ArrayList arrayList1 = new ArrayList();
//        Collections.copy(arrayList1,arrayList);
//        System.out.println(arrayList1);

        //正确的
        List<Object> objects = Arrays.asList(new Object[arrayList.size()]);
        Collections.copy(objects,arrayList);

        System.out.println(objects);            //[123, 852, -85, -65]
    }

    /**
     *      Collections 类中提供了多个 synchronizedXxx() 方法，该方法可使将指定集合包装成线程同步的集合，
     *      从而可以解决多线程并发访问集合时的线程安全问题
     */
    @Test
    public void test4() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(123);
        arrayList.add(852);
        arrayList.add(-85);
        arrayList.add(-65);

        //返回的list就是线程安全的
        List list = Collections.synchronizedList(arrayList);

    }

    /**
     *      对一个Java源文件中的关键字进行计数。
     *
     *      提示：Java源文件中的每一个单词，需要确定该单词是否是一个关键字。为了高效处理这个问题，
     *      将所有的关键字保存在一个HashSet中。用contains()测试。
     */
    @Test
    public void test5() throws FileNotFoundException {
        File file = new File("Test.java");
        Scanner scanner = new Scanner(file);
        while (scanner.hasNext()) {
            String word = scanner.next();
            System.out.println(word);
        }
    }


}
