package top.java.annotationTest;


import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.LOCAL_VARIABLE;

/**
 *          jdk 8.0         注解的新特性：  可重复注解 ， 类型注解
 */
public class AnnotationTest2 {

    /**
     *      可重复注解
     *
     *      1.在 MyAnnotation2 上声明 @Repeatable() ，成员值为 MyAnnotations.class
     *      2.定义 MyAnnotation2 的数组注解 MyAnnotations，让 @Target和 @Retention 与 MyAnnotation2相同
     *
     *      类型注解：
     *
     *          ElementType.TYPE_PARAMETER 表示该注解能写在类型变量的声明语句中（如：泛型声明）。
     *
     *          ElementType.TYPE_USE 表示该注解能写在使用类型的任何语句中。
     *
     *       使用：
     *          在 @Target 成员值中添加即可使用
     */

    public void test1() {

    }

}


@Inherited
@Repeatable(MyAnnotations.class)
@Target({TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR, LOCAL_VARIABLE}) //指定该注解可以修饰的结构
@Retention(RetentionPolicy.RUNTIME)         //指定该 Annotation 的生命周期
@interface MyAnnotation2 {
    //    String value();
    String value() default "hello";         //指定默认值,使用default定义
}

@Inherited
@Target({TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR, LOCAL_VARIABLE}) //指定该注解可以修饰的结构
@Retention(RetentionPolicy.RUNTIME)         //指定该 Annotation 的生命周期
@interface MyAnnotations {
    MyAnnotation2[] value();
}