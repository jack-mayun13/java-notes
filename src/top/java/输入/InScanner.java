package top.java.输入;

import java.util.Scanner;

public class InScanner {

    public static void main(String[] args) {

        /*
            获取int类型     nextInt
            获取String类型  next
            获取double类型  nextDouble
            获取boolean类型 nextBoolean
         */

        //Scanner中没有获取char型的方法

        Scanner scanner = new Scanner(System.in);

        int i = scanner.nextInt();

        System.out.println(i);

    }
}
