package top.java.projectThree.service;

import top.java.projectThree.domain.*;

public class NameListService {

    private Employee[] employees;


    public NameListService() {

        employees = new Employee[Data.EMPLOYEES.length];

        for (int index = 0;index < Data.EMPLOYEES.length;index++) {
            int id = Integer.parseInt(Data.EMPLOYEES[index][1]);
            String name = Data.EMPLOYEES[index][2];
            int age = Integer.parseInt(Data.EMPLOYEES[index][3]);
            double salary = Double.parseDouble(Data.EMPLOYEES[index][4]);

            int type = Integer.parseInt(Data.EMPLOYEES[index][0]);
            Equipment equipment;
            double bonus;
            int stock;

            switch (type) {
                case Data.EMPLOYEE:
                    employees[index] = new Employee(id,name,age,salary);
                    break;
                case Data.PROGRAMMER:
                    equipment = createEquipment(index);
                    employees[index] = new Programmer(id,name,age,salary,equipment);
                    break;
                case Data.DESIGNER:
                    bonus = Double.parseDouble(Data.EMPLOYEES[index][5]);
                    equipment = createEquipment(index);
                    employees[index] = new Designer(id,name,age,salary,equipment,bonus);
                    break;
                case Data.ARCHITECT:
                    bonus = Double.parseDouble(Data.EMPLOYEES[index][5]);
                    stock = Integer.parseInt(Data.EMPLOYEES[index][6]);
                    equipment = createEquipment(index);
                    employees[index] = new Architect(id,name,age,salary,equipment,bonus,stock);
                    break;
            }
        }
    }


    //获取当前所有员工。返回：包含所有员工对象的数组
    public Employee[] getAllEmployees(){
        return employees;
    }

    private Equipment createEquipment(int index) {

        int type = Integer.parseInt(Data.EQUIPMENTS[index][0]);

        switch (type) {
            case Data.PC:
                return new PC(Data.EQUIPMENTS[index][1],Data.EQUIPMENTS[index][2]);
            case Data.NOTEBOOK:
                return new NoteBook(Data.EQUIPMENTS[index][1],Double.parseDouble(Data.EQUIPMENTS[index][2]));
            case Data.PRINTER:
                return new Printer(Data.EQUIPMENTS[index][1],Data.EQUIPMENTS[index][2]);
        }
        return null;
    }

    public Employee getEmployee(int id) throws TeamException{
        for (Employee e : employees) {
            if (e.getId() == id) {
                return e;
            }
        }
        throw new TeamException("找不到该成员");
    }
}
