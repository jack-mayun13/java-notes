package top.java.projectThree.service;

public class TeamException extends Exception{

    static final long serialVersionUID = -3387519462429948L;

    public TeamException() {
    }

    public TeamException(String msg) {
        super(msg);
    }
}
