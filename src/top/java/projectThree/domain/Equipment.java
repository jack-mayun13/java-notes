package top.java.projectThree.domain;

public interface Equipment {

    public String getDescription();

}
