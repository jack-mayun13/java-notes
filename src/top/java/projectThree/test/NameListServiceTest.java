package top.java.projectThree.test;

import org.junit.Test;
import top.java.projectThree.domain.Employee;
import top.java.projectThree.service.NameListService;
import top.java.projectThree.service.TeamException;

public class NameListServiceTest {
    public static void main(String[] args) {

        NameListService nameListService = new NameListService();
        Employee[] allEmployees = nameListService.getAllEmployees();

        for (Employee e : allEmployees) {
            System.out.println(e);
        }
    }

    @Test
    public void test1() {
        NameListService nameListService = new NameListService();
        Employee employee = null;
        try {
            employee = nameListService.getEmployee(3);
        } catch (TeamException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(employee);
    }

}
