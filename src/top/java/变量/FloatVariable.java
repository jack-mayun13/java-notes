package top.java.变量;

public class FloatVariable {
    public static void main(String[] args) {

        /*
                float   4字节    单精度    精确到7位有效数字
                double  8字节    双精度    精确到14位有效数字
         */

        float a = 3.1415926F;   //小数后加F表示float类型数据，声明float类型变量必须以 F 或 f 结尾

        double b = 3.14159268755;

        System.out.println(b);

    }
}
