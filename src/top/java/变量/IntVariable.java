package top.java.变量;

public class IntVariable {
    public static void main(String[] args) {

        /*
            八种基本数据类型    byte short int long float double boolean char
            三种引用数据类型    类class(字符串是一个class类型) 数组[] 接口interface
        */
        /*
                byte    1字节
                short   2字节
                int     4字节
                long    8字节
         */

//     byte a = 128; //编译不通过 byte范围在 -128~127 之间 即1字节8个比特位 2的8次方256分正负和0

//        int w;               //未初始化不能使用,没有默认值
//        int y = w+1;
//        System.out.println(y)

        short b = 1111;
        int c = 222222;
        long d = 333333333L;   //整数后加L表示long类型数据，声明Long类型变量必须以 l 或 L 结尾

        System.out.println(d);


    }
}
