package top.java.变量;

public class BooleanVariable {
    public static void main(String[] args) {

        /*
                只能取 true 和 false 之一
         */
        boolean a = true;
        System.out.println(a);
        System.out.println("野兽大姚\\n撒促成撒比");    //  \n为换行   只输出 \n 不换行则 \\n
        System.out.println("---------------");
        System.out.println("哦IC撒\"野兽\"撒促成撒比"); //  只输出 "*"  则 \"*\"
    }
}
