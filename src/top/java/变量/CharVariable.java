package top.java.变量;

public class CharVariable {

    public static void main(String[] args) {

        /*
             char 2字节
         */

//        char i = 'AC';        //字符不能存两个   定义char类型通常使用一对 ''
//        char t = '是';        //字符可以存汉字
//        System.out.println(t);

//        char h = '';              //编译不通过
        char a = '\n';              //可以存转义字符
        System.out.println(a);      //输出语句是先输出再换行
        System.out.println("111");

        char a1 = 65;               //存数字自动转为对应Ascll码值
        System.out.println(a1);
        char a2 = 'A';
        System.out.println((int) a2);

    }
}
