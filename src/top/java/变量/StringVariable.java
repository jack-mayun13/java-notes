package top.java.变量;

public class StringVariable {
    public static void main(String[] args) {

        /*
                String   属于引用数据类型

                声明String类型变量时，使用一对 " "
                String可以和8种基本数据类型做运算   且只能做连接运算   运算结果仍然是String类型

                String不能转换为基本数据类型(包括强转)
         */

        //运算顺序是从左到右依次运算
        System.out.println("jjjj"+5+6);     //输出为jjjj56
        System.out.println(5+"jjjj"+6);     //输出为5jjjj6
        System.out.println(5+6+"jjjj");     //输出为11jjjj

        char c = '*';               //运用快捷键自动赋值给char
        String s = "*";             //运用快捷键自动赋值给Sting

        //说明 "" 为字符串    '' 为字符

        System.out.println("* *");                  //* *
        System.out.println('*' + '\t' + '*');       //93
        System.out.println('*' + "\t" + '*');       //* *
        System.out.println('*' + '\t' + "*");       //51*
        System.out.println('*' + ('\t' + "*"));     //* *
        
    }
}
