package top.java.IoTest;


import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *      标准输入输出流
 *      打印流
 *      数据流
 */
public class IoTest6 {

    /**
     *      标准输入输出流 (字节流)
     *
     *      System.in：标准的输入流，默认从键盘输入
     *      System.out：标准的输出流，默认从控制台输出
     *
     *      System类的setIn(InputStream is) / setOut(PrintStream ps) 方式重新指定输入和输出的流
     *
     *      练习题： 从键盘输入字符串，要求将读取到的整行字符串转成大写输出。然后继续进行输入操作，
     *             直至当输入“e”或者“exit”时，退出程序。
     *
     *     方法一：使用Scanner实现，调用next()返回一个字符串
     *     方法二：使用System.in实现
     */
    public static void main(String[] args) {
        BufferedReader buf = null;
        try {
            InputStreamReader isr = new InputStreamReader(System.in);
            buf = new BufferedReader(isr);
            while (true) {
                System.out.println("请输入字符串：");
                String str = buf.readLine();
                if ("e".equalsIgnoreCase(str) || "exit".equalsIgnoreCase(str)) {
                    System.out.println("程序结束");
                    break;
                }else {
                    System.out.println(str.toUpperCase());
                }
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (buf != null) {
                    buf.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *      打印流：PrintStream和PrintWriter
     *
     *      实现将基本数据类型的数据格式转化为字符串输出
     *
     *      提供了一系列重载的print()和println()方法，用于多种数据类型的输出
     *      PrintStream和PrintWriter的输出不会抛出IOException异常
     *      PrintStream和PrintWriter有自动flush功能
     *      PrintStream 打印的所有字符都使用平台的默认字符编码转换为字节。在需要写入字符而不是写入字节的情况下，
     *                  应该使用 PrintWriter 类。
     *      System.out返回的是PrintStream的实例
     */

    /**
     *   为了方便地操作Java语言的基本数据类型和String的数据，可以使用数据流。
     *
     *      数据流有两个类：(用于读取和写出基本数据类型、String类的数据）
     *       DataInputStream 和 DataOutputStream
     *       分别“套接”在 InputStream 和 OutputStream 子类的流上
     */
    @Test
    public void test1() {

    }
    
}
