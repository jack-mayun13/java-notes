package top.java.IoTest;


import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *      随机存取文件流：RandomAccessFile
 *
 *      构造器
 *          public RandomAccessFile(File file, String mode)
 *          public RandomAccessFile(String name, String mode)
 *      创建 RandomAccessFile 类实例需要指定一个 mode 参数，该参数指定 RandomAccessFile 的访问模式：
 *          r: 以只读方式打开
 *          rw：打开以便读取和写入
 *          rwd:打开以便读取和写入；同步文件内容的更新
 *          rws:打开以便读取和写入；同步文件内容和元数据的更新
 *              如果模式为只读r。则不会创建文件，而是会去读取一个已经存在的文件，如果读取的文件不存在则会出现异常。
 *              如果模式为rw读写。如果文件不存在则会去创建文件，如果存在则不会创建。
 *
 *      使用说明：
 *
 *          1.RandomAccessFile直接继承于java.lang.Object类，实现了DataInput和DataOutput接口
 *          2.RandomAccessFile既可以作为一个输入流，又可以作为一个输出流
 *
 *          3.如果RandomAccessFile作为输出流时，写出到的文件如果不存在，则在执行过程中自动创建。
 *            如果写出到的文件存在，则会对原文件内容进行覆盖。（默认情况下，从头覆盖）
 *
 *          4.可以通过相关的操作，实现RandomAccessFile“插入”数据的效果。seek(int pos)
 */
public class IoTest9 {

    /**
     *      使用 RandomAccessFile 实现图片的复制
     */
    @Test
    public void test1() throws IOException {
        RandomAccessFile raf1 = new RandomAccessFile(new File("ttt1.png"),"r");
        RandomAccessFile raf2 = new RandomAccessFile(new File("ttt2.png"),"rw");
        byte[] bytes = new byte[10];
        int len;
        while ((len = raf1.read(bytes)) != -1) {
            raf2.write(bytes,0,len);
        }
        raf2.close();
        raf1.close();
    }

    /**
     *      使用 RandomAccessFile 实现文本的写入
     */
    @Test
    public void test2() throws IOException {

        RandomAccessFile raf1 = new RandomAccessFile(new File("hello5.txt"),"rw");

        raf1.write("inh".getBytes());

        raf1.close();
    }

    /**
     *      使用 RandomAccessFile 实现文本的插入
     */
    @Test
    public void test3() throws IOException {

        RandomAccessFile raf1 = new RandomAccessFile(new File("hello5.txt"),"rw");

        raf1.seek(3);           //将指针调到角标为3的位置

        //保存指针3后面的数据到StringBuilder中
        StringBuilder stb = new StringBuilder((int) new File("hello5.txt").length());

        byte[] bytes = new byte[5];
        int len;
        while ((len = raf1.read(bytes)) != -1) {
            stb.append(new String(bytes,0,len));
        }
        //调回指针，写入"xyz"
        raf1.seek(3);
        raf1.write("xyz".getBytes());
        raf1.write(stb.toString().getBytes());
        raf1.close();

        //结果：
        //inhewqewqwewqeqw
        //inhxyzewqewqwewqeqw
    }

}
