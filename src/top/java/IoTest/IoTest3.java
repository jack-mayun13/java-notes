package top.java.IoTest;


import org.junit.Test;

import java.io.*;


/**
 *    FileInputStream / FileOutputStream的使用：
 *          对于文本文件(.txt,.java,.c,.cpp)，使用字符流处理
 *          对于非文本文件(.jpg,.mp3,.mp4,.avi,.doc,.ppt,...)，使用字节流处理
 *
 *    字节流处理文本文件的中文字符可能出现乱码
 */
public class IoTest3 {

    @Test
    public void test1() {
        FileInputStream f1 = null;
        FileOutputStream f2 = null;
        try {
            File file1 = new File("hhh.png");
            File file2 = new File("ttt.png");
            f1 = new FileInputStream(file1);
            f2 = new FileOutputStream(file2);
            byte[] bytes = new byte[5];
            int len;
            while ((len = f1.read(bytes)) != -1) {
                f2.write(bytes,0,len);
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (f2 != null) {
                    f2.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (f1 != null) {
                    f1.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
