package top.java.IoTest;


import org.junit.Test;

import java.io.*;

/**
 *      处理流之对象流：
 *      ObjectInputStream 和 ObjectOutputStream
 *
 *      用于存储和读取基本数据类型数据或对象的处理流。它的强大之处就是可以把Java中的对象写入到数据源中，
 *      也能把对象从数据源中还原回来。
 *
 *      序列化：用ObjectOutputStream类保存基本类型数据或对象的机制
 *
 *      反序列化：用ObjectInputStream类读取基本类型数据或对象的机制
 *
 *      ObjectOutputStream和ObjectInputStream不能序列化static和transient修饰的成员变量
 *
 *      对象序列化机制允许把内存中的Java对象转换成平台无关的二进制流，从而允许把这种二进制流持久地保存在磁盘上，
 *      或通过网络将这种二进制流传输到另一个网络节点。//当其它程序获取了这种二进制流，就可以恢复成原来的Java对象
 */
public class IoTest7 {

    /**
     *      序列化     将内存中的java对象保存到磁盘中或通过网络传输出去
     */
    @Test
    public void test1() {
        ObjectOutputStream oos = null;
        try {
            FileOutputStream fos = new FileOutputStream("hello4.txt");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(new String("啊上次aaacsccscs"));
            oos.flush();
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (oos != null) {
                    oos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *      反序列化     将磁盘中或网络中的java对象读取到内存中
     */
    @Test
    public void test2() {
        ObjectInputStream ois = null;
        try {
            FileInputStream sis = new FileInputStream("hello4.txt");
            ois = new ObjectInputStream(sis);
            Object o = ois.readObject();
            String str = (String) o;
            System.out.println(str.toUpperCase());
            System.out.println(str.length());
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (ois != null) {
                    ois.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
