package top.java.IoTest;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 *      File类的一个对象，代表一个文件或一个文件目录(文件夹)
 *
 *      File声明在java.io包下
 *
 *      File的常用方法
 *
 *      File类中涉及到关于文件或文件目录的创建、删除、重命名、修改时间、文件大小等方法，
 *      并未涉及到写入或读取文件内容的操作。如果需要读取或写入文件内容，必须使用IO流来完成。
 */
public class FileTest1 {

    /**
     *      File    类的实例化
     *
     *      绝对路径：是一个固定的路径,从盘符开始
     *      相对路径：是相对于某个位置开始
     *
     *  public File(String pathname)    以pathname为路径创建File对象，可以是绝对路径或者相对路径，如果
     *                                  pathname是相对路径，则默认的当前路径在系统属性user.dir中存储。
     *  public File(String parent,String child)     以parent为父路径，child为子路径创建File对象。
     *  public File(File parent,String child)       根据一个父File对象和子文件路径创建File对象
     */
    @Test
    public void test1() {
        //第一种实例化方式
        File file1 = new File("hello.txt");
        File file2 = new File("D:\\he.txt");
        //支持Linux平台
        File file3 = new File("d:" + File.separator  + "info.txt");

        System.out.println(file1);
        System.out.println(file2);
        System.out.println(file3);

        //第二种实例化方式
        File file4 = new File("D:\\","jjjjj");
        
        System.out.println(file4);

        //第三种实例化方式
        File file5 = new File(file4,"hi.txt");

        System.out.println(file5);
    }

    /**
     *      File的常用方法
     *
     *       File类的获取功能：
     *       public String getAbsolutePath()：获取绝对路径
     *       public String getPath() ：获取路径
     *       public String getName() ：获取名称
     *       public String getParent()：获取上层文件目录路径。若无，返回null
     *       public long length() ：获取文件长度（即：字节数）。不能获取目录的长度。
     *       public long lastModified() ：获取最后一次的修改时间，毫秒值
     *
     *       如下两个方法适用于文件目录
     *       public String[] list() ：获取指定目录下的所有文件或者文件目录的名称数组
     *       public File[] listFiles() ：获取指定目录下的所有文件或者文件目录的File数组
     */
    @Test
    public void test2() {
        File file1 = new File("hello.txt");

        //E:\home-java\work\java-notes\hello.txt
        System.out.println(file1.getAbsolutePath());
        System.out.println(file1.getPath());                //hello.txt
        System.out.println(file1.getName());                //hello.txt
        System.out.println(file1.getParent());              //null
        System.out.println(file1.length());                 //13
        System.out.println(file1.lastModified());           //1655542392110
    }

    /**
     *      File的常用方法
     *
     *      File类的重命名功能:
     *      public boolean renameTo(File dest):把文件重命名为指定的文件路径
     */
    @Test
    public void test3() {
        File file = new File("hello.txt");
        File file1 = new File("D:\\io\\");
        boolean b = file.renameTo(file1);
        System.out.println(b);
    }

    /**
     *       File的常用方法：
     *
     *       File类的判断功能
     *       public boolean isDirectory()：判断是否是文件目录
     *       public boolean isFile() ：判断是否是文件
     *       public boolean exists() ：判断是否存在
     *       public boolean canRead() ：判断是否可读
     *       public boolean canWrite() ：判断是否可写
     *       public boolean isHidden() ：判断是否隐藏
     */
    @Test
    public void test4() {
        File file1 = new File("hello.txt");
        System.out.println(file1.isDirectory());        //false
        System.out.println(file1.isFile());             //true
        System.out.println(file1.exists());             //true
        System.out.println(file1.canRead());            //true
        System.out.println(file1.canWrite());           //true
        System.out.println(file1.isHidden());           //false

        System.out.println("*********************");
        File file2 = new File("d:\\io");
        System.out.println(file2.isDirectory());        //true
        System.out.println(file2.isFile());             //false
        System.out.println(file2.exists());             //true
        System.out.println(file2.canRead());            //true
        System.out.println(file2.canWrite());           //true
        System.out.println(file2.isHidden());           //false
    }

    /**
     *       File的常用方法
     *
     *       File类的创建功能：
     *       public boolean createNewFile() ：创建文件。若文件存在，则不创建，返回false
     *       public boolean mkdir() ：创建文件目录。如此文件目录存在，就不创建。如果此文件目录的上层目录不存在，
     *                              也不创建。
     *       public boolean mkdirs() ：创建文件目录。如果上层文件目录不存在，一并创建
     *
     *       public boolean delete()：删除文件或者文件夹
     */
    @Test
    public void test5() throws IOException {
        //文件的删除与创建
        File file1 = new File("hi.txt");
        if (file1.createNewFile()) {
            System.out.println("创建成功1");
        }else {
            file1.delete();
            System.out.println("文件已存在,删除成功");
        }

        //文件目录的创建
        File file2 = new File("d:\\io\\io1");
        boolean mkdir = file2.mkdir();
        if (mkdir) {
            System.out.println("创建成功2");
        }
    }
}
