package top.java.IoTest;

import org.junit.Test;

import java.io.*;

/**
 *      流的体系结构：
 *
 *      抽象基类                节点流                     缓冲流(处理流的一种)
 *      InputStream         FileInputStream         BufferedInputStream
 *      OutputStream        FileOutputStream        BufferedOutputStream
 *      reader              FileReader              BufferedReader
 *      writer              FileWriter              BufferedWriter
 *
 *      操作流程：
 *       1.File类的实例化    2.流的实例化   3.读入操作/写入操作   4.资源的关闭
 *
 *      读操作说明：
 *          read()的理解：返回读入的一个字符。如果达到文件末尾，返回-1
 *          异常的处理：为了保证流资源一定可以执行关闭操作。需要使用try-catch-finally处理
 *          读入的文件一定要存在，否则就会报FileNotFoundException。
 *
 *      写操作说明：
 *          输出操作，对应的File可以不存在的。并不会报异常
 *
 *          File对应的硬盘中的文件如果不存在，在输出的过程中，会自动创建此文件。
 *          File对应的硬盘中的文件如果存在：
 *            如果流使用的构造器是：FileWriter(file,false) / FileWriter(file):对原文件的覆盖
 *            如果流使用的构造器是：FileWriter(file,true):不会对原文件覆盖，而是在原文件基础上追加内容
 */
public class IoTest2 {

    public static void main(String[] args) {
        File file = new File("工程名称\\hello.txt");   //相对路径  相较于当前工程需要加工程名称
        //E:\home-java\work\java-notes\hello.txt
        System.out.println(file.getAbsolutePath());
    }

    @Test
    public void test1() {
        File file = new File("hello.txt");          //相对路径  相较于当前Module
        //E:\home-java\work\java-notes\hello.txt
        System.out.println(file.getAbsolutePath());
    }

    @Test
    public void test2() {

        FileReader fr = null;
        try {
            //实例化File类的对象,指明要操作的文件
            File file = new File("hello.txt");
            //提供具体的流
            fr = new FileReader(file);

            int read = fr.read();                   //返回读入的一个字符，如果达到末尾。返回-1
            System.out.println(read);               //65279
            int data;
            while ((data = fr.read()) != -1) {
                System.out.print((char) data);     //helloWord
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (fr != null) {
                    fr.close();                             //关闭流
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void test3() {
        FileReader fr = null;
        try {
            //File类的实例化
            File file = new File("hello.txt");
            //FileReader流的实例化
            fr = new FileReader(file);
            //读入操作
            char[] arr = new char[5];
            int len;
            while ((len = fr.read(arr)) != -1) {
                //错误写法一：
//                for (int i = 0; i < arr.length;i++) {           //错误写法   helloWord123rd
//                    System.out.print(arr[i]);        //最后三个字符串赋值给字符数组前三个值，最后两个值未
//                }                                 //被覆盖,按字符数组长度操作会被使用，应每次操作len个
                //错误写法二：
//                String str = new String(arr);            //helloWord123rd
                //正确写法一：
//                String str = new String(arr,0,len);       //helloWord123
//                System.out.print(str);
                //正确写法二：
                for (int i = 0; i < len;i++) {
                    System.out.print(arr[i]);               //helloWord123
                }
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (fr != null) {
                    //资源的关闭
                    fr.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void test4() {
        FileWriter fw = null;
        try {
            File file = new File("hi.txt");
            fw = new FileWriter(file,true);
            fw.write("啊猝啊上次粗撒\n");
            fw.write("哦色彩哈桑层次上吃撒");
        }catch (IOException e){
            System.out.println(e.getMessage());
        }finally {
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //文本文件的复制
    @Test
    public void test5() {
        FileReader f1 = null;
        FileWriter f2 = null;
        try {
            File file1 = new File("hello.txt");
            File file2 = new File("hello1.txt");
            f1 = new FileReader(file1);
            f2 = new FileWriter(file2);
            char[] arr = new char[5];
            int len;
            while ((len = f1.read(arr)) != -1) {
                f2.write(arr,0,len);
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (f2 != null) {
                    f2.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (f1 != null) {
                    f1.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
