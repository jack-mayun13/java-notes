package top.java.IoTest;

/**
 *      输入input：读取外部数据（磁盘、光盘等存储设备的数据）到程序（内存）中。
 *
 *      输出output：将程序（内存）数据输出到磁盘、光盘等存储设备中。
 *
 *      按操作数据单位不同分为：字节流(图片，视频等，8 bit)，字符流(处理文本数据，16 bit)
 *      按数据流的流向不同分为：输入流，输出流
 *      按流的角色的不同分为：节点流，处理流
 *
 *      (抽象基类)          字节流             字符流
 *      输入流         InputStream         Reader
 *      输出流         OutputStream        Writer
 *
 *      流的体系结构：
 *
 *      抽象基类                节点流                     缓冲流(处理流的一种)
 *      InputStream         FileInputStream         BufferedInputStream
 *      OutputStream        FileOutputStream        BufferedOutputStream
 *      reader              FileReader              BufferedReader
 *      writer              FileWriter              BufferedWriter
 */
public class IoTest1 {


}
