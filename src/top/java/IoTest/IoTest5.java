package top.java.IoTest;

import org.junit.Test;

import java.io.*;

/**
 *          处理流之二：转换流
 *
 *    转换流，属于字符流
 *        InputStreamReader：将一个字节的输入流转换为字符的输入流
 *        OutputStreamWriter：将一个字符的输出流转换为字节的输出流
 *
 *    作用：提供字节流与字符流直接的转换
 *
 *    解码：字节、字节数组       -->     字符数组、字符串
 *    编码：字符数组、字符串     -->      字节、字节数组
 */
public class IoTest5 {

    /**
     *      InputStreamReader   解码：字节、字节数组      -->     字符数组、字符串
     *
     *      InputStreamReader(fis,"utf-8")  构造器中可以指定字符集     如不指定默认使用系统字符集
     *      具体使用那个编码集，取决于文件保存时使用的字符集
     *
     *      应该使用try-catch-finally处理异常     为了方便使用throws抛出异常
     */
    @Test
    public void test1() throws IOException {
        FileInputStream fis = new FileInputStream("hello2.txt");

        InputStreamReader inputStreamReader = new InputStreamReader(fis);
//        InputStreamReader inputStreamReader = new InputStreamReader(fis,"utf-8");
        char[] chars = new char[5];
        int len;
        while ((len = inputStreamReader.read(chars)) != -1) {
            String str = new String(chars,0,len);
            System.out.print(str);
        }
        inputStreamReader.close();
    }


    /**
     *      综合使用InputStreamReader   和   OutputStreamWriter
     *
     *      换编码复制
     */
    @Test
    public void test2() throws IOException {
        FileInputStream fis = new FileInputStream("hello2.txt");
        FileOutputStream fiw = new FileOutputStream("hello3.txt");

        InputStreamReader isr = new InputStreamReader(fis,"utf-8");
        OutputStreamWriter osw = new OutputStreamWriter(fiw,"gbk");

        char[] chars = new char[5];
        int len;
        while ((len = isr.read(chars)) != -1) {
            osw.write(chars,0,len);
        }
        isr.close();
        osw.close();
    }
}
