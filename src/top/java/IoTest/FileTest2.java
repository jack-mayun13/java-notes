package top.java.IoTest;


import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 *          File类    练习
 */

public class FileTest2 {

    @Test
    public void test1() throws IOException {
        File file1 = new File("d:\\io\\io1\\io2\\io3");
        file1.mkdirs();
        File file2 = new File("d:\\io\\io1\\io2\\io3\\ttt.txt");
        if (file2.createNewFile()) {
            System.out.println("创建成功");
        }else {
            file2.delete();
            System.out.println("已存在该文件");
            System.out.println("删除成功");
        }
    }


}
