package top.java.IoTest;


import org.junit.Test;

import java.io.*;

/**
 *          缓冲流的使用：
 *
 *            节点流                                       缓冲流(处理流的一种)
 *    FileInputStream(read(byte[] arr))            BufferedInputStream(read(byte[] arr))
 *    FileOutputStream(write(byte[] arr,0,len))    BufferedOutputStream(write(byte[] arr,0,len))
 *    FileReader(read(char[] arr))                 BufferedReader(read(char[] arr) / readLine())
 *    FileWriter(write(char[] arr,0,len))          BufferedWriter(write(char[] arr,0,len))
 *
 *          作用：提供流的读取，写入的速度
 *              提高读写速度的原因：内部提供了一个缓冲区
 *
 *          处理流，就是 "套接" 在已有的流的基础上
 *
 */
public class IoTest4 {

    /**
     *      缓冲流实现非文本数据的复制
     */
    @Test
    public void test1() {
        FileInputStream f1 = null;
        FileOutputStream f2 = null;
        BufferedInputStream bs1 = null;
        BufferedOutputStream bs2 = null;
        try {
            File file1 = new File("ttt.png");
            File file2 = new File("ttt1.png");

            f1 = new FileInputStream(file1);
            f2 = new FileOutputStream(file2);

            bs1 = new BufferedInputStream(f1);
            bs2 = new BufferedOutputStream(f2);

            byte[] bytes = new byte[10];
            int len;
            while ((len = bs1.read(bytes)) != -1) {
                bs2.write(bytes,0,len);
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (bs1 != null)
                bs1.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (bs2 != null)
                bs2.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //说明：   先关闭外层流，再关闭内层流
            //       关闭外层流的同时，内层流也会自动关闭，关于内层流的关闭，我们可以省略
//            f1.close();
//            f2.close();
        }
    }


    /**
     *      缓冲流实现非文本数据的复制
     */
    @Test
    public void test2() {
        BufferedReader bsr = null;
        BufferedWriter bsw = null;
        try {
            File file1 = new File("hello.txt");
            File file2 = new File("hello2.txt");

            FileReader f1 = new FileReader(file1);
            FileWriter f2 = new FileWriter(file2);

            bsr = new BufferedReader(f1);
            bsw = new BufferedWriter(f2);

            //方式一：
//            char[] arr = new char[10];
//            int len;
//            while ((len = bsr.read(arr)) != -1) {
//                bsw.write(arr, 0, len);
//            }
            //方式二：
            String data;
            while ((data = bsr.readLine()) != null) {
                bsw.write(data + "\n");         //readLine() 每次读取一行 没有返回null  不包含换行符
//                bsw.newLine();                   //提供换行的操作
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (bsw != null)
                bsw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (bsr != null)
                bsr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
