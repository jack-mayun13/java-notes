package top.java.IoTest;


import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *          自定义类对象的序列化与反序列化
 *
 *          1.自定义类实现如下两个接口之一。否则，会抛出NotSerializableException异常
 *              Serializable    Externalizable
 *
 *          2.需要一个序列化版本标识符的静态变量
 *              private static final long serialVersionUID = -6849794470754667710L;
 *
 *          3.除了当前Person类需要实现Serializable接口之外，还需要保证其内部所有属性都是可序列化的
 *            (默认情况下，基本数据类型可序列化)
 *
 *        说明：ObjectOutputStream和ObjectInputStream不能序列化static和transient修饰的成员变量
 */
public class IoTest8 {
    /**
     *      序列化：    创建一个 ObjectOutputStream
     *                调用 ObjectOutputStream 对象的 writeObject(对象) 方法输出可序列化对象
     *                注意写出一次，操作flush()一次
     */
    @Test
    public void test1() {
        ArrayList<Person> list = new ArrayList<>();
        list.add(new Person("张三",43));
        list.add(new Person("李四",55));
        list.add(new Person("王五",39));
        list.add(new Person("赵七",22));
        ObjectOutputStream oos = null;
        try {
            FileOutputStream fos = new FileOutputStream("hello4.txt");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
            oos.flush();
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (oos != null) {
                    oos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *      反序列化：    创建一个 ObjectInputStream
     *                  调用 readObject() 方法读取流中的对象
     */
    @Test
    public void test2() {
        ObjectInputStream ois = null;
        Object o = null;
        try {
            FileInputStream fis = new FileInputStream("hello4.txt");
            ois = new ObjectInputStream(fis);
            o = ois.readObject();
        }catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if (ois != null) {
                    ois.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
       if (o != null) {
           List<Person> list = (List<Person>) o;
           Iterator<Person> iterator = list.iterator();
           while (iterator.hasNext()) {
               System.out.println(iterator.next());
           }
       }
    }

}

class Person implements Serializable{
    private String name;
    private int age;

    private static final long serialVersionUID = 492626289822354710L;

    public Person() {

    }

    public Person(String name,int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
